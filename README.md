# General Information

## Maintainers
Luca Galli [luca.galli@pi.infn.it]
Marco Francesconi [marco.francesconi@pi.infn.it]

## Authors
Luca Galli [luca.galli@pi.infn.it]
Marco Francesconi [marco.francesconi@pi.infn.it]

# Origin
This repository was originally part of a larger repository that is now available here:
https://bitbucket.org/twavedaq/wavedaq_old.git

Note that the major part of the history was preserved when splitting the repository.

# Description
This repositry contains firmware files of the Trigger Control Board (TCB) for WaveDAQ.

There is a main repository containing all WaveDAQ repositories as submodules:
https://bitbucket.org/twavedaq/wavedaq_main.git
