`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 09.09.2015 15:39:50
// Design Name: 
// Module Name: MEMIN
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: all the rams for input serdes and processed data 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module ALLMEM(
    input CLK,
    input RUNMODE,
    input SYNC,
    input FADCMODE,
    input TESTTXMODE,
    input WENA,
    input RCLK,
    input [1023:0] INDATA,
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [31:0] WDATA,
    output tri [31:0] RDATA,
    output [1023:0] ALGDATA,
    output [9:0] ADDROUT,
    input [63:0] INDATAOUT,
    output reg [63:0] SERDESDATAOUT,
    input [31:0] BGOBITS,
    input [63:0] RDCBITS
    );
    
reg [6:0] COUNT_ADDR;

always @(posedge CLK) begin
    if (SYNC) begin
        COUNT_ADDR <= 0;
    end else begin
        if(RUNMODE) COUNT_ADDR <= COUNT_ADDR + 1;
    end
end

assign ADDROUT = {2'b000, COUNT_ADDR};

wire [1023:0] MEMALGDATA;
   
//bypass register
//reg [1023:0] INDATAREG;
//always @(posedge CLK) begin
//        INDATAREG <= INDATA;
//end
//output mux
assign ALGDATA = (FADCMODE==1'b1)? INDATA : MEMALGDATA;
//assign ALGDATA = (FADCMODE==1'b1)? INDATAREG : MEMALGDATA;

genvar iSerdes;
for(iSerdes=0; iSerdes<16; iSerdes=iSerdes+1) begin
    NEWRAM 
    #(
        .BASEADDR(`RINMEM0_ADDR+256*iSerdes),
        .SIZE(7)
    ) INPUT_LOWER_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(FADCMODE),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(INDATA[32*2*iSerdes+:32]),
        .DOUT(MEMALGDATA[32*2*iSerdes+:32])
    );
    NEWRAM 
    #(
        .BASEADDR(`RINMEM0_ADDR+256*iSerdes+128),
        .SIZE(7)
    ) INPUT_UPPER_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(FADCMODE),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(INDATA[32*(2*iSerdes+1)+:32]),
        .DOUT(MEMALGDATA[32*(2*iSerdes+1)+:32])
    );
end


wire [63:0] SERDESDATA;

    NEWRAM 
    #(
        .BASEADDR(`RINMEM0_ADDR+256*16),
        .SIZE(7)
    ) OUTPUT_LOWER_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(~TESTTXMODE),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(INDATAOUT[0+:32]),
        .DOUT(SERDESDATA[0+:32])
    );
    NEWRAM 
    #(
        .BASEADDR(`RINMEM0_ADDR+256*16+128),
        .SIZE(7)
    ) OUTPUT_UPPER_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(~TESTTXMODE),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(INDATAOUT[32+:32]),
        .DOUT(SERDESDATA[32+:32])
    );


//output mux
always @(*) begin
    casez(TESTTXMODE)
        1'b1: SERDESDATAOUT = SERDESDATA;
        1'b0: SERDESDATAOUT = INDATAOUT;
     endcase
end

// RAM fro BGO Bits
    NEWRAM 
    #(
        .BASEADDR(`RBGOMEM_ADDR),
        .SIZE(`RBGOMEM_SIZE)
    ) BGO_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(1'b1),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(BGOBITS),
        .DOUT()
    );

// RAMs fro RDC Bits
    NEWRAM 
    #(
        .BASEADDR(`RRDCMEM_ADDR),
        .SIZE(`RRDCMEM_SIZE)
    ) RDC_RAM_LOWER (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(1'b1),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(RDCBITS[31:0]),
        .DOUT()
    );
    NEWRAM 
    #(
        .BASEADDR(`RRDCMEM_ADDR+128),
        .SIZE(`RRDCMEM_SIZE)
    ) RDC_RAM_UPPER (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(1'b1),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(RDCBITS[63:32]),
        .DOUT()
    );

endmodule
