`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.04.2017 17:10:09
// Design Name: 
// Module Name: AUXTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"
`define CRCSLOT 6
`define BGOPRESHSLOT 7
`define BGOSLOT 8
`define MONITORDCSLOT 0
`define RDCLYSOSLOT 9
`define RDCPLASTICSLOT 14

`define PROTONCURRENTCHANNEL 16*13+12
`define ACCELRFCHANNEL 16*13+13
`define TCLASERCHANNEL 16*13+14
`define TCDIODECHANNEL 16*13+15
`define XECLEDMPPCCHANNEL 16*14+12
`define XECLEDPMTCHANNEL 16*14+13
`define NGENCHANNEL 16*14+14

 module AUXTRG(
    input [1023:0] WDDATA,
    input CLK,
    //bgo parameters
    input [31:0] BGOTHR,
    input [31:0] BGOVETOTHR,
    input [31:0] BGOHITDLY,
    input [31:0] BGOMASK,
    //rdc parameters
    input [31:0] RDCLYSOTHR,
    input [31:0] RDCLYSOVETOTHR,
    input [95:0] RDCLYSOHITMASK,
    input [31:0] RDCHITDLY,
    input [31:0] RDCMASK,
    //ngen parameters
    input [31:0] NGENDLY,
    input [31:0] NGENWIDTH,
    //crc parameters
    input [31:0] CRCHITMASK,
    input [31:0] CRCPAIRENABLE,
    //bgo triggers
    output BGOTRG,
    output BGOCOSM,
    output BGOTHRFIRE,
    output BGOVETOFIRE,
    output BGOPRESHCOUNTER,
    //rdc triggers
    output RDCTRG,
    output RDCPLASTICSINGLE,
    output RDCLYSOOR,
    output RDCLYSOTHRFIRE,
    //debug bits to memories
    output [31:0] BGOBITS,
    output [63:0] RDCBITS,
    //aux triggers
    output reg TCLASERTRG,
    output reg TCDIODETRG,
    output reg XECLEDPMTTRG,
    output reg XECLEDMPPCTRG,
    output reg RFACCELTRG,
    output reg PROTONCURRENTTRG,
    output MONITORDCTRG,
    //ngen window
    output NGENTRG,
    //crc triggers
    output CRCSINGLETRG,
    output CRCPAIRTRG
    );
    
    //DECODE DATA
    wire [16*16-1:0] SLOTHIT;
    wire [16*25-1:0] SLOTSUM;
    wire [16*4-1:0] SLOTMAX;
    wire [16*21-1:0] SLOTMAXWFM;
    genvar iSlot;
    for(iSlot =0; iSlot<16; iSlot= iSlot+1) begin
      SERDESDECODE #(
         .SLOT(iSlot),
         .NSLOT(16)
      ) slot_decode (
         .DATA(WDDATA),
         .WFMSUM(),
         .TDCSUM(),
         .TDCNUM(),
         .ANYTDC(),
         .TDCHIT(),
         .TDCVAL(),
         .AUXWFMSUM(SLOTSUM[(25*iSlot)+:25]),
         .AUXWFMMAX(SLOTMAX[(4*iSlot)+:4]),
         .AUXWFMMAXVAL(SLOTMAXWFM[(21*iSlot)+:21]),
         .DISCRSTATE(SLOTHIT[(16*iSlot)+:16])
      );
    end

    //split according to map
    wire [23:0] RDCPLASTICHIT;
    assign RDCPLASTICHIT[11:0] = SLOTHIT[16*`RDCPLASTICSLOT+:12];
    assign RDCPLASTICHIT[23:12] = SLOTHIT[16*(`RDCPLASTICSLOT+1)+:12];
    
    wire [75:0] RDCLYSOHIT;
    wire [5*25-1:0]  RDCLYSOINSUM;
    wire [5*21-1:0]  RDCLYSOINMAXWFM;
    wire [5*4-1:0]  RDCLYSOINMAX;
    assign RDCLYSOHIT = SLOTHIT[16*`RDCLYSOSLOT+:76];
    assign RDCLYSOINSUM = SLOTSUM[25*`RDCLYSOSLOT+:5*25];
    assign RDCLYSOINMAXWFM = SLOTMAXWFM[21*`RDCLYSOSLOT+:5*21];
    assign RDCLYSOINMAX = SLOTMAX[4*`RDCLYSOSLOT+:5*4];
    
    wire [15:0] CRCHIT;
    assign CRCHIT = SLOTHIT[16*`CRCSLOT+:16];
    
    wire [24:0] BGOSUM;
    wire [20:0] BGOMAXWFM;
    wire [3:0] BGOMAX;
    wire [15:0] BGOHIT;
    wire [15:0] BGOPRESHHIT;
    assign BGOSUM = SLOTSUM[25*`BGOSLOT+:25];
    assign BGOMAXWFM = SLOTMAXWFM[21*`BGOSLOT+:21];
    assign BGOMAX = SLOTMAX[4*`BGOSLOT+:4];
    assign BGOHIT = SLOTHIT[16*`BGOSLOT+:16];
    assign BGOPRESHHIT = SLOTHIT[16*`BGOPRESHSLOT+:16];

    wire [15:0] MONITORDCHIT;
    assign MONITORDCHIT = SLOTHIT[16*`MONITORDCSLOT+:16];
    
    wire NGENSTART;
    assign NGENSTART = SLOTHIT[`NGENCHANNEL];

    //RDC Logic
    wire [27:0] RDCLYSOSUM;
    wire [6:0] RDCLYSOMAX;
    wire [20:0] RDCLYSOMAXWFM;
    wire [11:0] RDCPLASTICCOINC;
    wire RDCLYSOVETOFIRE;
    wire RDCPLASTICLYSOSINGLE;

    RDCTRG RDCTRGBLOCK(
        .CLK(CLK),
        .PLASTICHIT(RDCPLASTICHIT),
        .LYSOHIT(RDCLYSOHIT),
        .LYSOINSUM(RDCLYSOINSUM),
        .LYSOINMAXWFM(RDCLYSOINMAXWFM),
        .LYSOINMAX(RDCLYSOINMAX),
        .LYSOTHR(RDCLYSOTHR),
        .LYSOVETOTHR(RDCLYSOVETOTHR),
        .LYSOHITMASK(RDCLYSOHITMASK),
        .HITDLY(RDCHITDLY),
        .MASK(RDCMASK),
        .LYSOSUM(RDCLYSOSUM),
        .LYSOMAX(RDCLYSOMAX),
        .LYSOMAXWFM(RDCLYSOMAXWFM),
        .PLASTICCOINC(RDCPLASTICCOINC),
        .PLASTICSINGLE(RDCPLASTICSINGLE),
        .LYSOOR(RDCLYSOOR),
        .LYSOTHRFIRE(RDCLYSOTHRFIRE),
        .LYSOVETOFIRE(RDCLYSOVETOFIRE),
        .PLASTICLYSOSINGLE(RDCPLASTICLYSOSINGLE),
        .TRG(RDCTRG)
    );
    assign RDCBITS = {RDCTRG, 14'b0, RDCPLASTICLYSOSINGLE, RDCPLASTICSINGLE, RDCLYSOVETOFIRE, RDCLYSOTHRFIRE, RDCLYSOOR, RDCPLASTICCOINC, RDCLYSOMAX, RDCLYSOSUM[24:0]};

    //BGO Logic
    wire BGOHITOR;
    wire BGOISMAX;
    wire BGOPRESH_OR;
    wire BGOPRESH_AND;
    wire BGOPRESH_OR_VETO;
    wire BGOPRESH_AND_VETO;
    BGOTRG BGOTRGBLOCK(
        .CLK(CLK),
        .SUM(BGOSUM),
        .MAX(BGOMAX),
        .MAXWFM(BGOMAXWFM),
        .HIT(BGOHIT),
        .PRESH_HIT(BGOPRESHHIT),
        .THR(BGOTHR),
        .VETOTHR(BGOVETOTHR),
        .HITDLY(BGOHITDLY),
        .MASK(BGOMASK),
        .TRG(BGOTRG),
        .TRGCOSM(BGOCOSM),
        .THRFIRE(BGOTHRFIRE),
        .VETOFIRE(BGOVETOFIRE),
        .HITOR(BGOHITOR),
        .ISMAX(BGOISMAX),
        .PRESH_OR(BGOPRESH_OR),
        .PRESH_AND(BGOPRESH_AND),
        .PRESH_OR_VETO(BGOPRESH_OR_VETO),
        .PRESH_AND_VETO(BGOPRESH_AND_VETO),
        .PRESHCOUNTER(BGOPRESHCOUNTER)
    );
    assign BGOBITS = {BGOPRESHCOUNTER, BGOTRG, BGOISMAX, BGOHITOR, BGOVETOFIRE, BGOTHRFIRE, BGOPRESH_AND_VETO, BGOPRESH_OR_VETO, BGOPRESH_AND, BGOPRESH_OR, BGOSUM[21:0]};
    
    // SINGLE BIT LOGIC (i.e. TCLASER, TCDIODE, XECLED...)
    always @(posedge CLK) begin
        TCLASERTRG       <= SLOTHIT[`TCLASERCHANNEL];
        TCDIODETRG       <= SLOTHIT[`TCDIODECHANNEL];
        XECLEDPMTTRG     <= SLOTHIT[`XECLEDPMTCHANNEL];
        XECLEDMPPCTRG    <= SLOTHIT[`XECLEDMPPCCHANNEL];
        RFACCELTRG       <= SLOTHIT[`ACCELRFCHANNEL];
        PROTONCURRENTTRG <= SLOTHIT[`PROTONCURRENTCHANNEL];
    end
    
    //CRC Trigger
    CRCTRG CRCTRGBLOCK(
    .CLK(CLK),
    .HIT(CRCHIT),
    .HITMASK(CRCHITMASK[7:0]),
    .PAIRENABLE(CRCPAIRENABLE[15:0]),
    .SINGLETRG(CRCSINGLETRG),
    .PAIRTRG(CRCPAIRTRG)
    );
    
    //MONITOR DC Logic
    MONITORDCTRG MONITORDCTRGBLOCK(
    .CLK(CLK),
    .HIT(MONITORDCHIT),
    .TRG(MONITORDCTRG)
    );
    
    //Neutron generator Logic
    NGENTRG NGENTRGBLOCK(
    .CLK(CLK),
    .START(NGENSTART),
    .DLY(NGENDLY[14:0]),
    .WIDTH(NGENWIDTH[14:0]),
    .TRG(NGENTRG)
    );

endmodule
