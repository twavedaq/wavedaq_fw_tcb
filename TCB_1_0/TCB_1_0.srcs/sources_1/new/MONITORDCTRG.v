`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.04.2017 16:35:03
// Design Name: 
// Module Name: MONITORDCTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MONITORDCTRG(
    input CLK,
    input [15:0] HIT,
    output reg TRG
    );
    
    always @(posedge CLK) begin
        TRG <= |HIT;
    end
    
endmodule
