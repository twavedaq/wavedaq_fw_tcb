`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.04.2017 10:19:39
// Design Name: 
// Module Name: RDCTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RDCTRG(
    input CLK,
    //data input
    input [23:0] PLASTICHIT,
    input [75:0] LYSOHIT,
    input [5*25-1:0] LYSOINSUM,
    input [5*21-1:0] LYSOINMAXWFM,
    input [5*4-1:0] LYSOINMAX,
    //parameters
    input [31:0] LYSOTHR,
    input [31:0] LYSOVETOTHR,
    input [95:0] LYSOHITMASK,
    input [31:0] HITDLY,
    input [31:0] MASK,
    //elaborated inputs
    output reg [27:0] LYSOSUM,
    output [6:0] LYSOMAX,
    output [20:0] LYSOMAXWFM,
    output reg [11:0] PLASTICCOINC,
    //trigger conritions
    output PLASTICSINGLE,
    output LYSOOR,
    output LYSOTHRFIRE,
    output LYSOVETOFIRE,
    output PLASTICLYSOSINGLE,
    //trigger
    output TRG
    );
    
    //PLASTIC COINCIDENCIES
    reg[31:0] PLASTICSINGLE_PIPELINE;

    integer iPlastic;
    always @(posedge CLK) begin
        for(iPlastic=0; iPlastic<12; iPlastic= iPlastic+1) begin
            PLASTICCOINC[iPlastic] = PLASTICHIT[2*iPlastic] & PLASTICHIT[2*iPlastic+1];
        end
        PLASTICSINGLE_PIPELINE[0] <= |PLASTICCOINC;
    end

    //programmable delay & shaper
    always @(posedge CLK) begin
       PLASTICSINGLE_PIPELINE[31:1] <=  PLASTICSINGLE_PIPELINE[30:0];
    end
    SHAPER #(
      .SRLENGTH(4)
    ) PRESHPLASTICSINGLE (
      .DIN(PLASTICSINGLE_PIPELINE[HITDLY[4:0]]),
      .CLK(CLK),
      .DOUT(PLASTICSINGLE)
   );
    
    //LYSO OR
    reg[31:0] LYSOOR_PIPELINE;
    always @(posedge CLK) begin
        LYSOOR_PIPELINE[0] <= |( LYSOHIT & LYSOHITMASK[75:0]);
    end

    //programmable delay & shaper
    always @(posedge CLK) begin
       LYSOOR_PIPELINE[31:1] <=  LYSOOR_PIPELINE[30:0];
    end
    SHAPER #(
      .SRLENGTH(4)
    ) PRESHLYSOOR (
      .DIN(LYSOOR_PIPELINE[HITDLY[4:0]]),
      .CLK(CLK),
      .DOUT(LYSOOR)
   );
    
    //PLASTIC-LYSO SPATIAL COINCIDENCE
    reg [11:0]PLASTICLYSOCOINC;
    reg [31:0]PLASTICLYSOCOINC_PIPELINE;
    always @(posedge CLK) begin
       PLASTICLYSOCOINC[0] <= PLASTICCOINC[0] & (LYSOHIT[0] | LYSOHIT[1] | LYSOHIT[2] | LYSOHIT[3]);
       PLASTICLYSOCOINC[1] <= PLASTICCOINC[1] & (LYSOHIT[4] | LYSOHIT[5] | LYSOHIT[6] | LYSOHIT[7] | LYSOHIT[8] | LYSOHIT[9]);
       PLASTICLYSOCOINC[2] <= PLASTICCOINC[2] & (LYSOHIT[10] | LYSOHIT[11] | LYSOHIT[12] | LYSOHIT[13] | LYSOHIT[14] | LYSOHIT[15] | LYSOHIT[16] | LYSOHIT[17]);
       PLASTICLYSOCOINC[3] <= PLASTICCOINC[3] & (LYSOHIT[18] | LYSOHIT[19] | LYSOHIT[20] | LYSOHIT[21] | LYSOHIT[22] | LYSOHIT[23] | LYSOHIT[24] | LYSOHIT[25] | LYSOHIT[26] | LYSOHIT[27]);
       PLASTICLYSOCOINC[4] <= PLASTICCOINC[4] & (LYSOHIT[18] | LYSOHIT[19] | LYSOHIT[20] | LYSOHIT[21] | LYSOHIT[22] | LYSOHIT[23] | LYSOHIT[24] | LYSOHIT[25] | LYSOHIT[26] | LYSOHIT[27] | LYSOHIT[28] | LYSOHIT[29] | LYSOHIT[30] | LYSOHIT[31] | LYSOHIT[32] | LYSOHIT[33] | LYSOHIT[34] | LYSOHIT[35] | LYSOHIT[36] | LYSOHIT[37]);
       PLASTICLYSOCOINC[5] <= PLASTICCOINC[5] & (LYSOHIT[28] | LYSOHIT[29] | LYSOHIT[30] | LYSOHIT[31] | LYSOHIT[32] | LYSOHIT[33] | LYSOHIT[34] | LYSOHIT[35] | LYSOHIT[36] | LYSOHIT[37]);
       PLASTICLYSOCOINC[6] <= PLASTICCOINC[6] & (LYSOHIT[38] | LYSOHIT[39] | LYSOHIT[40] | LYSOHIT[41] | LYSOHIT[42] | LYSOHIT[43] | LYSOHIT[44] | LYSOHIT[45] | LYSOHIT[46] | LYSOHIT[47]);
       PLASTICLYSOCOINC[7] <= PLASTICCOINC[7] & (LYSOHIT[38] | LYSOHIT[39] | LYSOHIT[40] | LYSOHIT[41] | LYSOHIT[42] | LYSOHIT[43] | LYSOHIT[44] | LYSOHIT[45] | LYSOHIT[46] | LYSOHIT[47]| 
          LYSOHIT[48] | LYSOHIT[49] | LYSOHIT[50] | LYSOHIT[51] | LYSOHIT[52] | LYSOHIT[53] | LYSOHIT[54] | LYSOHIT[55] | LYSOHIT[56] | LYSOHIT[57]);
       PLASTICLYSOCOINC[8] <= PLASTICCOINC[8] & (LYSOHIT[48] | LYSOHIT[49] | LYSOHIT[50] | LYSOHIT[51] | LYSOHIT[52] | LYSOHIT[53] | LYSOHIT[54] | LYSOHIT[55] | LYSOHIT[56] | LYSOHIT[57]);
       PLASTICLYSOCOINC[9] <= PLASTICCOINC[9] & (LYSOHIT[58] | LYSOHIT[59] | LYSOHIT[60] | LYSOHIT[61] | LYSOHIT[62] | LYSOHIT[63] | LYSOHIT[64] | LYSOHIT[65]);
       PLASTICLYSOCOINC[10] <= PLASTICCOINC[10] & (LYSOHIT[66] | LYSOHIT[67] | LYSOHIT[68] | LYSOHIT[69] | LYSOHIT[70] | LYSOHIT[71]);
       PLASTICLYSOCOINC[11] <= PLASTICCOINC[11] & (LYSOHIT[72] | LYSOHIT[73] | LYSOHIT[74] | LYSOHIT[75]);

       PLASTICLYSOCOINC_PIPELINE[0] <= |PLASTICLYSOCOINC;
    end

    //programmable delay & shaper
    always @(posedge CLK) begin
       PLASTICLYSOCOINC_PIPELINE[31:1] <=  PLASTICLYSOCOINC_PIPELINE[30:0];
    end
    SHAPER #(
      .SRLENGTH(4)
    ) PRESHPLASTICLYSOCOINC (
      .DIN(PLASTICLYSOCOINC_PIPELINE[HITDLY[4:0]]),
      .CLK(CLK),
      .DOUT(PLASTICLYSOSINGLE)
   );
    
    //LYSO SUM THREE
    reg [3*26-1:0] FIRSTOUT;
    reg [2*27-1:0] SECONDOUT;
    integer isumzero;
    always @(posedge CLK) begin
        for(isumzero = 0; isumzero < 2; isumzero = isumzero + 1) begin  
            FIRSTOUT[isumzero*26+:26] <= $signed(LYSOINSUM[25*isumzero*2+:25]) + $signed(LYSOINSUM[25*(isumzero*2+1)+:25]);
        end
        //sign-extend last input
        FIRSTOUT[2*26+:26] <= {LYSOINSUM[25*5-1], LYSOINSUM[25*4+:25]};
    end
    always @(posedge CLK) begin
        SECONDOUT[0+:27] <= $signed(FIRSTOUT[0+:26]) + $signed(FIRSTOUT[26*1+:26]);
        SECONDOUT[27+:27] <= {FIRSTOUT[3*26-1], FIRSTOUT[2*26+:26]};
    end
    always @(posedge CLK) begin
        LYSOSUM <= $signed(SECONDOUT[0+:27]) + $signed(SECONDOUT[27*1+:27]);
    end
    //always @(posedge CLK) begin
    //  if($signed(LYSOSUM)>$signed(LYSOTHR[26:0])) begin
    //      LYSOTHRFIRE <= 1'b1;
    //  end else begin
    //      LYSOTHRFIRE <= 1'b0;
    //  end
    //end
    wire LYSOTHRFIRE_PROMPT;
    DISCRIMINATEWFM #(
       .LENGTH(8),
       .WFMSIZE(28)
    ) LYSOTHRFIRE_DISCR (
       .INPUT(LYSOSUM),
       .THR(LYSOTHR[27:0]),
       .CLK(CLK),
       .TRIG(LYSOTHRFIRE_PROMPT)
    );
    reg [1:0] LYSOTHRFIRE_DLY;
    always @(posedge CLK) begin
       LYSOTHRFIRE_DLY <= {LYSOTHRFIRE_DLY[0], LYSOTHRFIRE_PROMPT};
    end
    assign LYSOTHRFIRE = LYSOTHRFIRE_DLY[1];

    DISCRVETO #(
       .WFMSIZE(28)
    ) LYSOTHRFIRE_VETO (
       .INPUT(LYSOSUM),
       .THR(LYSOVETOTHR[27:0]),
       .THR_TOVETO(LYSOTHR[27:0]),
       .DISCR_TOVETO(LYSOTHRFIRE),
       .CLK(CLK),
       .TRIG(LYSOVETOFIRE)
    );

    //LYSO MAX THREE
    genvar icmpzero;
    wire [3*5-1:0] FIRSTMAX;
    wire signed [21*3-1:0] OUTFIRSTCMP;
    wire [2*6-1:0] SECONDMAX;
    wire signed [21*2-1:0] OUTSECONDCMP;
    for(icmpzero = 0; icmpzero < 2; icmpzero = icmpzero + 1) begin
        CMP_MAX_BLOCK #(
        .MAXWIDTH(5),
        .WFMWIDTH(21)
        ) CMP_MAX_INST(
        .WFMA(LYSOINMAXWFM[21*icmpzero*2+:21]),
        .WFMB(LYSOINMAXWFM[21*(icmpzero*2+1)+:21]),
        .MAXA({1'b0, LYSOINMAX[4*icmpzero*2+:4]}),
        .MAXB({1'b1, LYSOINMAX[4*(icmpzero*2+1)+:4]}),
        .CLK(CLK),
        .WFMO(OUTFIRSTCMP[21*icmpzero+:21]),
        .MAXO(FIRSTMAX[5*icmpzero+:5])
        );
    end
    reg [3:0] DLYFIRSTMAX;
    reg [20:0] DLYFIRSTMAXWFM;
    always @(posedge CLK) begin
        DLYFIRSTMAX <= LYSOINMAX[4*4+:4];
        DLYFIRSTMAXWFM <= LYSOINMAXWFM[21*4+:21];
        
    end
    assign OUTFIRSTCMP[21*2+:21] = DLYFIRSTMAXWFM;
    assign FIRSTMAX[5*2+:5] = {1'b0, DLYFIRSTMAX};
    
    CMP_MAX_BLOCK #(
    .MAXWIDTH(6),
    .WFMWIDTH(21)
    ) SECOND_CMP_MAX_INST(
    .WFMA(OUTFIRSTCMP[0+:21]),
    .WFMB(OUTFIRSTCMP[21+:21]),
    .MAXA({1'b0, FIRSTMAX[0+:5]}),
    .MAXB({1'b1, FIRSTMAX[5+:5]}),
    .CLK(CLK),
    .WFMO(OUTSECONDCMP[0+:21]),
    .MAXO(SECONDMAX[0+:6])
    );
    reg [4:0] DLYSECONDMAX;
    reg [20:0] DLYSECONDMAXWFM;
    always @(posedge CLK) begin
        DLYSECONDMAX <= FIRSTMAX[5*2+:5];
        DLYSECONDMAXWFM <= OUTFIRSTCMP[21*2+:21];
        
    end
    assign OUTSECONDCMP[21+:21] = DLYSECONDMAXWFM;
    assign SECONDMAX[6+:6] = {1'b0, DLYSECONDMAX};
    
    CMP_MAX_BLOCK #(
    .MAXWIDTH(7),
    .WFMWIDTH(21)
    ) THIRD_CMP_MAX_INST(
    .WFMA(OUTSECONDCMP[0+:21]),
    .WFMB(OUTSECONDCMP[21+:21]),
    .MAXA({1'b0, SECONDMAX[0+:6]}),
    .MAXB({1'b1, SECONDMAX[6+:6]}),
    .CLK(CLK),
    .WFMO(LYSOMAXWFM),
    .MAXO(LYSOMAX)
    );
    
    //COMBINE LOGIC TOGETHER
    DETECTORBUILD #(
    .SIZE(5)
    ) RDCBUILD(
    .CLK(CLK),
    .IN({PLASTICLYSOSINGLE, PLASTICSINGLE, LYSOOR, ~LYSOVETOFIRE, LYSOTHRFIRE}),
    .MASK(MASK[4:0]),
    .OUT(TRG)
    );
        
endmodule
