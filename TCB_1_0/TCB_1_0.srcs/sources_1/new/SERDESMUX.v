`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.11.2016 17:56:00
// Design Name: 
// Module Name: SERDESMUX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "global_parameters.vh"
module SERDESMUX(
    input [`NTRG-1:0] TRGENA,
    input [64*`NTRG-1:0] IN,
    output reg [63:0] OUT
    );
    
    integer iTRG;
    always @(*) begin
        OUT =0;
    
        for(iTRG=`NTRG-1; iTRG>=0; iTRG=iTRG-1) begin
            if (TRGENA[iTRG]) OUT = IN[64*iTRG+:64];
        end
    
    end
endmodule
