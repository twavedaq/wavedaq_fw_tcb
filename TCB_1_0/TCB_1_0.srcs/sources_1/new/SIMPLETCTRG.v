`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.11.2016 15:06:07
// Design Name: 
// Module Name: SIMPLETCTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SIMPLETCTRG(
    input [1023:0] WDDATA,
    input CLK,
    input [127:0] TCMASKS,
    output reg TCOR,
    output HIT0,
    output HIT1,
    output [6:0] TILEID0,
    output [6:0] TILEID1,
    output [4:0] TILETIME0,
    output [4:0] TILETIME1,
    input [31:0] TCOMPH,
    input [31:0] TCOMPL,
    output [7:0] TCNHIT
    );
    
    wire [255:0] HIT;
    wire [767:0] HITTIME;
   genvar islot;
   for(islot = 0; islot < 16; islot = islot+1) begin
      SERDESDECODE #(
         .SLOT(islot),
         .NSLOT(16)
      ) slot_decode (
         .DATA(WDDATA),
         .WFMSUM(),
         .TDCSUM(),
         .TDCNUM(),
         .TDCHIT(HIT[16*islot+:16]),
         .TDCVAL(HITTIME[3*16*islot+:3*16]),
         .ANYTDC(),
         .AUXWFMSUM(),
         .AUXWFMMAX(),
         .AUXWFMMAXVAL(),
         .DISCRSTATE()
      );
   end
    
    //tile coincidence
    reg [255:0] HIT_REG;
    integer iTile;
    reg [256*3-1:0] HITTIME_REG;
    always @(posedge CLK) begin
      HIT_REG <= HIT;
        for(iTile=0; iTile<128; iTile=iTile+1)begin
            HITTIME_REG[6*iTile+:3] <= HITTIME[6*iTile+:3];
            HITTIME_REG[6*iTile+3+:3] <= HITTIME[6*iTile+3+:3];
        end
    end

    reg [127:0] TILEHIT;
    reg [128*5-1:0] TILETIME;
    always @(posedge CLK) begin
        for(iTile=0; iTile<128; iTile=iTile+1)begin
            if(~TCMASKS[iTile]) begin
               TILEHIT[iTile] <= 1'b0;
               TILETIME[5*iTile+:5] <= 0;
            end else begin
               if(HIT[2*iTile] & HIT[2*iTile+1]) begin
                  //hit within the same clock
                  TILEHIT[iTile] <= 1'b1;
                  TILETIME[5*iTile+:5] <= {1'b0, HITTIME[6*iTile+:3]} + {1'b0, HITTIME[(6*iTile+3)+:3]};
              end else if(HIT[2*iTile] & HIT_REG[2*iTile+1]) begin
                  //hit between two clock
                  TILEHIT[iTile] <= 1'b1;
                  TILETIME[5*iTile+:5] <= {1'b0, HITTIME[6*iTile+:3]} + {1'b1, HITTIME_REG[6*iTile+3+:3]};
               end else if (HIT_REG[2*iTile] & HIT[2*iTile+1]) begin
                  //hit between two clock
                  TILEHIT[iTile] <= 1'b1;
                  TILETIME[5*iTile+:5] <= {1'b1, HITTIME_REG[6*iTile+:3]} + {1'b0, HITTIME[6*iTile+3+:3]};
               end else begin
                  TILEHIT[iTile] <= 1'b0;
                  TILETIME[5*iTile+:5] <= 0;
               end
            end
        end
    end

    //align hits
    reg [127:0] TILEHITREG;
    reg [128*5-1:0] TILETIMEREG;
    reg [127:0] TILEHITALIGNED;
    reg [128*5-1:0] TILETIMEALIGNED;
    always @(posedge CLK) begin
       TILEHITREG <=TILEHIT;
       TILETIMEREG <=TILETIME;
       for(iTile=0; iTile<128; iTile=iTile+1)begin
          if(TILEHIT[iTile] & TILETIME[5*iTile+:5]>=5'b10000) begin
             TILEHITALIGNED[iTile] <= 1'b1;
              TILETIMEALIGNED[5*iTile+:5] = {1'b0, TILETIME[5*iTile+:4]};
          end else if(TILEHITREG[iTile] & TILETIMEREG[5*iTile+:5]<5'b10000) begin
             TILEHITALIGNED[iTile] <= 1'b1;
             TILETIMEALIGNED[5*iTile+:5] = {1'b0, TILETIMEREG[5*iTile+:4]};
          end else begin
            TILEHITALIGNED[iTile] <= 1'b0;
            TILETIMEALIGNED[5*iTile+:5] =0;
          end
       end
    end

    // global or
    always @(posedge CLK) begin
        TCOR <= |TILEHITSH;
    end
    
    //PRIORITY ENCODER WITH TIME CONDITION TO FIND UP TO TWO TRACKS
    TCTrackFinder TCTrackFinder_Inst(
        .TILEHIT(TILEHITALIGNED),
        .CLK(CLK),
        .TILETIME(TILETIMEALIGNED),
        .HIT0(HIT0),
        .HIT1(HIT1),
        .TILEID0(TILEID0),
        .TILEID1(TILEID1),
        .TILETIME0(TILETIME0),
        .TILETIME1(TILETIME1),
        .TCOMPH(TCOMPH),
        .TCOMPL(TCOMPL)        
        );
        
    //SHAPE HIT TO COUNT MULTIPLICITY
    wire [127:0] TILEHITSH;
    SHAPER #(.SRLENGTH(3)) SHAPER_HIT[127:0] (
        .DIN(TILEHIT),
        .DOUT(TILEHITSH),
        .CLK(CLK)
    );
    
    //calculate the number of fired tile for multiplicity trigger
    integer iPix;
    reg [6:0] iHit0;
    reg [6:0] iHit1;
    reg [6:0] TCNHIT0;
    reg [6:0] TCNHIT1;
    reg [7:0] TCNHITREG;
    always @(posedge CLK) begin
        iHit0 = 7'b0;
        iHit1 = 7'b0;
        for (iPix=0; iPix<64; iPix=iPix+1) begin
            if(TILEHITSH[iPix]) iHit0 = iHit0[5:0] + 6'b1;
        end
        for (iPix=64; iPix<128; iPix=iPix+1) begin
            if(TILEHITSH[iPix]) iHit1 = iHit1[5:0] + 6'b1;
        end
        TCNHIT0 <= iHit0;
        TCNHIT1 <= iHit1;
        TCNHITREG <= TCNHIT0+TCNHIT1;
    end
   //Truncate Number of hit
   assign TCNHIT = TCNHITREG;

endmodule
