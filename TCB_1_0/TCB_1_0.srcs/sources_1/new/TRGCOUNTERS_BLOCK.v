`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.09.2015 11:00:39
// Design Name: 
// Module Name: TRGCOUNTERS_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TRGCOUNTERS_BLOCK(
    input TRIMOD,
    input SYNC,
    input GETRI,
    input RUNMODE,
    output [31:0] TRGCOUNTER
    );
reg [31:0] TRGCOUNTER_TEMP;
reg [31:0] TRGCOUNTER_REG;
//COUNTER INCREASED BY TRIMOD
always @(posedge TRIMOD or posedge SYNC) begin
    if(SYNC)
        TRGCOUNTER_TEMP <= 0;
    else
        TRGCOUNTER_TEMP = TRGCOUNTER_TEMP + 1;
end
//NOW LATCH THE TRGCOUNTER 
//always @(posedge GETRI) begin
always @(negedge RUNMODE) begin
    TRGCOUNTER_REG <= TRGCOUNTER_TEMP;
end
assign TRGCOUNTER = TRGCOUNTER_REG;
endmodule
