`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 10.09.2015 09:47:43
// Design Name: 
// Module Name: TRIGBLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: trigger logic and trigger signal generation (with prescaling)
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module TRIGBLOCK(
    input [1023:0] WDDATA,
    input CLK,
    input ALGCLK,
    input RUNMODE,
    output [28:0] SUM,
    //output [21:0] MAXWFM,
    output [3:0] MAX,
    output [7:0] TDCSUM,
    output [4:0] TDCNUM,
    output TCOR,
    input [127:0] TCMASKS,
    output HIT0,
    output HIT1,
    output [6:0] TILEID0,
    output [6:0] TILEID1,
    output [4:0] TILETIME0,
    output [4:0] TILETIME1,
    input [31:0] TCOMPH,
    input [31:0] TCOMPL,
    output [7:0] TCNHIT,
    input [31:0] BGOTHR,
    input [31:0] BGOVETOTHR,
    input [31:0] BGOHITDLY,
    output [31:0] BGOBITS,
    input [31:0] RDCLYSOTHR,
    input [31:0] RDCLYSOVETOTHR,
    input [95:0] RDCLYSOHITMASK,
    input [31:0] RDCHITDLY,
    input [31:0] BGOMASK,
    input [31:0] RDCMASK,
    output RDCTRG,
    output RDCPLASTICSINGLE,
    output RDCLYSOOR,
    output RDCLYSOTHRFIRE,
    output [63:0] RDCBITS,
    output BGOTRG,
    output BGOCOSM,
    output BGOTHRFIRE,
    output BGOVETOFIRE,
    output BGOPRESHCOUNTER,
    output TCLASERTRG,
    output TCDIODETRG,
    output XECLEDPMTTRG,
    output XECLEDMPPCTRG,
    output RFACCELTRG,
    output PROTONCURRENTTRG,
    output MONITORDCTRG,
    input [31:0] NGENDLY,
    input [31:0] NGENWIDTH,
    output NGENTRG,
    input [31:0] CRCHITMASK,
    input [31:0] CRCPAIRENABLE,
    output CRCSINGLETRG,
    output CRCPAIRTRG
    );

//wire RUNMODE;
/////////////////////////
// WFM PULSE HEIGHT
WFMTRG WFMTRGBLOCK(
    .WDDATA(WDDATA),
    .CLK(CLK),
    .ALGCLK(ALGCLK),
    .SUM(SUM),
    //.MAXWFM(MAXWFM),
    .MAX(MAX),
    .TDCNUM(TDCNUM),
    .TDCSUM(TDCSUM)
);

/////////////////////////
// TC SIMPLE TRIGGER
/////////////////////////
SIMPLETCTRG SIMPLETCTRGBLOCK(
    .WDDATA(WDDATA),
    .CLK(CLK),
    .TCMASKS(TCMASKS),
    .TCOR(TCOR),
    .HIT0(HIT0),
    .HIT1(HIT1),
    .TILEID0(TILEID0),
    .TILEID1(TILEID1),
    .TILETIME0(TILETIME0),
    .TILETIME1(TILETIME1),
    .TCOMPH(TCOMPH),
    .TCOMPL(TCOMPL),
    .TCNHIT(TCNHIT)
);
/////////////////////////
// AUX TRIGGERS
/////////////////////////
AUXTRG AUXTRGBLOCK(
    .WDDATA(WDDATA),
    .CLK(CLK),
    .BGOHITDLY(BGOHITDLY),
    .BGOMASK(BGOMASK),
    .RDCMASK(RDCMASK),
    .BGOTHR(BGOTHR),
    .BGOVETOTHR(BGOVETOTHR),
    .RDCLYSOTHR(RDCLYSOTHR),
    .RDCLYSOVETOTHR(RDCLYSOVETOTHR),
    .RDCLYSOHITMASK(RDCLYSOHITMASK),
    .RDCHITDLY(RDCHITDLY),
    .BGOTRG(BGOTRG),
    .BGOBITS(BGOBITS),
    .RDCBITS(RDCBITS),
    .RDCPLASTICSINGLE(RDCPLASTICSINGLE),
    .RDCLYSOOR(RDCLYSOOR),
    .RDCLYSOTHRFIRE(RDCLYSOTHRFIRE),
    .RDCTRG(RDCTRG),
    .BGOCOSM(BGOCOSM),
    .BGOTHRFIRE(BGOTHRFIRE),
    .BGOVETOFIRE(BGOVETOFIRE),
    .BGOPRESHCOUNTER(BGOPRESHCOUNTER),
    .TCLASERTRG(TCLASERTRG),
    .TCDIODETRG(TCDIODETRG),
    .XECLEDPMTTRG(XECLEDPMTTRG),
    .XECLEDMPPCTRG(XECLEDMPPCTRG),
    .RFACCELTRG(RFACCELTRG),
    .PROTONCURRENTTRG(PROTONCURRENTTRG),
    .MONITORDCTRG(MONITORDCTRG),
    .NGENDLY(NGENDLY),
    .NGENWIDTH(NGENWIDTH),
    .NGENTRG(NGENTRG),
    .CRCHITMASK(CRCHITMASK),
    .CRCPAIRENABLE(CRCPAIRENABLE),
    .CRCSINGLETRG(CRCSINGLETRG),
    .CRCPAIRTRG(CRCPAIRTRG)
);
  
endmodule
