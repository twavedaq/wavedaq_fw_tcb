`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 09/30/2016 04:32:45 PM
// Design Name: 
// Module Name: WFMTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// here we perform the sum of 16 samples and look for maximum
//////////////////////////////////////////////////////////////////////////////////

module WFMTRG(
    input [1023:0] WDDATA,
    input          CLK,
    input          ALGCLK,
    output  [28:0] SUM,
    //output  [21:0] MAXWFM,
    output   [3:0] MAX,
    output   [7:0] TDCSUM,
    output   [4:0] TDCNUM
    );

//DECODE DATA
wire [25*16-1:0] WFMFROMWD;
wire [8*16-1:0] TDCSUMFROMWD;
wire [5*16-1:0] TDCNUMFROMWD;
wire [16-1:0] ANYTDCFROMWD;

genvar islot;
for(islot = 0; islot < 16; islot = islot+1) begin
   SERDESDECODE #(
      .SLOT(islot),
      .NSLOT(16)
   ) slot_decode (
      .DATA(WDDATA),
      .WFMSUM(WFMFROMWD[25*islot+:25]),
      .TDCSUM(TDCSUMFROMWD[8*islot+:8]),
      .TDCNUM(TDCNUMFROMWD[5*islot+:5]),
      .ANYTDC(),
      .TDCHIT(),
      .TDCVAL(),
      .AUXWFMSUM(),
      .AUXWFMMAX(),
      .AUXWFMMAXVAL(),
      .DISCRSTATE()
   );
end

/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
///             THIS IS FOR THE SUM                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    

ADDERTREE #(.SIZE(25))  ADDERTREE_INST (
    .CLK(ALGCLK),
    .IN(WFMFROMWD),
    .OUT(SUM)
);

/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
///             THIS IS FOR THE MAX                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    

/*MAXFIND #(
    .CMP_LAYER(4), // number of comparison layers in tree implementation
    .MAXWFMSIZE(22),
    .MAXSIZE(0),
    .INHERIT_MAX(0),
    .TIMESIZE(12)
) MAXFIND_INST (
    .CLK(CLK),
    .MAXWFMIN(WFMFROMWD),
    .MAXIN(0),
    .TIMEIN(0),
    .MAXWFM(MAXWFM),
    .MAX(MAX),
    .TIME(TIME)
);*/

MAXFIND #(
    .CMP_LAYER(4), // number of comparison layers in tree implementation
    .MAXWFMSIZE(5),
    .MAXSIZE(0),
    .INHERIT_MAX(0),
    .SIGNED_COMPARE(0),
    .TIMESIZE(8)
) MAXFIND_INST (
    .CLK(CLK),
    .MAXWFMIN(TDCNUMFROMWD),
    .MAXIN(0),
    .TIMEIN(TDCSUMFROMWD),
    .MAXWFM(TDCNUM),
    .MAX(MAX),
    .TIME(TDCSUM)
);

endmodule


