`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.11.2016 17:03:29
// Design Name: 
// Module Name: SERDESDECODE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module decodes serdes data from TCB_1
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SERDESDECODE(
    input [64*NCONN-1:0] DATA,
    //XEC-like connection
    output [28:0] WFMSUM,
    output [3:0] MAX,
    output [7:0] TDCSUM,
    output [4:0] TDCNUM,
    //TC-like connection
    output TCOR,
    output [1:0] TCHIT,
    output [2*7-1:0] TCTILEID,
    output [2*5-1:0] TCTILETIME,
    output [7:0] TCNHIT,
    //AUX-like connection
    output CRCPAIRTRG,
    output CRCSINGLETRG,
    output BGOPRESHCOUNTER,
    output NGENTRG,
    output RDCPLASTICLYSOSINGLE,
    output BGOVETOFIRE,
    output MONITORDCTRG,
    output PROTONCURRENTTRG,
    output RFACCELTRG,
    output XECLEDMPPCTRG,
    output XECLEDPMTTRG,
    output TCDIODETRG,
    output TCLASERTRG,
    output BGOTHRFIRE,
    output BGOCOSM,
    output BGOTRG,
    output RDCPLASTICSINGLE,
    output RDCLYSOOR,
    output RDCLYSOTHRFIRE,
    output RDCTRG
    );

    parameter CONN=0;
    parameter NCONN=1;
    
    //decode XEC
    // CONNECTION SCHEME:
    //  Di :BIT 27-0 FOR SUM OF WAVEFORMs
    //     :BIT 31-28 FOR PATCH ID WITH MOST TDCs
    //     :BIT 55-48 FOR SUM OF TDCs
    //     :BIT 60-56 FOR NUM OF TDCs
    //     :BIT 63 FOR ANY TDC OVER THRESHOLD
    assign WFMSUM = DATA[64*CONN+:29];
    assign MAX = DATA[64*CONN+29+:4];
    assign TDCSUM = DATA[64*CONN+48+:8];
    assign TDCNUM = DATA[64*CONN+56+:5];

    //decode TC
    //  Di :BIT 0 FOR ANY TC TILE
    //     :BIT 1 FOR TRACK0 HIT
    //     :BIT 8-2 FOR TRACK0 ID
    //     :BIT 13-9 FOR TRACK0 TIME
    //     :BIT 14 FOR TRACK1 HIT
    //     :BIT 21-15 FOR TRACK1 ID
    //     :BIT 26-22 FOR TRACK1 TIME
    //     :BIT 34-27 FOR MULTIPLICITY
    assign TCOR = DATA[64*CONN];
    assign TCHIT = {DATA[64*CONN+14], DATA[64*CONN+1]};
    assign TCTILEID = {DATA[64*CONN+15+:7], DATA[64*CONN+2+:7]};
    assign TCTILETIME = {DATA[64*CONN+22+:5], DATA[64*CONN+9+:5]};
    assign TCNHIT = DATA[64*CONN+27+:8];

    //decode AUX
    //  Di :BIT 19-0 TRIGGER
    assign {CRCPAIRTRG, CRCSINGLETRG, BGOPRESHCOUNTER, NGENTRG,RDCPLASTICLYSOSINGLE,BGOVETOFIRE,MONITORDCTRG,PROTONCURRENTTRG,RFACCELTRG,XECLEDMPPCTRG,XECLEDPMTTRG,TCDIODETRG,TCLASERTRG,BGOTHRFIRE,BGOCOSM,BGOTRG,RDCPLASTICSINGLE,RDCLYSOOR,RDCLYSOTHRFIRE,RDCTRG} = DATA[64*CONN+:20];
    
endmodule
