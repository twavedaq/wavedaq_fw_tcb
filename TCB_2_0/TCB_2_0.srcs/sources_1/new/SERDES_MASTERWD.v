`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.02.2016 16:42:27
// Design Name: 
// Module Name: ALLSERDES
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SERDES_MASTERWD(
    input [7:0] WDB_RX0_D_P,
    input [7:0] WDB_RX0_D_N,
    input [7:0] WDB_RX1_D_P,
    input [7:0] WDB_RX1_D_N,
    input [7:0] WDB_RX2_D_P,
    input [7:0] WDB_RX2_D_N,
    input [7:0] WDB_RX3_D_P,
    input [7:0] WDB_RX3_D_N,
    input [7:0] WDB_RX4_D_P,
    input [7:0] WDB_RX4_D_N,
    input [7:0] WDB_RX5_D_P,
    input [7:0] WDB_RX5_D_N,
    input [7:0] WDB_RX6_D_P,
    input [7:0] WDB_RX6_D_N,
    input [7:0] WDB_RX7_D_P,
    input [7:0] WDB_RX7_D_N,
    input [7:0] WDB_RX8_D_P,
    input [7:0] WDB_RX8_D_N,
    input [7:0] WDB_RX9_D_P,
    input [7:0] WDB_RX9_D_N,
    input [7:0] WDB_RX10_D_P,
    input [7:0] WDB_RX10_D_N,
    input [7:0] WDB_RX11_D_P,
    input [7:0] WDB_RX11_D_N,
    input [7:0] WDB_RX12_D_P,
    input [7:0] WDB_RX12_D_N,
    input [7:0] WDB_RX13_D_P,
    input [7:0] WDB_RX13_D_N,
    input [7:0] WDB_RX14_D_P,
    input [7:0] WDB_RX14_D_N,
    input [7:0] WDB_RX15_D_P,
    input [7:0] WDB_RX15_D_N,
    input SERDES_CLK,
    input CLK,
    input MEMORYCLK,
    output [1023:0] DATA,
    output [255:0] TCB_RTX_DATA,
    input RESET,
    input [7:0] TCB_RTX_D_P,
    input [7:0] TCB_RTX_D_N,
    input [7:0] TCB_RTX1_D_P,
    input [7:0] TCB_RTX1_D_N,
    input [7:0] TCB_RTX2_D_P,
    input [7:0] TCB_RTX2_D_N,
    input [7:0] TCB_RTX3_D_P,
    input [7:0] TCB_RTX3_D_N,
    input [31:0] FCDLY,
    input REFCLK,
    input RELOADDLY,
    input [127:0] WDDLY       
    );
    wire [63:0] WD0_DATA;
    delay_serdes RX_WD0_BACKPLANE(
        .data_in_to_device(WD0_DATA),
        .data_in_from_pins_p(WDB_RX0_D_P),
        .data_in_from_pins_n(WDB_RX0_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[4:0], WDDLY[4:0],WDDLY[4:0],WDDLY[4:0],WDDLY[4:0],WDDLY[4:0],WDDLY[4:0],WDDLY[4:0]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD1_DATA;
    delay_serdes RX_WD1_BACKPLANE(
        .data_in_to_device(WD1_DATA),
        .data_in_from_pins_p(WDB_RX1_D_P),
        .data_in_from_pins_n(WDB_RX1_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[12:8], WDDLY[12:8],WDDLY[12:8],WDDLY[12:8],WDDLY[12:8],WDDLY[12:8],WDDLY[12:8],WDDLY[12:8]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );    
    wire [63:0] WD2_DATA;
    delay_serdes RX_WD2_BACKPLANE(
        .data_in_to_device(WD2_DATA),
        .data_in_from_pins_p(WDB_RX2_D_P),
        .data_in_from_pins_n(WDB_RX2_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[20:16],WDDLY[20:16],WDDLY[20:16],WDDLY[20:16],WDDLY[20:16],WDDLY[20:16],WDDLY[20:16],WDDLY[20:16]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD3_DATA;
    delay_serdes RX_WD3_BACKPLANE(
        .data_in_to_device(WD3_DATA),
        .data_in_from_pins_p(WDB_RX3_D_P),
        .data_in_from_pins_n(WDB_RX3_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[28:24],WDDLY[28:24],WDDLY[28:24],WDDLY[28:24],WDDLY[28:24],WDDLY[28:24],WDDLY[28:24],WDDLY[28:24]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD4_DATA;
    delay_serdes RX_WD4_BACKPLANE(
        .data_in_to_device(WD4_DATA),
        .data_in_from_pins_p(WDB_RX4_D_P),
        .data_in_from_pins_n(WDB_RX4_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[4+32:0+32], WDDLY[4+32:0+32], WDDLY[4+32:0+32], WDDLY[4+32:0+32], WDDLY[4+32:0+32], WDDLY[4+32:0+32], WDDLY[4+32:0+32], WDDLY[4+32:0+32]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD5_DATA;
    delay_serdes RX_WD5_BACKPLANE(
        .data_in_to_device(WD5_DATA),
        .data_in_from_pins_p(WDB_RX5_D_P),
        .data_in_from_pins_n(WDB_RX5_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[12+32:8+32],WDDLY[12+32:8+32],WDDLY[12+32:8+32],WDDLY[12+32:8+32],WDDLY[12+32:8+32],WDDLY[12+32:8+32],WDDLY[12+32:8+32],WDDLY[12+32:8+32]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );   
    wire [63:0] WD6_DATA;
    delay_serdes RX_WD6_BACKPLANE(
        .data_in_to_device(WD6_DATA),
        .data_in_from_pins_p(WDB_RX6_D_P),
        .data_in_from_pins_n(WDB_RX6_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[20+32:16+32],WDDLY[20+32:16+32],WDDLY[20+32:16+32],WDDLY[20+32:16+32],WDDLY[20+32:16+32],WDDLY[20+32:16+32],WDDLY[20+32:16+32],WDDLY[20+32:16+32]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD7_DATA;
    delay_serdes RX_WD7_BACKPLANE(
        .data_in_to_device(WD7_DATA),
        .data_in_from_pins_p(WDB_RX7_D_P),
        .data_in_from_pins_n(WDB_RX7_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[28+32:24+32], WDDLY[28+32:24+32], WDDLY[28+32:24+32], WDDLY[28+32:24+32], WDDLY[28+32:24+32], WDDLY[28+32:24+32], WDDLY[28+32:24+32], WDDLY[28+32:24+32]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD8_DATA;
    delay_serdes RX_WD8_BACKPLANE(
        .data_in_to_device(WD8_DATA),
        .data_in_from_pins_p(WDB_RX8_D_P),
        .data_in_from_pins_n(WDB_RX8_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[4+64:0+64], WDDLY[4+64:0+64], WDDLY[4+64:0+64], WDDLY[4+64:0+64], WDDLY[4+64:0+64], WDDLY[4+64:0+64], WDDLY[4+64:0+64], WDDLY[4+64:0+64]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD9_DATA;
    delay_serdes RX_WD9_BACKPLANE(
        .data_in_to_device(WD9_DATA),
        .data_in_from_pins_p(WDB_RX9_D_P),
        .data_in_from_pins_n(WDB_RX9_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[12+64:8+64],WDDLY[12+64:8+64],WDDLY[12+64:8+64],WDDLY[12+64:8+64],WDDLY[12+64:8+64],WDDLY[12+64:8+64],WDDLY[12+64:8+64],WDDLY[12+64:8+64]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    ); 
    wire [63:0] WD10_DATA;
    delay_serdes RX_WD10_BACKPLANE(
        .data_in_to_device(WD10_DATA),
        .data_in_from_pins_p(WDB_RX10_D_P),
        .data_in_from_pins_n(WDB_RX10_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[20+64:16+64],WDDLY[20+64:16+64],WDDLY[20+64:16+64],WDDLY[20+64:16+64],WDDLY[20+64:16+64],WDDLY[20+64:16+64],WDDLY[20+64:16+64],WDDLY[20+64:16+64]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD11_DATA;
    delay_serdes RX_WD11_BACKPLANE(
        .data_in_to_device(WD11_DATA),
        .data_in_from_pins_p(WDB_RX11_D_P),
        .data_in_from_pins_n(WDB_RX11_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[28+64:24+64], WDDLY[28+64:24+64], WDDLY[28+64:24+64], WDDLY[28+64:24+64], WDDLY[28+64:24+64], WDDLY[28+64:24+64], WDDLY[28+64:24+64], WDDLY[28+64:24+64]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD12_DATA;
    delay_serdes RX_WD12_BACKPLANE(
        .data_in_to_device(WD12_DATA),
        .data_in_from_pins_p(WDB_RX12_D_P),
        .data_in_from_pins_n(WDB_RX12_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[4+96:0+96], WDDLY[4+96:0+96], WDDLY[4+96:0+96], WDDLY[4+96:0+96], WDDLY[4+96:0+96], WDDLY[4+96:0+96], WDDLY[4+96:0+96], WDDLY[4+96:0+96]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD13_DATA;
    delay_serdes RX_WD13_BACKPLANE(
        .data_in_to_device(WD13_DATA),
        .data_in_from_pins_p(WDB_RX13_D_P),
        .data_in_from_pins_n(WDB_RX13_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[12+96:8+96],WDDLY[12+96:8+96],WDDLY[12+96:8+96],WDDLY[12+96:8+96],WDDLY[12+96:8+96],WDDLY[12+96:8+96],WDDLY[12+96:8+96],WDDLY[12+96:8+96]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    ); 
    wire [63:0] WD14_DATA;
    delay_serdes RX_WD14_BACKPLANE(
        .data_in_to_device(WD14_DATA),
        .data_in_from_pins_p(WDB_RX14_D_P),
        .data_in_from_pins_n(WDB_RX14_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[20+96:16+96],WDDLY[20+96:16+96],WDDLY[20+96:16+96],WDDLY[20+96:16+96],WDDLY[20+96:16+96],WDDLY[20+96:16+96],WDDLY[20+96:16+96],WDDLY[20+96:16+96]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    wire [63:0] WD15_DATA;
    delay_serdes RX_WD15_BACKPLANE(
        .data_in_to_device(WD15_DATA),
        .data_in_from_pins_p(WDB_RX15_D_P),
        .data_in_from_pins_n(WDB_RX15_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({WDDLY[28+96:24+96], WDDLY[28+96:24+96], WDDLY[28+96:24+96], WDDLY[28+96:24+96], WDDLY[28+96:24+96], WDDLY[28+96:24+96], WDDLY[28+96:24+96], WDDLY[28+96:24+96]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );

    wire [63:0] TCB_RTX_DATA_NEG;
    /*delay_serdes TCB_RTX_FRONTPANEL(
        .data_in_to_device(TCB_RTX_DATA_NEG),
        .data_in_from_pins_p(TCB_RTX_D_P),
        .data_in_from_pins_n(TCB_RTX_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({FCDLY[4:0], FCDLY[4:0],FCDLY[4:0],FCDLY[4:0],FCDLY[4:0],FCDLY[4:0],FCDLY[4:0],FCDLY[4:0]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );*/
    
    wire [63:0] TCB_RTX1_DATA_NEG;
    /*input_serdes TCB_RTX1_FRONTPLANE(
        .data_in_to_device(TCB_RTX1_DATA_NEG),
        .data_in_from_pins_p(TCB_RTX1_D_P),
        .data_in_from_pins_n(TCB_RTX1_D_N),
         .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
         .bitslip(1'b0)
    );*/
    delay_serdes TCB_RTX1_FRONTPANEL(
        .data_in_to_device(TCB_RTX1_DATA_NEG),
        .data_in_from_pins_p(TCB_RTX1_D_P),
        .data_in_from_pins_n(TCB_RTX1_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({FCDLY[12:8], FCDLY[12:8], FCDLY[12:8], FCDLY[12:8], FCDLY[12:8], FCDLY[12:8], FCDLY[12:8]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );    
    
    wire [63:0] TCB_RTX2_DATA_NEG;
/*    input_serdes TCB_RTX2_FRONTPLANE(
        .data_in_to_device(TCB_RTX2_DATA_NEG),
        .data_in_from_pins_p(TCB_RTX2_D_P),
        .data_in_from_pins_n(TCB_RTX2_D_N),
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );*/
    delay_serdes TCB_RTX2_FRONTPANEL(
        .data_in_to_device(TCB_RTX2_DATA_NEG),
        .data_in_from_pins_p(TCB_RTX2_D_P),
        .data_in_from_pins_n(TCB_RTX2_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({FCDLY[20:16], FCDLY[20:16], FCDLY[20:16], FCDLY[20:16], FCDLY[20:16], FCDLY[20:16], FCDLY[20:16], FCDLY[20:16]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );
    
    wire [63:0] TCB_RTX3_DATA_NEG;
    /*input_serdes TCB_RTX3_FRONTPLANE(
        .data_in_to_device(TCB_RTX3_DATA_NEG),
        .data_in_from_pins_p(TCB_RTX3_D_P),
        .data_in_from_pins_n(TCB_RTX3_D_N),
         .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
         .bitslip(1'b0)
    );*/
    delay_serdes TCB_RTX3_FRONTPANEL(
        .data_in_to_device(TCB_RTX3_DATA_NEG),
        .data_in_from_pins_p(TCB_RTX3_D_P),
        .data_in_from_pins_n(TCB_RTX3_D_N),
        .in_delay_reset(RELOADDLY),
        .in_delay_data_ce(5'b0),//increment-decrement enable
        .in_delay_data_inc(5'b0),//increment-decrement selector
        .in_delay_tap_in({FCDLY[28:24], FCDLY[28:24], FCDLY[28:24], FCDLY[28:24], FCDLY[28:24], FCDLY[28:24], FCDLY[28:24], FCDLY[28:24]}),//map register to delay
        .in_delay_tap_out(),//delay current value, could be used as readout in future
        .clk_in(SERDES_CLK),                            
        .clk_div_in(CLK),                        
        .io_reset(RESET),
        .bitslip(1'b0)
    );    
    
    assign TCB_RTX_DATA[63:0] = ~TCB_RTX_DATA_NEG[63:0];
    assign TCB_RTX_DATA[127:64] = ~TCB_RTX1_DATA_NEG[63:0];
    assign TCB_RTX_DATA[191:128] = ~TCB_RTX2_DATA_NEG[63:0];
    assign TCB_RTX_DATA[255:192] = ~TCB_RTX3_DATA_NEG[63:0];
    (* IODELAY_GROUP = "input_serdes_group" *)
        IDELAYCTRL
        delayctrl (
         .REFCLK (REFCLK),
         .RST    (RESET));
    
    assign DATA = {WD15_DATA, WD14_DATA, WD13_DATA, WD12_DATA, WD11_DATA, WD10_DATA, WD9_DATA, WD8_DATA, WD7_DATA, WD6_DATA, WD5_DATA, WD4_DATA, WD3_DATA, WD2_DATA, WD1_DATA, WD0_DATA};
    
endmodule
