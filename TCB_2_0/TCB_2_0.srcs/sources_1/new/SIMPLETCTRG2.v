`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.12.2016 10:31:21
// Design Name: 
// Module Name: SIMPLETCTRG2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SIMPLETCTRG2(
    input [255:0] FCDATA,
    input CLK,
    output TCOR,
    input [31:0] TCOMPH,
    input [31:0] TCOMPL,
    output [3:0] TRACKHIT,
    output [8*4-1:0] TRACKTILEID,
    output [5*4-1:0] TRACKTILETIME,
    output reg [9:0] TCNHIT
    );
    
    //DECODE SERDES WORDS
    wire [3:0] TCORINPUT;
    wire [4*8-1:0] TCNHITINPUT;
    wire [7:0] TRACKHITINPUT;
    wire [8*7-1:0] TRACKTILEIDINPUT;
    wire [8*8-1:0] TRACKTILEIDEXTENDED;
    wire [8*5-1:0] TRACKTILETIMEINPUT;
    genvar iFC;
    for (iFC=0; iFC<4; iFC = iFC+1) begin
      SERDESDECODE #(
         .CONN(iFC),
         .NCONN(4)
      ) DECODE (
         .DATA(FCDATA),
         .WFMSUM(),
         .MAX(),
         .TDCSUM(),
         .TDCNUM(),
         .TCOR(TCORINPUT[iFC]),
         .TCHIT(TRACKHITINPUT[2*iFC+:2]),
         .TCTILEID(TRACKTILEIDINPUT[2*7*iFC+:2*7]),
         .TCTILETIME(TRACKTILETIMEINPUT[2*5*iFC+:2*5]),
         .TCNHIT(TCNHITINPUT[8*iFC+:8]),
         .CRCPAIRTRG(),
         .CRCSINGLETRG(),
         .BGOPRESHCOUNTER(),
         .NGENTRG(),
         .RDCPLASTICLYSOSINGLE(),
         .BGOVETOFIRE(),
         .MONITORDCTRG(),
         .PROTONCURRENTTRG(),
         .RFACCELTRG(),
         .XECLEDMPPCTRG(),
         .XECLEDPMTTRG(),
         .TCDIODETRG(),
         .TCLASERTRG(),
         .BGOTHRFIRE(),
         .BGOCOSM(),
         .BGOTRG(),
         .RDCPLASTICSINGLE(),
         .RDCLYSOOR(),
         .RDCLYSOTHRFIRE(),
         .RDCTRG()
      );
      //extend Tile IDs
      assign TRACKTILEIDEXTENDED[2*8*iFC+:8] = (iFC==0 || iFC==2) ? {1'b0, TRACKTILEIDINPUT[2*7*iFC+:7]} : {1'b1, TRACKTILEIDINPUT[2*7*iFC+:7]};
      assign TRACKTILEIDEXTENDED[2*8*iFC+8+:8] = (iFC==0 || iFC==2) ? {1'b0, TRACKTILEIDINPUT[2*7*iFC+7+:7]} : {1'b1, TRACKTILEIDINPUT[2*7*iFC+7+:7]};
    end
    
    //OR OF TILES
    assign TCOR = |TCORINPUT;
    
    //HERE THE TRACK MERGING
    TCTrackFinder2 TCTrackFinder_InstUS(
    .CLK(CLK),
    .TRACKHITINPUT(TRACKHITINPUT[0+:4]),
    .TRACKTILEIDINPUT(TRACKTILEIDEXTENDED[0+:8*4]),
    .TRACKTILETIMEINPUT(TRACKTILETIMEINPUT[0+:5*4]),
    .TCOMPH(TCOMPH),
    .TCOMPL(TCOMPL),
    .TRACKHIT(TRACKHIT[0+:2]),
    .TRACKTILEID(TRACKTILEID[0+:8*2]),
    .TRACKTILETIME(TRACKTILETIME[0+:5*2])    
    );
    TCTrackFinder2 TCTrackFinder_InstDS(
    .CLK(CLK),
    .TRACKHITINPUT(TRACKHITINPUT[4+:4]),
    .TRACKTILEIDINPUT(TRACKTILEIDEXTENDED[8*4+:8*4]),
    .TRACKTILETIMEINPUT(TRACKTILETIMEINPUT[5*4+:5*4]),
    .TCOMPH(TCOMPH),
    .TCOMPL(TCOMPL),
    .TRACKHIT(TRACKHIT[2+:2]),
    .TRACKTILEID(TRACKTILEID[8*2+:8*2]),
    .TRACKTILETIME(TRACKTILETIME[5*2+:5*2])    
    );

    //sum up fired tiles
    reg [9*2-1:0] TCNHITSUM;
    always @(posedge CLK) begin
        TCNHITSUM[0+:9] <= TCNHITINPUT[0+:8] + TCNHITINPUT[8+:8];
        TCNHITSUM[9+:9] <= TCNHITINPUT[16+:8] + TCNHITINPUT[24+:8];
        TCNHIT <= TCNHITSUM[0+:9] + TCNHITSUM[9+:9];
    end

endmodule
