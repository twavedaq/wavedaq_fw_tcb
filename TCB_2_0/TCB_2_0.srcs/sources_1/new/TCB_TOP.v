`timescale 1ns / 1ps
// Author: Luca Galli
// TCB for pre-engineering run
//
//`default_nettype none
`include "global_parameters.vh"
module TCB_TOP( 
    //CLK INPUTS
    MAIN_CLK_P, MAIN_CLK_N,
    //INPUT TRIGGER BUS, ALWAYS USED
    SYNC_IN_P, SYNC_IN_N, TRG_IN_P, TRG_IN_N, SPARE_IN_P, SPARE_IN_N, 
    //OUTPUT TRIGGER BUS, ONLY FOR THE MASTER TCB
    //SYNC_OUT_P, SYNC_OUT_N, TRG_OUT_P, TRG_OUT_N, BUSY_IN_P, BUSY_IN_N,
    // INPUTS  FROM WAVEDREAM BOARDS, USED IN MASTER MODE
    WDB_RX0_D_P, WDB_RX0_D_N,
    WDB_RX1_D_P, WDB_RX1_D_N,
    WDB_RX2_D_P, WDB_RX2_D_N,
    WDB_RX3_D_P, WDB_RX3_D_N,
    WDB_RX4_D_P, WDB_RX4_D_N,
    WDB_RX5_D_P, WDB_RX5_D_N,
    WDB_RX6_D_P, WDB_RX6_D_N,
    WDB_RX7_D_P, WDB_RX7_D_N,
    WDB_RX8_D_P, WDB_RX8_D_N,
    WDB_RX9_D_P, WDB_RX9_D_N,
    WDB_RX10_D_P, WDB_RX10_D_N,
    WDB_RX11_D_P, WDB_RX11_D_N,
    WDB_RX12_D_P, WDB_RX12_D_N,
    WDB_RX13_D_P, WDB_RX13_D_N,
    WDB_RX14_D_P, WDB_RX14_D_N,
    WDB_RX15_D_P, WDB_RX15_D_N,
    // WDB-Like LINK TO TCB, USED IN SLAVE MODE
    WDB_TX_D_P, WDB_TX_D_N,
    // FC Connectors, USED IN SLAVE MODE
    TCB_RTX_D_P, TCB_RTX_D_N,
    TCB_RTX1_D_P, TCB_RTX1_D_N,
    TCB_RTX2_D_P, TCB_RTX2_D_N,
    TCB_RTX3_D_P, TCB_RTX3_D_N, 
    // BACKPLANE CONNECTION TO DCB
    DCB_DATA_P, DCB_DATA_N,
    DCB_READY_P, DCB_READY_N,
    // LOCAL TRIGGER BUS TO DCB
    LOC_TRIGGER_P, LOC_TRIGGER_N,
    LOC_TRG_INFO_P, LOC_TRG_INFO_N,
    //CONFIGURATION AND SINGLE WORD ACCESS SIGNALS
    SPI_CLK, SPI_MOSI, SPI_MISO, MISO_EN,
    //UTILITY FOR DATA CONCENTRATOR BOARD
    NREADY, NSELECT, NATTENTION, NFLASHSELECT,
    //FRONTPANEL LEDS DRIVERS
    LOCKCLOCK,RUNMODE,DTACK,INBUSY,LEDSPA0,LEDSPA1,
    //FPGA RESET SIGNAL
    FPGA_RESET,
    //SPARE BUS
    BUSSPARE_IN, BUSSPARE_OUT
    );
// 4 COPIES OF THE 100MHz MAIN CLOCK
input [3:0] MAIN_CLK_P,MAIN_CLK_N;
// INPUT TRIGEGR BUS FROM BACKPLANE
input SYNC_IN_P,SYNC_IN_N,TRG_IN_P,TRG_IN_N,SPARE_IN_P,SPARE_IN_N;
// OUTPUT TRIGGER BUS TO FRONTPANE, ONLY IN MASTER MODE
//output SYNC_OUT_P,SYNC_OUT_N,TRG_OUT_P,TRG_OUT_N;
//input BUSY_IN_P,BUSY_IN_N;
//DATA FROM WAVEDREAM BOARDS, ONLY IN MASTER MODE
input [7:0] WDB_RX0_D_P, WDB_RX0_D_N;
input [7:0] WDB_RX1_D_P, WDB_RX1_D_N;
input [7:0] WDB_RX2_D_P, WDB_RX2_D_N;
input [7:0] WDB_RX3_D_P, WDB_RX3_D_N;
input [7:0] WDB_RX4_D_P, WDB_RX4_D_N;
input [7:0] WDB_RX5_D_P, WDB_RX5_D_N;
input [7:0] WDB_RX6_D_P, WDB_RX6_D_N;
input [7:0] WDB_RX7_D_P, WDB_RX7_D_N;
input [7:0] WDB_RX8_D_P, WDB_RX8_D_N;
input [7:0] WDB_RX9_D_P, WDB_RX9_D_N;
input [7:0] WDB_RX10_D_P, WDB_RX10_D_N;
input [7:0] WDB_RX11_D_P, WDB_RX11_D_N;
input [7:0] WDB_RX12_D_P, WDB_RX12_D_N;
input [7:0] WDB_RX13_D_P, WDB_RX13_D_N;
input [7:0] WDB_RX14_D_P, WDB_RX14_D_N;
input [7:0] WDB_RX15_D_P, WDB_RX15_D_N;
// DATA TO MASTER TCB, ONLY IN SLAVE MODE
output [7:0] WDB_TX_D_P, WDB_TX_D_N;
// FRONT PANEL CONNECTORS
input [7:0] TCB_RTX_D_P, TCB_RTX_D_N;
input [7:0] TCB_RTX1_D_P, TCB_RTX1_D_N;
input [7:0] TCB_RTX2_D_P, TCB_RTX2_D_N;
input [7:0] TCB_RTX3_D_P, TCB_RTX3_D_N;
//BACKPLANE CONNECTION TO DCB
output DCB_DATA_P, DCB_DATA_N;
input  DCB_READY_P, DCB_READY_N;
//LOCAL TRIGGER BUS TO DCB
output LOC_TRIGGER_P, LOC_TRIGGER_N;
output LOC_TRG_INFO_P, LOC_TRG_INFO_N;
// SPI INTERFACE + SYSTEM MONITOR
input SPI_CLK, SPI_MOSI;
//NOTE THAT THE SELECT SIGNAL IS RECEIVED ON THE ATTENTION PIN FOR THE PRE-ENGINEERING RUN
output SPI_MISO, MISO_EN;
inout tri NREADY;
wire INBUSY_N;
output tri FPGA_RESET;
wire FPGA_RESET_N;
input NSELECT, NATTENTION, NFLASHSELECT;
//FRONTPANEL LEDS DRIVERS
output LOCKCLOCK,RUNMODE,DTACK,INBUSY,LEDSPA0,LEDSPA1;
//SPARE I/O BUS 
output [7:0] BUSSPARE_IN;
output [7:0] BUSSPARE_OUT;
wire [7:0] BUSSPARE_IN;
wire [7:0] BUSSPARE_OUT;
// clk bus definition
wire [1:0] ALGCLK, FASTCLK, CLK;
wire PEDCLK, SLOWCLK;
wire [3:0] PLLLOCKED;
wire BUSY_OUT;
wire MISO_EN;
// HERE THE CONTROL BLOCK WITH REGISTERS
wire SELECT;
wire [31:0] RADDR;
wire [31:0] WADDR;
wire SPI_CLK;
wire SPI_MOSI;
wire [31:0] WDATA;
tri [31:0] RDATA;
wire WENA;
wire RCLK;
wire WCLK;
wire BUSY;
wire SYSBUSY;
wire [31:0] ALGSEL;
wire SPI_MISO;
wire LDTACK;
wire RMODE;
wire BOASYNC;
wire BOASTOP;
wire RMODEMEM;
wire TRGIN;
wire SYNCIN;
wire FADCMODE;
wire TESTTXMODE;
wire [31:0] EVECOU; 
wire [31:0] LIVETIME;
wire [31:0] TOTALTIME;
wire [255:0] ALGDATA;
wire [9:0] MEMADDR;
wire [9:0] ALGCLKMEMADDR;
wire [63:0] SERDESDATAOUT;
wire PATTERNSERDES;
wire [31:0] SYS_EVECOU;
wire [`TRITYPDIM-1:0] SYS_TRITYPE;
wire [`TRGFAMNUM-1:0] SYS_ROENA;
wire SYS_VALID;
wire [31:0] TRGBUSDLY;
wire [63:0] SERDESWORD;
wire SYS_ERROR;
wire [31:0] TCOMPH;
wire [31:0] TCOMPL;
wire [31:0] ALFATHR;
wire [31:0] ALFASCALE;
wire RESETDLYCTRL;
wire [31:0] PLLRES;
wire DLYCTRLRES;
wire [31:0] RESCOU;
wire SYS_ENADC;
wire [63:0] TCBITS;
wire [63:0] ALFABITS;


wire [`NSERDESFP*64-1:0] FCDATA;
wire [7:0] DCBDATA;
wire DCBREADY;
wire [`NSERDESFP-1:0] FCSERDESMASK;
wire [7:0] BPSERDESRESET;
wire DCBSERDESRESET;
wire CALIBSTART;
wire FCSERDESMASKENABLE;
wire RESET_CALIBFSM;
wire [`NSERDESFP-1:0] FCCALIBBUSY;
wire [`NSERDESFP-1:0] FCCALIBFAIL;
wire [(`NSERDESFP)*8*5-1:0] FCCURRENTDLY;
wire [(`NSERDESFP)*8*3-1:0] FCCURRENTBISTLIP;
wire [(`NSERDESFP)*32-1:0] FCDLY_TESTED;
wire [(`NSERDESFP)*32-1:0] FCDLY_STATE;
wire CALIBMASK;
wire FCALIGNBUSY;
wire FCALIGNFAIL;
wire [`NSERDESFP*4-1:0] FCALIGNOFFSET;
wire [`NSERDESFP-1:0] FCALIGNDLY;
wire [4:0] FCSERDESMIN;
wire [7:0] SYNC_RISING_WFM;
wire [7:0] SYNC_FALLING_WFM;
wire RESET_SYNC_WFM;

////////////////////////////////////////////////////////////////////////////    
//MMCM MACRO
ALLMMCM ALLMMCM_inst(
    .clk_in_p(MAIN_CLK_P), 
    .clk_in_n(MAIN_CLK_N),              
    .clk(CLK),          
    .pedclk(PEDCLK),   
    .slowclk(SLOWCLK),  
    .reset(PLLRES[0]), 
    .locked(PLLLOCKED),
    .fastclk(FASTCLK),
    .algclk(ALGCLK),
    .resetdlyctrl(RESETDLYCTRL),
    .dlyctrlres(DLYCTRLRES)
    );
////////////////////////////////////////////////////////////////////////////    
//HERE THE SERDES 
SERDES_SLAVE SERDES_BLOCK(
    .TCB_RTX_D_P(TCB_RTX_D_P),
    .TCB_RTX_D_N(TCB_RTX_D_N),
    .TCB_RTX1_D_P(TCB_RTX1_D_P),
    .TCB_RTX1_D_N(TCB_RTX1_D_N),
    .TCB_RTX2_D_P(TCB_RTX2_D_P),
    .TCB_RTX2_D_N(TCB_RTX2_D_N),
    .TCB_RTX3_D_P(TCB_RTX3_D_P),
    .TCB_RTX3_D_N(TCB_RTX3_D_N),
    .FCDATA(FCDATA),
    .WDB_TX_D_P(WDB_TX_D_P),
    .WDB_TX_D_N(WDB_TX_D_N),
    .BPDATA(SERDESDATAOUT),
    .SERDES_CLK(FASTCLK[0]),
    .CLK(CLK[0]),
    .FCCURRENTDLY(FCCURRENTDLY),
    .FCCURRENTBISTLIP(FCCURRENTBISTLIP),
    .FCSERDESMASK(FCSERDESMASK),
    .BPSERDESRESET(BPSERDESRESET),
    .SYNC(BOASYNC),
    .CALIBSTART(CALIBSTART),
    .RESET_CALIBFSM(RESET_CALIBFSM),
    .CALIBMASK(CALIBMASK),
    .FCSERDESMASKENABLE(FCSERDESMASKENABLE),
    .PATTERNSERDES(PATTERNSERDES),
    .FCCALIBBUSY(FCCALIBBUSY),
    .FCCALIBFAIL(FCCALIBFAIL),
    .FCALIGNBUSY(FCALIGNBUSY),
    .FCALIGNFAIL(FCALIGNFAIL),
    .FCALIGNOFFSET(FCALIGNOFFSET),
    .FCSERDESMIN(FCSERDESMIN),
    .FCALIGNDLY(FCALIGNDLY),
    .FCDLY_TESTED(FCDLY_TESTED),
    .FCDLY_STATE(FCDLY_STATE)
);

SERDES_DCB DCB_BLOCK(
    .SERDES_CLK(FASTCLK[0]),
    .CLK(CLK[0]),
    .RESET(DCBSERDESRESET),
    //SERDES CONNECTIONS
    .DCB_DATA_P(DCB_DATA_P),
    .DCB_DATA_N(DCB_DATA_N),
    .DCBDATA(DCBDATA),
    //READY CONNECTIONS
    .DCB_READY_P(DCB_READY_P),
    .DCB_READY_N(DCB_READY_N),
    .DCBREADY(DCBREADY)
);

////////////////////////////////////////////////////////////////////////////    
//HERE THE INPUT MEMORY BLOCK
ALLMEM ALLMEM_BLOCK(
    .CLK(CLK[0]),
    .ALGCLK(ALGCLK[0]),
    .RUNMODE(RMODEMEM),
    .SYNC(BOASYNC), //HERE WE SHOULD USE BOASYNC
    .FADCMODE(FADCMODE),
    .TESTTXMODE(TESTTXMODE),
    .WENA(WENA),
    .RCLK(RCLK), 
    .INDATA(FCDATA),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WDATA(WDATA),
    .RDATA(RDATA),
    .ALGDATA(ALGDATA),
    .TCBITS(TCBITS),
    .ALFABITS(ALFABITS),
    .ADDROUT(MEMADDR),
    .ALGCLKADDROUT(ALGCLKMEMADDR),
    .INDATAOUT(SERDESWORD),
    .SERDESDATAOUT(SERDESDATAOUT)
);
////////////////////////////////////////////////////////////////////////////    
// HERE THE MAIN BLOCK WITHOUT TRIGGER GENERATION
MAIN MAINBLOCK(
    .FCDATA(ALGDATA),
    .CLK(CLK[0]),
    .SLOWCLK(SLOWCLK),
    .PEDCLK(PEDCLK),
    .ALGCLK(ALGCLK[0]),
    .RUNMODE(RMODE),
    .SYNC(BOASYNC), // HERE WE SHOULD USE BOASYNC
    .ALGSEL(ALGSEL),
    .LIVETIME(LIVETIME),
    .TOTALTIME(TOTALTIME),
    .EVECOU(EVECOU),
    .BOASTOP(BOASTOP),
    .SERDESWORD(SERDESWORD),
    .TCBITS(TCBITS),
    .TCOMPH(TCOMPH),
    .TCOMPL(TCOMPL),
    .ALFATHR(ALFATHR),
    .ALFABITS(ALFABITS),
    .ALFASCALE(ALFASCALE)
);

//////////////////////////////////////////////////////////////////////////    
assign SELECT = (~NSELECT) & NFLASHSELECT; //SO SELECT IS IN POSITIVE LOGIC
CONTROL CONTROL_BLOCK(
    .CLK(CLK[1]),
    .SPI_CLK(SPI_CLK),
    .SPI_MISO(SPI_MISO),
    .SPI_MOSI(SPI_MOSI),
    .SELECT(SELECT),
    .MISO_EN(MISO_EN),
    .ALGSEL(ALGSEL),
    .LIVETIME(LIVETIME),
    .TOTALTIME(TOTALTIME),
    .EVECOU(EVECOU),
    .WDATA(WDATA),
    .RDATA(RDATA),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WENA(WENA),
    .RCLK(RCLK),
    .LDTACK(LDTACK),
    .RMODE(RMODE),
    .RMODEMEM(RMODEMEM),
    .INBUSY_N(INBUSY_N),  //THIS IS NOT BUSY, to be 3-stated before being sent to the backplane
    .SYSBUSY(SYSBUSY),
    .FPGA_RESET_N(FPGA_RESET_N),
    .FADCMODE(FADCMODE),
    .TESTTXMODE(TESTTXMODE),
    .TRGIN(TRGIN),
    .SYNCIN(SYNCIN), 
    .BOASTOP(BOASTOP),
    .BOASYNC(BOASYNC),
    .MEMADDR(MEMADDR),
    .ALGCLKMEMADDR(ALGCLKMEMADDR),
    .PATTERNSERDES(PATTERNSERDES),
    .SYS_EVECOU(SYS_EVECOU),
    .SYS_TRITYPE(SYS_TRITYPE),
    .SYS_ROENA(SYS_ROENA),
    .SYS_VALID(SYS_VALID),
    .TRGBUSDLY(TRGBUSDLY),
    .SYS_ERROR(SYS_ERROR),
    .TCOMPH(TCOMPH),
    .TCOMPL(TCOMPL),
    .ALFATHR(ALFATHR),
    .ALFASCALE(ALFASCALE),
    .RESETDLYCTRL(RESETDLYCTRL),
    .PLLRES(PLLRES),
    .DLYCTRLRES(DLYCTRLRES),
    .RESCOU(RESCOU),
    .FCCURRENTBISTLIP(FCCURRENTBISTLIP),
    .FCSERDESMASK(FCSERDESMASK),
    .FCCURRENTDLY(FCCURRENTDLY),
    .BPSERDESRESET(BPSERDESRESET),
    .DCBSERDESRESET(DCBSERDESRESET),
    .CALIBSTART(CALIBSTART),
    .RESET_CALIBFSM(RESET_CALIBFSM),
    .CALIBMASK(CALIBMASK),
    .FCSERDESMASKENABLE(FCSERDESMASKENABLE),
    .FCCALIBBUSY(FCCALIBBUSY),
    .FCCALIBFAIL(FCCALIBFAIL),
    .FCALIGNBUSY(FCALIGNBUSY),
    .FCALIGNFAIL(FCALIGNFAIL),
    .FCALIGNOFFSET(FCALIGNOFFSET),
    .FCSERDESMIN(FCSERDESMIN),
    .FCALIGNDLY(FCALIGNDLY),
    .FCDLY_TESTED(FCDLY_TESTED),
    .FCDLY_STATE(FCDLY_STATE),
    .DCBDATA(DCBDATA),
    .DCBREADY(DCBREADY),
    .SYNC_RISING_WFM(SYNC_RISING_WFM),
    .SYNC_FALLING_WFM(SYNC_FALLING_WFM),
    .RESET_SYNC_WFM(RESET_SYNC_WFM),
    .SYS_ENADC(SYS_ENADC)
);
//////////////////////////////////////////////////////////////////////////    
// HERE THE TRGBUS I/O BLOCK - THIS IS IN MASTER CONFIGURATION
TRGBUS_RX TRGBUS_RX_BLOCK(
    .CLK(CLK[0]),
    .FASTCLK(FASTCLK[0]),
    .SERDES_RESET(RESET_SYNC_WFM),
    .SYNC(BOASYNC),
    .TRG_IN_P(TRG_IN_P),     //INPUT FROM BACKPLANE
    .TRG_IN_N(TRG_IN_N),     //INPUT FROM BACKPLANE
    .SYNC_IN_P(SYNC_IN_P),   //INPUT FROM BACKPLANE
    .SYNC_IN_N(SYNC_IN_N),   //INPUT FROM BACKPLANE
    .SPARE_IN_P(SPARE_IN_P),   //INPUT FROM BACKPLANE
    .SPARE_IN_N(SPARE_IN_N),   //INPUT FROM BACKPLANE
    .TRG_IN(TRGIN),         //BLOCK OUTPUT TO ALGORITHM
    .SYNC_IN(SYNCIN),       //BLOCK OUTPUT TO ALGORITHM
    .SYNC_RISING_WFM(SYNC_RISING_WFM),
    .SYNC_FALLING_WFM(SYNC_FALLING_WFM),
    .SYS_EVECOU(SYS_EVECOU),
    .SYS_VALID(SYS_VALID),
    .SYS_TRITYPE(SYS_TRITYPE),
    .SYS_ROENA(SYS_ROENA),
    .TRGBUSDLY(TRGBUSDLY[15:0]),
    .SYS_ERROR(SYS_ERROR),
    .SYS_ENADC(SYS_ENADC)
);
//BUSY TRISTATE
// if the busy is met (INBUSY_N low) then the 3-state is transparent
// else high impedance
wire SYSBUSY_N;
IOBUF IOBUF_BUSY3STATE (
   .IO(NREADY),
   .O(SYSBUSY_N),
   .I(INBUSY_N),
   .T(INBUSY_N)
);
assign SYSBUSY = ~SYSBUSY_N;

//FPGA_RESET TRISTATE
OBUFT OBUF_RESET3STATE (
   .O(FPGA_RESET),
   .I(FPGA_RESET_N),
   .T(FPGA_RESET_N)
);

// HERE THE LED CONNECTIONS
OBUF OBUF_LEDLOCK (
   .O(LOCKCLOCK),     // Buffer output (connect directly to top-level port)
   .I(&PLLLOCKED)      // Buffer input 
);
OBUF OBUF_LEDDTACK (
   .O(DTACK),     // Buffer output (connect directly to top-level port)
   .I(LDTACK)      // Buffer input 
);
OBUF OBUF_LEDRMOD (
   .O(RUNMODE),     // Buffer output (connect directly to top-level port)
   .I(RMODE)      // Buffer input 
);
OBUF OBUF_LEDBUSY (
   .O(INBUSY),     // Buffer output (connect directly to top-level port)
   .I(~INBUSY_N)      // Buffer input 
);
OBUF OBUF_LEDS0 (
   .O(LEDSPA0),     // Buffer output (connect directly to top-level port)
   .I(FADCMODE)      // Buffer input 
);
OBUF OBUF_LEDS1 (
   .O(LEDSPA1),     // Buffer output (connect directly to top-level port)
   .I(SYSBUSY)      // Buffer input 
);
// BUSPARE OUT 
BUSSPARE BUSSPAREOUT(
.IN(0),  //0
.OUT(BUSSPARE_OUT)
);
BUSSPARE BUSSPAREIN(
.IN({4'b0, INBUSY_N, RMODE, TRGIN, SYNCIN}),
.OUT(BUSSPARE_IN)
);

////////////////////////////////////////////////////////////////////////////    
// I/OBUFDS FOR UNUSED SIGNALS
OBUFDS #(
   .IOSTANDARD("LVDS_25") // Specify the output I/O standard
) OBUFDS_LOC_TRIGGER (
   .O(LOC_TRIGGER_P),    // Diff_p output (connect directly to top-level port)
   .OB(LOC_TRIGGER_N),   // Diff_n output (connect directly to top-level port)
   .I(1'b0)  // Buffer input 
);
OBUFDS #(
   .IOSTANDARD("LVDS_25") // Specify the output I/O standard
) OBUFDS_LOC_TRG_INFO (
   .O(LOC_TRG_INFO_P),    // Diff_p output (connect directly to top-level port)
   .OB(LOC_TRG_INFO_N),   // Diff_n output (connect directly to top-level port)
   .I(1'b0)  // Buffer input 
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB0_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX0_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX0_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB1_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX1_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX1_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB2_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX2_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX2_D_N) // Diff_n buffer input (connect directly to top-level port)
);


IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB3_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX3_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX3_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB4_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX4_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX4_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB5_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX5_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX5_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB6_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX6_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX6_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB7_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX7_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX7_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB8_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX8_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX8_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB9_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX9_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX9_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB10_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX10_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX10_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB11_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX11_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX11_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB12_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX12_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX12_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB13_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX13_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX13_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB14_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX14_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX14_D_N) // Diff_n buffer input (connect directly to top-level port)
);

IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDSWDB15_inst[7:0] (
    .O(),  // Buffer output
    .I(WDB_RX15_D_P),  // Diff_p buffer input (connect directly to top-level port)
    .IB(WDB_RX15_D_N) // Diff_n buffer input (connect directly to top-level port)
);

//OBUFDS #(
//   .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
//   .SLEW("SLOW")           // Specify the output slew rate
//) OBUFDS_WDB_TX_D_P[7:0] (
//   .O(WDB_TX_D_P),     // Diff_p output (connect directly to top-level port)
//   .OB(WDB_TX_D_N),   // Diff_n output (connect directly to top-level port)
//   .I(8'b0000)      // Buffer input 
//);

//IBUFDS #(
//    .DIFF_TERM("FALSE"),       // Differential Termination
//    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
//    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
//) IBUFDSRTX_inst[7:0] (
//    .O(),  // Buffer output
//    .I(TCB_RTX_D_P),  // Diff_p buffer input (connect directly to top-level port)
//    .IB(TCB_RTX_D_N) // Diff_n buffer input (connect directly to top-level port)
//);

//IBUFDS #(
//    .DIFF_TERM("FALSE"),       // Differential Termination
//    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
//    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
//) IBUFDSRTX1_inst[7:0] (
//    .O(),  // Buffer output
//    .I(TCB_RTX1_D_P),  // Diff_p buffer input (connect directly to top-level port)
//    .IB(TCB_RTX1_D_N) // Diff_n buffer input (connect directly to top-level port)
//);

//IBUFDS #(
//    .DIFF_TERM("FALSE"),       // Differential Termination
//    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
//    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
//) IBUFDSRTX2_inst[7:0] (
//    .O(),  // Buffer output
//    .I(TCB_RTX2_D_P),  // Diff_p buffer input (connect directly to top-level port)
//    .IB(TCB_RTX2_D_N) // Diff_n buffer input (connect directly to top-level port)
//);

//IBUFDS #(
//    .DIFF_TERM("FALSE"),       // Differential Termination
//    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
//    .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
//) IBUFDSRTX3_inst[7:0] (
//    .O(),  // Buffer output
//    .I(TCB_RTX3_D_P),  // Diff_p buffer input (connect directly to top-level port)
//    .IB(TCB_RTX3_D_N) // Diff_n buffer input (connect directly to top-level port)
//);

endmodule
