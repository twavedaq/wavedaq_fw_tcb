`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 10.09.2015 09:47:43
// Design Name: 
// Module Name: TRIGBLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: trigger logic and trigger signal generation (with prescaling)
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module TRIGBLOCK(
    input [255:0] FCDATA,
    input CLK,
    input PEDCLK,
    input ALGCLK,
    input RUNMODE,
    input SYNC,
    output [30:0] SUM,
    output [30:0] PMTSUM,
    //output [21:0] MAXWFM,
    output [5:0] MAX,
    output [4:0] TDCNUM,
    output [7:0] TDCSUM,
    output TCOR,
    input [31:0] TCOMPH,
    input [31:0] TCOMPL,
    output [3:0] TRACKHIT,
    output [8*4-1:0] TRACKTILEID,
    output [5*4-1:0] TRACKTILETIME,
    output [9:0] TCNHIT,
    output [63:0] TCBITS,
    output RDCTRG,
    output RDCPLASTICSINGLE,
    output RDCLYSOOR,
    output RDCLYSOTHRFIRE,
    output BGOTRG,
    output BGOCOSM,
    output BGOTHRFIRE,
    output BGOPRESHCOUNTER,
    output TCLASERTRG,
    output TCDIODETRG,
    output XECLEDPMTTRG,
    output XECLEDMPPCTRG,
    output RFACCELTRG,
    output PROTONCURRENTTRG,
    output MONITORDCTRG,
    output BGOVETOFIRE,
    output RDCPLASTICLYSOSINGLE,
    output NGENTRG,
    output CRCSINGLETRG,
    output CRCPAIRTRG,
    input [31:0] ALFATHR,
    input [31:0] ALFASCALE,
    output reg [63:0] ALFABITS,
    output ALFATRG
    );
//wire RUNMODE;
/////////////////////////
// WFM PULSE HEIGHT
WFMTRG WFMTRGBLOCK(
    .FCDATA(FCDATA),
    .ALGCLK(ALGCLK),
    .CLK(CLK),
    .SUM(SUM),
//    .MAXWFM(MAXWFM),
    .MAX(MAX),
    .TDCNUM(TDCNUM),
    .TDCSUM(TDCSUM),
    .PMTSUM(PMTSUM)
);

/////////////////////////
// TC TRIGGER

SIMPLETCTRG2 SIMPLETCTRGBLOCK(
    .FCDATA(FCDATA),
    .CLK(CLK),
    .TCOR(TCOR),
    .TCOMPH(TCOMPH),
    .TCOMPL(TCOMPL),
    .TRACKHIT(TRACKHIT),
    .TRACKTILEID(TRACKTILEID),
    .TRACKTILETIME(TRACKTILETIME),
    .TCNHIT(TCNHIT)
);

assign TCBITS = {TCNHIT[6:0], TRACKTILETIME[5*3+:5], TRACKTILEID[8*3+:8], TRACKHIT[3],TRACKTILETIME[5*2+:5], TRACKTILEID[8*2+:8], TRACKHIT[2],TRACKTILETIME[5*1+:5], TRACKTILEID[8*1+:8], TRACKHIT[1],TRACKTILETIME[5*0+:5], TRACKTILEID[8*0+:8], TRACKHIT[0], TCOR};


/////////////////////////
// ALFA TRIGGER AND PMT SUM
// NB: removed MSB bit to fit in a DSP 
wire [3:0] ALFAISMAXBITS;
wire [32:0] ALFACHARGE;
wire [29:0] ALFAPEAK;
wire [47:0] ALFAQOVERA;
ALFATRG ALFATRGBLOCK(
    .CLK(CLK),
    .ALGCLK(ALGCLK),
    .PMTSUM(PMTSUM[29:0]),
    .THR(ALFATHR),
    .SCALE(ALFASCALE),
    .ISMAXBITS(ALFAISMAXBITS),
    .CHARGE(ALFACHARGE),
    .PEAK(ALFAPEAK),
    .QOVERA(ALFAQOVERA),
    .TRG(ALFATRG)
);
always @(posedge ALGCLK) begin
   ALFABITS = {ALFATRG,ALFACHARGE[30:0],ALFAISMAXBITS, ALFAPEAK[27:0]};
end


/////////////////////////
// AUX DECODING TRIGGER

AUXTRG AUXTRGBLOCK(
    .FCDATA(FCDATA),
    .CLK(CLK),
    .BGOCOSM(BGOCOSM),
    .BGOTHRFIRE(BGOTHRFIRE),
    .BGOPRESHCOUNTER(BGOPRESHCOUNTER),
    .BGOTRG(BGOTRG),
    .RDCPLASTICSINGLE(RDCPLASTICSINGLE),
    .RDCLYSOOR(RDCLYSOOR),
    .RDCLYSOTHRFIRE(RDCLYSOTHRFIRE),
    .RDCTRG(RDCTRG),
    .TCLASERTRG(TCLASERTRG),
    .TCDIODETRG(TCDIODETRG),
    .XECLEDPMTTRG(XECLEDPMTTRG),
    .XECLEDMPPCTRG(XECLEDMPPCTRG),
    .RFACCELTRG(RFACCELTRG),
    .PROTONCURRENTTRG(PROTONCURRENTTRG),
    .MONITORDCTRG(MONITORDCTRG),
    .BGOVETOFIRE(BGOVETOFIRE),
    .RDCPLASTICLYSOSINGLE(RDCPLASTICLYSOSINGLE),
    .NGENTRG(NGENTRG),
    .CRCSINGLETRG(CRCSINGLETRG),
    .CRCPAIRTRG(CRCPAIRTRG)
);

endmodule
