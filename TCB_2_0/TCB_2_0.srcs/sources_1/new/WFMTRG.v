`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 09/30/2016 04:32:45 PM
// Design Name: 
// Module Name: WFMTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// here we perform the sum of 16 samples and compare it with a threshold
//////////////////////////////////////////////////////////////////////////////////


module WFMTRG(
    input [255:0] FCDATA,
    input CLK,
    input ALGCLK,
    output [30:0] SUM,
    //output [21:0] MAXWFM,
    output [5:0] MAX,
    output [4:0] TDCNUM,
    output [7:0] TDCSUM,
    output [30:0] PMTSUM
    );
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
///             THIS IS FOR THE SUM                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    

genvar iserdes;
wire [29*4-1:0] WFMFROMTCB;
wire [4*4-1:0] MAXFROMTCB;
//wire [22*4-1:0] MAXWFMFROMTCB;
wire [4*8-1:0] TDCSUMFROMTCB;
wire [4*5-1:0] TDCNUMFROMTCB;

for(iserdes = 0; iserdes < 4; iserdes = iserdes+1) begin
   SERDESDECODE #(
      .CONN(iserdes),
      .NCONN(4)
   ) DECODE (
      .DATA(FCDATA),
      .WFMSUM(WFMFROMTCB[29*iserdes+:29]),
      .MAX(MAXFROMTCB[4*iserdes+:4]),
      .TDCSUM(TDCSUMFROMTCB[8*iserdes+:8]),
      .TDCNUM(TDCNUMFROMTCB[5*iserdes+:5]),
      .TCOR(),
      .TCHIT(),
      .TCTILEID(),
      .TCTILETIME(),
      .TCNHIT(),
      .CRCPAIRTRG(),
      .CRCSINGLETRG(),
      .BGOPRESHCOUNTER(),
      .NGENTRG(),
      .RDCPLASTICLYSOSINGLE(),
      .BGOVETOFIRE(),
      .MONITORDCTRG(),
      .PROTONCURRENTTRG(),
      .RFACCELTRG(),
      .XECLEDMPPCTRG(),
      .XECLEDPMTTRG(),
      .TCDIODETRG(),
      .TCLASERTRG(),
      .BGOTHRFIRE(),
      .BGOCOSM(),
      .BGOTRG(),
      .RDCPLASTICSINGLE(),
      .RDCLYSOOR(),
      .RDCLYSOTHRFIRE(),
      .RDCTRG()
   );
end

/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
///             THIS IS FOR THE SUM                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    

   QUADRUPLESUM #(
        .SIZE(29),
        .REGIN(1),
        .REGOUT(1)
    ) QUADRUPLESUM_INST (
        .CLK(ALGCLK),
        .IN(WFMFROMTCB),
        .OUT(SUM)
    );
    QUADRUPLESUM #(
         .SIZE(29),
         .REGIN(1),
         .REGOUT(1)
    ) SUMPMT_INST (
         .CLK(ALGCLK),
         .IN({29'b0, WFMFROMTCB[0+:29*3]}),
         .OUT(PMTSUM)
    );

/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
///             THIS IS FOR THE MAX                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    

MAXFIND #(
    .CMP_LAYER(2), // number of comparison layers in tree implementation
    .MAXWFMSIZE(5),
    .MAXSIZE(4),
    .SIGNED_COMPARE(0),
    .TIMESIZE(8)
) MAXFIND_INST (
    .CLK(ALGCLK),
    .MAXWFMIN(TDCNUMFROMTCB),
    .MAXIN(MAXFROMTCB),
    .TIMEIN(TDCSUMFROMTCB),
    .MAXWFM(TDCNUM),
    .MAX(MAX),
    .TIME(TDCSUM)
);

//genvar icmpzero;
//wire [9:0] FIRSTMAX;
//wire signed [22*2-1:0] OUTFIRSTCMP;

//for(icmpzero = 0; icmpzero < 2; icmpzero = icmpzero + 1) begin
//    CMP_MAX_BLOCK #(
//    .MAXWIDTH(5)
//    ) CMP_MAX_INST(
//    .WFMA(MAXWFMFROMTCB[22*icmpzero*2+:22]),
//    .WFMB(MAXWFMFROMTCB[22*(icmpzero*2+1)+:22]),
//    .MAXA({1'b0, MAXFROMTCB[4*icmpzero*2+:4]}),
//    .MAXB({1'b1, MAXFROMTCB[4*(icmpzero*2+1)+:4]}),
//    .CLK(CLK),
//    .WFMO(OUTFIRSTCMP[22*icmpzero+:22]),
//    .MAXO(FIRSTMAX[5*icmpzero+:5])
//    );
//end

//CMP_MAX_BLOCK #(
//.MAXWIDTH(6)
//) FINAL_CMP_MAX_INST(
//.WFMA(OUTFIRSTCMP[0+:22]),
//.WFMB(OUTFIRSTCMP[22+:22]),
//.MAXA({1'b0,FIRSTMAX[0+:5]}),
//.MAXB({1'b1,FIRSTMAX[5+:5]}),
//.CLK(CLK),
//.WFMO(MAXWFM),
//.MAXO(MAX)
//);

// TEMPORARELY TIME IS NOT PROPAGATED
//assign TIME = 0;

//// THIS STAYS AS A FUTURE OPTION
////constins all comparisons [3>2?,3>1?,2>1?,3>0?,2>0?,1>0?]
//wire [5:0] CMP;

//reg [255:0] FCDATAREG;

//// make comparisons
////always @(posedge CLK) begin
//  assign  CMP[0]    = $signed(MAXWFMFROMTCB[21+(22*1):22]) > $signed(MAXWFMFROMTCB[21:0]);
//  assign  CMP[1]    = $signed(MAXWFMFROMTCB[21+(22*2):22*2]) > $signed(MAXWFMFROMTCB[21:0]);
//  assign  CMP[2]    = $signed(MAXWFMFROMTCB[21+(22*3):22*3]) > $signed(MAXWFMFROMTCB[21:0]);
//  assign  CMP[3]    = $signed(MAXWFMFROMTCB[21+(22*2):22*2]) > $signed(MAXWFMFROMTCB[21+(22*1):22]);
//  assign  CMP[4]    = $signed(MAXWFMFROMTCB[21+(22*3):22*3]) > $signed(MAXWFMFROMTCB[21+(22*1):22]);
//  assign  CMP[5]    = $signed(MAXWFMFROMTCB[21+(22*3):22*3]) > $signed(MAXWFMFROMTCB[21+(22*2):22*2]);
    
//    // pipeline the values
//    //FCDATAREG<=FCDATA;
////end

////select right data
//always @(posedge CLK) begin
//    casez(CMP)
//        6'b???000: begin
//            //max 0
//            MAX <= {2'b00, MAXFROMTCB[4*0+:4]};
//            MAXWFM <= MAXWFMFROMTCB[22*0+:22];
////            TIME <= FCDATA[62+(64*0):51+(64*0)];
//        end
//        6'b?00??1: begin
//            //max 1
//            MAX <= {2'b01, MAXFROMTCB[4*1+:4]};
//            MAXWFM <= MAXWFMFROMTCB[22*1+:22];
////            TIME <= FCDATA[62+(64*1):51+(64*1)];
//        end
//        6'b0?1?1?: begin
//            //max 2
//            MAX <= {2'b10, MAXFROMTCB[4*2+:4]};
//            MAXWFM <= MAXWFMFROMTCB[22*2+:22];
////            TIME <= FCDATA[62+(64*2):51+(64*2)];
//        end
//        6'b11?1??: begin
//            //max 2
//            MAX <= {2'b11, MAXFROMTCB[4*3+:4]};
//            MAXWFM <= MAXWFMFROMTCB[22*3+:22];
////            TIME <= FCDATA[62+(64*3):51+(64*3)];
//        end
//        default: begin
//        //should never go here
//            MAX <= 0;
//            MAXWFM <= 0;
////            TIME <= 0;
//        end
//    endcase
//end

endmodule


