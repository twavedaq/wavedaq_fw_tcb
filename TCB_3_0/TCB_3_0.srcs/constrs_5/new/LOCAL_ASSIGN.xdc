###################################################################
#trigger bus signals to the front panel 

#BUSY_IN	LVDS	2	Output (THIS HAS OPPOSITE DIRECTION)		
set_property IOSTANDARD LVDS [get_ports BUSY_IN_P]
set_property DIFF_TERM TRUE [get_ports BUSY_IN_P]
set_property IOSTANDARD LVDS [get_ports BUSY_IN_N]
set_property DIFF_TERM TRUE [get_ports BUSY_IN_N]
set_property PACKAGE_PIN AE1 [get_ports BUSY_IN_P]
set_property PACKAGE_PIN AF1 [get_ports BUSY_IN_N]

#SYNC_OUT	LVDS	2	Output		
set_property IOSTANDARD LVDS [get_ports SYNC_OUT_P]
set_property DIFF_TERM FALSE [get_ports SYNC_OUT_P]
set_property IOSTANDARD LVDS [get_ports SYNC_OUT_N]
set_property DIFF_TERM FALSE [get_ports SYNC_OUT_N]
set_property PACKAGE_PIN AE5 [get_ports SYNC_OUT_P]
set_property PACKAGE_PIN AF5 [get_ports SYNC_OUT_N]
#prior of Jan 2019
#set_property PACKAGE_PIN AC2 [get_ports SYNC_OUT_P]
#set_property PACKAGE_PIN AC1 [get_ports SYNC_OUT_N]

#TRG_OUT	LVDS	2	Output		
#this is a serial signal with the trigger signal + eve num + trig type + etc encoded
set_property IOSTANDARD LVDS [get_ports TRG_OUT_P]
set_property DIFF_TERM FALSE [get_ports TRG_OUT_P]
set_property IOSTANDARD LVDS [get_ports TRG_OUT_N]
set_property DIFF_TERM FALSE [get_ports TRG_OUT_N]
set_property PACKAGE_PIN AC5 [get_ports TRG_OUT_P]
set_property PACKAGE_PIN AC4 [get_ports TRG_OUT_N]

#SPARE_OUT     LVDS    2       Output          
#this is a serial signal with the trigger signal + eve num + trig type + etc encoded
set_property IOSTANDARD LVDS [get_ports SPARE_OUT_P]
set_property DIFF_TERM FALSE [get_ports SPARE_OUT_P]
set_property IOSTANDARD LVDS [get_ports SPARE_OUT_N]
set_property DIFF_TERM FALSE [get_ports SPARE_OUT_N]
set_property PACKAGE_PIN AC2 [get_ports SPARE_OUT_P]
set_property PACKAGE_PIN AC1 [get_ports SPARE_OUT_N]
#prior of Jan 2019
#set_property PACKAGE_PIN AE5 [get_ports SPARE_OUT_P]
#set_property PACKAGE_PIN AF5 [get_ports SPARE_OUT_N]

#WDB_RTX1 LVDS 16 Output, TC1 Receiver (used in slave mode)
#01-10-2018 MODIFIED AS INTERFACE TO EXTRENAL DAQ
# RTX[3:0] --> RX[3:0];   RTX[7:4] --> TX[3:0]
set_property IOSTANDARD LVDS [get_ports {TCB_RX1_D_P[*]}]
set_property IOSTANDARD LVDS [get_ports {TCB_RX1_D_N[*]}]
set_property IOSTANDARD LVDS [get_ports {TCB_TX1_D_P[*]}]
set_property IOSTANDARD LVDS [get_ports {TCB_TX1_D_N[*]}]
set_property DIFF_TERM true [get_ports {TCB_RX1_D_P[*]}]
set_property DIFF_TERM true [get_ports {TCB_RX1_D_N[*]}]
set_property PACKAGE_PIN AJ6 [get_ports {TCB_RX1_D_P[0]}]
set_property PACKAGE_PIN AK6 [get_ports {TCB_RX1_D_N[0]}]
set_property PACKAGE_PIN AK5 [get_ports {TCB_RX1_D_P[1]}]
set_property PACKAGE_PIN AK4 [get_ports {TCB_RX1_D_N[1]}]
set_property PACKAGE_PIN AJ3 [get_ports {TCB_RX1_D_P[2]}]
set_property PACKAGE_PIN AK3 [get_ports {TCB_RX1_D_N[2]}]
set_property PACKAGE_PIN AH2 [get_ports {TCB_RX1_D_P[3]}]
set_property PACKAGE_PIN AJ2 [get_ports {TCB_RX1_D_N[3]}]
set_property PACKAGE_PIN AF7 [get_ports {TCB_TX1_D_P[0]}]
set_property PACKAGE_PIN AG7 [get_ports {TCB_TX1_D_N[0]}]
set_property PACKAGE_PIN AG4 [get_ports {TCB_TX1_D_P[1]}]
set_property PACKAGE_PIN AG3 [get_ports {TCB_TX1_D_N[1]}]
set_property PACKAGE_PIN AG2 [get_ports {TCB_TX1_D_P[2]}]
set_property PACKAGE_PIN AH1 [get_ports {TCB_TX1_D_N[2]}]
set_property PACKAGE_PIN AH6 [get_ports {TCB_TX1_D_P[3]}]
set_property PACKAGE_PIN AH5 [get_ports {TCB_TX1_D_N[3]}]


#WDB_RTX2 LVDS 16 Output, TC2 Receiver (used in slave mode)
set_property IOSTANDARD LVDS [get_ports {TCB_RTX2_D_P[*]}]
set_property IOSTANDARD LVDS [get_ports {TCB_RTX2_D_N[*]}]
set_property DIFF_TERM true [get_ports {TCB_RTX2_D_P[*]}]
set_property DIFF_TERM true [get_ports {TCB_RTX2_D_N[*]}]
set_property PACKAGE_PIN AD8 [get_ports {TCB_RTX2_D_P[0]}]
set_property PACKAGE_PIN AE8 [get_ports {TCB_RTX2_D_N[0]}]
set_property PACKAGE_PIN AD9 [get_ports {TCB_RTX2_D_P[1]}]
set_property PACKAGE_PIN AE9 [get_ports {TCB_RTX2_D_N[1]}]
set_property PACKAGE_PIN AG9 [get_ports {TCB_RTX2_D_P[2]}]
set_property PACKAGE_PIN AH9 [get_ports {TCB_RTX2_D_N[2]}]
set_property PACKAGE_PIN AJ9 [get_ports {TCB_RTX2_D_P[3]}]
set_property PACKAGE_PIN AK9 [get_ports {TCB_RTX2_D_N[3]}]
set_property PACKAGE_PIN AA11 [get_ports {TCB_RTX2_D_P[4]}]
set_property PACKAGE_PIN AA10 [get_ports {TCB_RTX2_D_N[4]}]
set_property PACKAGE_PIN AB10 [get_ports {TCB_RTX2_D_P[5]}]
set_property PACKAGE_PIN AC10 [get_ports {TCB_RTX2_D_N[5]}]
set_property PACKAGE_PIN AE10 [get_ports {TCB_RTX2_D_P[6]}]
set_property PACKAGE_PIN AF10 [get_ports {TCB_RTX2_D_N[6]}]
set_property PACKAGE_PIN AG10 [get_ports {TCB_RTX2_D_P[7]}]
set_property PACKAGE_PIN AH10 [get_ports {TCB_RTX2_D_N[7]}]

#WDB_RTX3 LVDS 16 Output, TC3 Receiver (used in slave mode)
set_property IOSTANDARD LVDS [get_ports {TCB_RTX3_D_P[*]}]
set_property IOSTANDARD LVDS [get_ports {TCB_RTX3_D_N[*]}]
set_property DIFF_TERM true [get_ports {TCB_RTX3_D_P[*]}]
set_property DIFF_TERM true [get_ports {TCB_RTX3_D_N[*]}]
set_property PACKAGE_PIN AA12 [get_ports {TCB_RTX3_D_P[0]}]
set_property PACKAGE_PIN AB12 [get_ports {TCB_RTX3_D_N[0]}]
set_property PACKAGE_PIN AC12 [get_ports {TCB_RTX3_D_P[1]}]
set_property PACKAGE_PIN AC11 [get_ports {TCB_RTX3_D_N[1]}]
set_property PACKAGE_PIN AH11 [get_ports {TCB_RTX3_D_P[2]}]
set_property PACKAGE_PIN AJ11 [get_ports {TCB_RTX3_D_N[2]}]
set_property PACKAGE_PIN AK11 [get_ports {TCB_RTX3_D_P[3]}]
set_property PACKAGE_PIN AK10 [get_ports {TCB_RTX3_D_N[3]}]
set_property PACKAGE_PIN AF12 [get_ports {TCB_RTX3_D_P[4]}]
set_property PACKAGE_PIN AG12 [get_ports {TCB_RTX3_D_N[4]}]
set_property PACKAGE_PIN AE13 [get_ports {TCB_RTX3_D_P[5]}]
set_property PACKAGE_PIN AF13 [get_ports {TCB_RTX3_D_N[5]}]
set_property PACKAGE_PIN AG13 [get_ports {TCB_RTX3_D_P[6]}]
set_property PACKAGE_PIN AH12 [get_ports {TCB_RTX3_D_N[6]}]
set_property PACKAGE_PIN AE11 [get_ports {TCB_RTX3_D_P[7]}]
set_property PACKAGE_PIN AF11 [get_ports {TCB_RTX3_D_N[7]}]

########################################################################
#timespec

set_false_path -from [get_cells {CONTROL_BLOCK/.*/VALUE_reg[[0-9]*]} -regexp -nocase -hierarchical] -to [get_cells {MAINBLOCK.*_reg[[0-9]*]} -regexp -hierarchical]

set_property LOC USR_ACCESS_X0Y0 [get_cells CONTROL_BLOCK/USR_ACCESS_7series_inst]
