`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/14/2020 10:16:08 PM
// Design Name: 
// Module Name: PATCHTRG_SIM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PATCHTRG_TB(

    );
    
    reg ALGCLK;
    reg CLK;
    //inputs from WFMTRG
    reg [4:0] TDCNUM;
    reg [7:0] MAX;
    //configurations
    reg [31:0] RLXe_PATCH;
    reg [31:0] RLXe_HITTHR;
    //bus
    reg [31:0] RADDR;
    reg [31:0] WADDR;
    reg [31:0] WDATA;
    reg RCLK;
    wire [31:0] RDATA;
    reg WENA;
    //outputs
    wire PATCHTRG;
    wire XECMAX_VALID;
    wire [7:0] XECMAX;
    
    PATCHTRG DUT(
    .ALGCLK(ALGCLK),
    .CLK(CLK),
    //inputs from WFMTRG
    .TDCNUM(TDCNUM),
    .MAX(MAX),
    //configurations
    .RLXe_PATCH(RLXe_PATCH),
    .RLXe_HITTHR(RLXe_HITTHR),
    //bus
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WDATA(WDATA),
    .RCLK(RCLK),
    .RDATA(RDATA),
    .WENA(WENA),
    //outputs
    .PATCHTRG(PATCHTRG),
    .XECMAX_VALID(XECMAX_VALID),
    .XECMAX(XECMAX)
);
  
  always begin
  #5 ALGCLK <= 1'b1;
  CLK<=1'b0;
  #5 ALGCLK <= 1'b0;
  #5 ALGCLK <= 1'b1;
  CLK<=1'b1;
  #5 ALGCLK <= 1'b0;
  end
  
  initial begin
    ALGCLK <= 1'b0;
    CLK <= 1'b1;
    #5 TDCNUM <= 5'b0;
    MAX <= 8'b0;
    //configurations
    RLXe_PATCH <=32'h1;
    RLXe_HITTHR <= 32'h0;
    #20 MAX <=32'h1;
    #20 MAX <=32'h2;
    #20 MAX <= 32'h1;
    TDCNUM <= 32'h1;
    #20 MAX <= 32'h2;
    TDCNUM <= 32'h1;   
    #20 MAX <= 32'h1;
    #400 $finish;
  end
    
endmodule
