`timescale 10ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.07.2020 12:30:01
// Design Name: 
// Module Name: READOUT_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module READOUT_TB(
    );
    
    reg CLK;
    reg DCBREADY;
    reg BUFFERBUSY;
    reg [31:0] BUFFERDATA;
    reg ENABLE;
    reg READYIGNORE;
    reg [15:0] PAYLOAD_MAX;
    
    wire [7:0] DCBDATA;
    wire [31:0] BUFFERADDR;
    wire NEXTBUFFER;

    
    READOUTFSM #(
        .BOARD_TYPE(8'h05)
    )READOUT_BLOCK(
        .CLK(CLK),
    // DCB interface
        .DCBDATA(DCBDATA),
        .DCBREADY(DCBREADY),
    // Buffer interface
        .BUFFERBUSY(BUFFERBUSY),
        .NEXTBUFFER(NEXTBUFFER),
        .BUFFERADDR(BUFFERADDR),
        .BUFFERDATA(BUFFERDATA),
    // flags
        .ENABLE(ENABLE),
        .READYIGNORE(READYIGNORE),
        .PAYLOAD_MAX(PAYLOAD_MAX),
    //header data
        .BOARD_REVISION(4'h4),
        .SERIAL_NUMBER(16'hDEAD),
        .CRATE_ID(8'h1),
        .SLOT_ID(8'h10)
    );
    
   parameter PERIOD = 10;
 
    always begin
       CLK = 1'b0;
       #(PERIOD/2) CLK = 1'b1;
       #(PERIOD/2);
    end
    
    
    // High level simulation of buffer memeory
    reg [31:0] mem [31:0];
    
    initial begin
        mem[0] = 32'h3;//NBANKS
        mem[1] = 32'h1;//EVECOU
        mem[2] = 32'h10;//TOTALTIME
        mem[3] = 32'h1;//SYS
        mem[4] = 32'hFFFF1000;//SYS
        mem[5] = 32'h444d4d59;//BANK NAME (DMMY)
        mem[6] = 32'b0; //BANK LENGHT (DUMMY)
        mem[7] = 32'hFEEDBEEF;//BANK NAME
        mem[8] = 32'h2; //BANK LENGTH
        mem[9] = 32'hDEADBEEF; //BANK CONTENT
        mem[10] = 32'h01010101; //BANK CONTENT
        mem[11] = 32'hF00DDEAD;//BANK NAME
        mem[12] = 32'h3; //BANK LENGTH
        mem[13] = 32'hDEADBEEF; //BANK CONTENT
        mem[14] = 32'h11111111; //BANK CONTENT
        mem[15] = 32'h12121212; //BANK CONTENT
    end
    
    always @(posedge CLK) begin
        BUFFERDATA <= mem[BUFFERADDR];
    end
    
    //check CRC on receiving end
    reg [7:0] PREV_DCBDATA;
    reg [7:0] PREV_PREV_DCBDATA;
    always @(posedge CLK) begin
        PREV_DCBDATA <= DCBDATA;
        PREV_PREV_DCBDATA <= PREV_DCBDATA;
    end

    parameter WAIT_START_STATE = 4'b0000,
               PKTSIZE0_STATE = 4'b0001,
               PKTSIZE1_STATE = 4'b0010,
               DATA_STATE = 4'b0011,
               CRC0_STATE = 4'b0100,
               CRC1_STATE = 4'b0101,
               CRC2_STATE = 4'b0110,
               CRC3_STATE = 4'b0111,
               CRCCHECK_STATE = 4'b1000;
    reg [3:0] STATE;
    initial begin
       STATE <= WAIT_START_STATE;
    end

    reg [15:0] PKTSIZECOUNTER;
    reg DCBCRCRESET;
    reg DCBCRCCALC;
    wire [31:0] CRCDCB;
    reg CRCVALID;

    always @(posedge CLK) begin
        case(STATE)
            WAIT_START_STATE : begin
               DCBCRCCALC <= 1'b0;
               CRCVALID <= 1'b0;
               if(PREV_PREV_DCBDATA == 8'h5a && PREV_DCBDATA == 8'hF0 && DCBDATA == 8'h0D) begin
                  DCBCRCRESET <= 1'b1;
                  STATE <= PKTSIZE0_STATE;
               end
            end
            PKTSIZE0_STATE : begin
               DCBCRCRESET <= 1'b0;
               PKTSIZECOUNTER[15:8] <= DCBDATA;
               STATE <= PKTSIZE1_STATE;
            end
            PKTSIZE1_STATE : begin
               DCBCRCCALC <=1'b1;
               PKTSIZECOUNTER[7:0] <= DCBDATA;
               STATE <= DATA_STATE;
            end
            DATA_STATE : begin
               DCBCRCCALC <=1'b1;
               PKTSIZECOUNTER <= PKTSIZECOUNTER - 1;
               if(PKTSIZECOUNTER == 16'h1) begin
                  STATE <= CRC0_STATE;
               end
            end
            CRC0_STATE : begin
               DCBCRCCALC <=1'b1;
               STATE <= CRC1_STATE;
            end
            CRC1_STATE : begin
               DCBCRCCALC <=1'b1;
               STATE <= CRC2_STATE;
            end
            CRC2_STATE : begin
               DCBCRCCALC <=1'b1;
               STATE <= CRC3_STATE;
            end
            CRC3_STATE : begin
               DCBCRCCALC <=1'b0;
               STATE <= CRCCHECK_STATE;
            end
            CRCCHECK_STATE : begin
               if(CRCDCB == 32'h38FB2284) CRCVALID <= 1'b1;
               STATE <= WAIT_START_STATE;
            end
         endcase
    end
    
    
    CRCCALC CRCCALC_BLOCK(
        .CLK(CLK),
        .RESET(DCBCRCRESET),
        .CALCENA(DCBCRCCALC),
        .DATA(DCBDATA),
        .CRC(CRCDCB)
    );
    
    initial begin
        PAYLOAD_MAX <= 16'h2;
        DCBREADY <= 1'b1;
        BUFFERBUSY <= 1'b0;
        ENABLE <= 1'b1;
        READYIGNORE <= 1'b0;
        #100 BUFFERBUSY <= 1'b1;
        #10 BUFFERBUSY <= 1'b0;
        #500 DCBREADY <= 1'b0;
        #1000 DCBREADY <= 1'b1;
        #4000 $finish;
    end
    
    
endmodule
