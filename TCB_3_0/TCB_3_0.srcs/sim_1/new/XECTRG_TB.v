`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/05/2016 10:01:28 AM
// Design Name: 
// Module Name: WFMTRG_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module XECTRG_TB();
    reg CLK_t;
    reg [1023:0] WDDATA_t;
    reg [23:0] QHTHR_t;
    wire ISTRG_t;
    wire [23:0] SUM_t;
    wire [3:0] MAX_t;
    wire [20:0] MAXWFM_t;


    XECTRG WFMTRGSIM(
        .CLK(CLK_t),
        .WDDATA(WDDATA_t),
        .QHTHR(QHTHR_t),
        .ISTRG(ISTRG_t),
        .SUM(SUM_t),
        .MAX(MAX_t),
        .MAXWFM(MAXWFM_t)
        ) ;


    initial begin
           CLK_t = 0;
    WDDATA_t[1023:0] = 0;
    end
    
    always 
        #5 CLK_t = !CLK_t;

    initial begin
    #10
    QHTHR_t <= 23'h10;
    WDDATA_t[19+64*0:0+64*0] <= 20'h1;
    #10
  
    WDDATA_t[19+64*0:0+64*0] <= 20'h0;
    WDDATA_t[19+64*1:0+64*1] <= 20'h1;
    #10
  
    WDDATA_t[19+64*1:0+64*1] <= 20'h0;
    WDDATA_t[19+64*2:0+64*2] <= 20'h1;
    #10
  
    WDDATA_t[19+64*2:0+64*2] <= 20'h0;
    WDDATA_t[19+64*3:0+64*3] <= 20'h1;
    #10
  
    WDDATA_t[19+64*3:0+64*3] <= 20'h0;
    WDDATA_t[19+64*4:0+64*4] <= 20'h1;
    #10
  
    WDDATA_t[19+64*4:0+64*4] <= 20'h0;
    WDDATA_t[19+64*5:0+64*5] <= 20'h1;
    #10
  
    WDDATA_t[19+64*5:0+64*5] <= 20'h0;
    WDDATA_t[19+64*6:0+64*6] <= 20'h1;
    #10
  
    WDDATA_t[19+64*6:0+64*6] <= 20'h0;
    WDDATA_t[19+64*7:0+64*7] <= 20'h1;
    #10
  
    WDDATA_t[19+64*7:0+64*7] <= 20'h0;
    WDDATA_t[19+64*8:0+64*8] <= 20'h1;
    #10
  
    WDDATA_t[19+64*8:0+64*8] <= 20'h0;
    WDDATA_t[19+64*9:0+64*9] <= 20'h1;
    #10

    WDDATA_t[19+64*9:0+64*9] <= 20'h0;
    WDDATA_t[19+64*10:0+64*10] <= 20'h1;
    #10
  
     WDDATA_t[19+64*10:0+64*10] <= 20'h0;
     WDDATA_t[19+64*11:0+64*11] <= 20'h1;
     #10

     WDDATA_t[19+64*11:0+64*11] <= 20'h0;
     WDDATA_t[19+64*12:0+64*12] <= 20'h1;
     #10   
   
    WDDATA_t[19+64*12:0+64*12] <= 20'h0;
    WDDATA_t[19+64*13:0+64*13] <= 20'h1;
    #10
  
    WDDATA_t[19+64*13:0+64*13] <= 20'h0;
    WDDATA_t[19+64*14:0+64*14] <= 20'h1;
    #10
  
    WDDATA_t[19+64*14:0+64*14] <= 20'h0;
    WDDATA_t[19+64*15:0+64*15] <= 20'h1;
    
end

endmodule
