`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.11.2016 18:39:11
// Design Name: 
// Module Name: ONEPRESCALER
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ONEPRESCALER(
    input ALGCLK,
    input CLK,
    input TRIMOD,
    input TRGFORCE,
    input [31:0] PRESCA,
    output TRIPATTERN,
    input SYNC,
    input MASK,
    input [5:0] TRGDLY
    );
    
    reg [31:0] COUNTER;
    reg OLD_TRIMOD;
    reg OLD_SYNC;
    reg [2:0] FLAG;
    reg TRIPATTERN_FIRE;
    
    wire END_COUNT;
    wire TRIPATTERN_DLY;
    reg TRIPATTERN_DLY_REG;
    
    assign END_COUNT = (COUNTER==0) & (PRESCA !=0);
    
    always @(posedge ALGCLK) begin
        //reg used for rising edge identification
        OLD_TRIMOD <= TRIMOD;
        OLD_SYNC <= SYNC;
        
        //counter with synchronous reset
        if((SYNC & ~OLD_SYNC) | END_COUNT) begin
            COUNTER <=PRESCA;
        end else if (TRIMOD & ~OLD_TRIMOD) begin
            COUNTER <= COUNTER-1;
        end
        
        // to shape output to be at least 4 ALGCLK ticks
        FLAG[0] <= END_COUNT;
        FLAG[1] <= FLAG[0];
        FLAG[2] <= FLAG[1];
        
        if (TRGFORCE) begin
            TRIPATTERN_FIRE <= 1'b1;
        end 
        else if(|FLAG | END_COUNT) begin
            TRIPATTERN_FIRE <= MASK;
        end        
        else begin 
            TRIPATTERN_FIRE <= 1'b0;
        end
        
    end

    //Shift register for trigger delay       
    SRLC32E #(
       .INIT(32'h00000000) // Initial Value of Shift Register
       ) SRLC32E_inst (
          .Q(TRIPATTERN_DLY),     // SRL data output
          .Q31(), // SRL cascade output pin
          .A(TRGDLY[4:0]),     // 5-bit shift depth select input
          .CE(1'b1),   // Clock enable input
          .CLK(CLK), // Clock input
          .D(TRIPATTERN_FIRE)      // SRL data input
       );
    always @(posedge CLK) begin
        TRIPATTERN_DLY_REG <= TRIPATTERN_DLY;
    end
       
    assign TRIPATTERN = (TRGDLY[5]==1'b0)?TRIPATTERN_FIRE:TRIPATTERN_DLY_REG;

endmodule
