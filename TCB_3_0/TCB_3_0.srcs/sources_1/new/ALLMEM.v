`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 09.09.2015 15:39:50
// Design Name: 
// Module Name: MEMIN
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: all the rams for input serdes and processed data 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module ALLMEM(
    input CLK,
    input RUNMODE,
    input SYNC,
    input FADCMODE,
    input TESTTXMODE,
    input WENA,
    input RCLK,
    input [1023:0] INDATA,
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [31:0] WDATA,
    output tri [31:0] RDATA,
    output [1023:0] ALGDATA,
    output [9:0] ADDROUT,
    input [63:0] XECDATA,
    input [63:0] GENTBITS
    );


    reg [6:0] COUNT_ADDR;
    
    always @(posedge CLK) begin
        if (SYNC) begin
            COUNT_ADDR <= 0;
        end else begin
            if(RUNMODE) COUNT_ADDR <= COUNT_ADDR + 1;
        end
    end
    
    assign ADDROUT = {2'b000, COUNT_ADDR};
    
reg [1023:0] MEMALGDATA;

//bypass register
//reg [1023:0] INDATAREG;
//always @(posedge CLK) begin
//        INDATAREG <= INDATA;
//end
//output mux
//assign ALGDATA = (FADCMODE==1'b1)? INDATAREG : MEMALGDATA;
assign ALGDATA = (FADCMODE==1'b1)? INDATA : MEMALGDATA;

//INPUT MEMORIES

genvar iSerdes;
for(iSerdes=0; iSerdes<16; iSerdes=iSerdes+1) begin
    //register output of memories to increase setup timing
    
    wire [63:0]MEMREG;
    always @(posedge CLK) begin
        MEMALGDATA[32*2*iSerdes+:64] <=MEMREG;
    end

    NEWRAM 
    #(
        .BASEADDR(`RINMEM0_ADDR+256*iSerdes),
        .SIZE(7)
    ) INPUT_LOWER_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(FADCMODE),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(INDATA[32*2*iSerdes+:32]),
        .DOUT(MEMREG[0+:32])
    );
    NEWRAM 
    #(
        .BASEADDR(`RINMEM0_ADDR+256*iSerdes+128),
        .SIZE(7)
    ) INPUT_UPPER_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(FADCMODE),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(INDATA[32*(2*iSerdes+1)+:32]),
        .DOUT(MEMREG[32+:32])
    );
end


// XENON RECO INFO

    NEWRAM 
    #(
        .BASEADDR(`RXECMEM_ADDR),
        .SIZE(7)
    ) XEC_LOWER_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(1'b1),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(XECDATA[0+:32]),
        .DOUT()
    );
    NEWRAM 
    #(
        .BASEADDR(`RXECMEM_ADDR+128),
        .SIZE(7)
    ) XEC_UPPER_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(1'b1),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(XECDATA[32+:32]),
        .DOUT()
    );


// RAMs FOR GENT BITS

    NEWRAM 
    #(
        .BASEADDR(`RGENTMEM_ADDR),
        .SIZE(`RGENTMEM_SIZE)
    ) GENT_LOWER_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(1'b1),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR[4:0]),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(GENTBITS[0+:32]),
        .DOUT()
    );
    NEWRAM 
    #(
        .BASEADDR(`RGENTMEM_ADDR+32),
        .SIZE(`RGENTMEM_SIZE)
    ) GENT_UPPER_RAM (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(RUNMODE),
        .RECORD(1'b1),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .INADDR(COUNT_ADDR[4:0]),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .DIN(GENTBITS[32+:32]),
        .DOUT()
    );



endmodule
