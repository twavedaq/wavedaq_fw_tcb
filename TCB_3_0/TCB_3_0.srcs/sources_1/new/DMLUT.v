`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2017 15:22:50
// Design Name: 
// Module Name: DMLUT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module DMLUT(
    input CLK,
    input [7:0] LXeID,
    input [8:0] TCID,
    output ISDM,
    input [31:0] RADDR,
    input [31:0] WADDR,
    input RCLK,
    input [31:0] WDATA,
    output [31:0] RDATA,
    input WENA
    );
    
parameter DMWIDE = 1;

ONELUT  #(
//.BASEADDR(`LXePatchAddr),
.INSIZE(17),
.OUTSIZE(1),
.MEMFILE((DMWIDE)?"dmwide.mem":"dmnarrow.mem")
)
    lut_inst (
    .DATAIN({TCID, LXeID}),
    .CLK(CLK),
    .DATAOUT(ISDM),
    .RCLK(RCLK),
    .WENA(WENA),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WDATA(WDATA),
    .RDATA(RDATA)
);  
        
endmodule
