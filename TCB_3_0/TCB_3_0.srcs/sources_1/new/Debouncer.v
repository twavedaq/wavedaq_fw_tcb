`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.10.2018 09:59:03
// Design Name: 
// Module Name: Debouncer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Debouncer(
    input CLK,
    input IN,
    output reg OUT
    );
    
    reg [1:0] DB;
    always @(posedge CLK) begin
        DB <= {DB[0], IN};
        OUT <= &DB;
    end
    
    
endmodule
