`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.01.2019 11:16:52
// Design Name: 
// Module Name: FCALO_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "global_parameters.vh"

module FCALO_BLOCK(
    input [1023:0] DATA,
    input [4:0]    SHAPER_VAL,
    input [4:0]    SHAPER_VAL_VETO,
    input 	   CLK,
    input [8:0]    MASKFCALO,
    input [6:0]    MASKFNEUTRON,
    output reg 	   FCALOOR,
    output reg 	   FNEUTRONOR
    );

//FOOT CALORIMETER TRIGGER
// ASSOCIATE THE CHANNELS, MASK AND COMPUTE THE OR
wire [8:0] FCALOHIT;
assign FCALOHIT[8:0] = DATA[(64*`FCALOSLOT)+48+:9] & MASKFCALO;

always @(posedge CLK) begin
    FCALOOR <= |FCALOHIT;
end

//FOOT NEUTRON TRIGGER
// ASSOCIATE THE CHANNELS, MASK AND COMPUTE THE OR
wire [6:0] FNEUTRONHIT;
assign FNEUTRONHIT[6:0] = DATA[(64*`FCALOSLOT)+57+:7] & MASKFNEUTRON;

always @(posedge CLK) begin
    FNEUTRONOR <= |FNEUTRONHIT;
end

endmodule
