`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.01.2019 11:16:52
// Design Name: 
// Module Name: FOOT_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "global_parameters.vh"

module FOOT_BLOCK(
    input [1023:0] DATA,
    input [4:0] SHAPER_VAL,
    input [4:0] SHAPER_VAL_VETO,
    input CLK,
    input [31:0] MAJTRIG,
    input [63:0] MASKTOFX,
    input [63:0] MASKTOFY,
    input [8:0]  MASKFCALO,
    input [6:0]  MASKFNEUTRON,
    output MARGTRGOR,
    output MARGTRG,
    output MARGTRGREG,
    output TOFTRG,
    output FCALOOR,
    output FNEUTRONOR,
    input [31:0] INTERSPILLDELAY,
    output INTERSPILLTRG,
    output TRIGBACKTRG,
    output FRAGTOFREG,
    output FRAGVETO,
    output FRAGTOFROMEREG,
    output FRAGVETOROME
    );

   // MARGARITA BLOCK
MARGARITA_BLOCK MARGARITA(
     .DATA(DATA),
     .SHAPER_VAL(SHAPER_VAL),
     .CLK(CLK),			  
     .MAJTRIG(MAJTRIG),
     .MARGTRGOR(MARGTRGOR),
     .MARGTRG(MARGTRG),
     .MARGTRGREG(MARGTRGREG)
);

   // TOF BLOCK
TOF_BLOCK TOF(
     .DATA(DATA),
     .SHAPER_VAL(SHAPER_VAL),
     .SHAPER_VAL_VETO(SHAPER_VAL_VETO),
     .CLK(CLK),			  
     .MAJTRIG(MAJTRIG),
     .MASKTOFX(MASKTOFX),
     .MASKTOFY(MASKTOFY),
     .TOFTRG(TOFTRG),
     .FRAGTOFREG(FRAGTOFREG),
     .FRAGVETO(FRAGVETO),
     .FRAGTOFROMEREG(FRAGTOFROMEREG),
     .FRAGVETOROME(FRAGVETOROME)
);
   

   // FCALO (AND NEUTRONS) BLOCK
FCALO_BLOCK FCALO(
     .DATA(DATA),
     .SHAPER_VAL(SHAPER_VAL),
     .SHAPER_VAL_VETO(SHAPER_VAL_VETO),
     .CLK(CLK),			  
     .MASKFCALO(MASKFCALO),
     .MASKFNEUTRON(MASKFNEUTRON),
     .FCALOOR(FCALOOR),
     .FNEUTRONOR(FNEUTRONOR)
);

//TRIGBACK INPUT SIGNAL ASSOCIATION
wire TRIGBACK;
//TRIGBACK SHAPING
assign TRIGBACK = DATA[(64*`TOFCENTRALSLOT)+62];
PROG_SHAPER #(
    .VAL_SIZE(8)
) CHANNEL_SHAPER_TRIGBACK(
    .IN(TRIGBACK),
    .OUT(TRIGBACKTRG),
    .CLK(CLK),
    .VAL(8'hC8)
);

//INTERSPILL INPUT SIGNAL ASSOCIATION
wire INTERSPILL;
//INTERSPILL SHAPING
assign INTERSPILL = DATA[(64*`TOFCENTRALSLOT)+63];
wire INTERSPILL_SH;
PROG_SHAPER #(
    .VAL_SIZE(8)
) CHANNEL_SHAPER_IUNTERSPILL(
    .IN(INTERSPILL),
    .OUT(INTERSPILL_SH),
    .CLK(CLK),
    .VAL(8'hC8)
);
   
//INTERSPILL DELAY
reg INTERSPILL_REG, INTERSPILL_REGOLD;
reg COUENA, COURESET;  
reg [31:0] COUVALUE;
reg [31:0] INTERSPILLDELAYPROTECTED;
always @(posedge CLK) begin
    INTERSPILL_REG <= INTERSPILL_SH;
    INTERSPILL_REGOLD <= INTERSPILL_REG;
    // PROTECT THE INTERSPILL DELAY IN CASE IS 0
    if(INTERSPILLDELAY == 32'b0) 
        INTERSPILLDELAYPROTECTED <= 32'h1;
    else 
        INTERSPILLDELAYPROTECTED <= INTERSPILLDELAY;        
    // LOOK FOR SIGNAL POSEDGE TO ENABLE THE COUNTER
    if(INTERSPILL_REG & ~INTERSPILL_REGOLD) begin
        COUENA <= 1'b1;
    end else if (COUVALUE == INTERSPILLDELAY) begin 
        COUENA <= 1'b0;
        COURESET <= 1'b1;
    end     // WHEN COUNTER DISABLED COUVALUE FORCED TO 0
    else if(COUENA == 1'b0) begin
        COUVALUE <= 32'b0;
        COURESET <= 1'b0;
    end
    // WHEN ENABLED THE COUNTER COUNTS
    if (COUENA == 1'b1) begin
        COUVALUE = COUVALUE + 1;
    end
end   

//SHAPE THE COURESET
PROG_SHAPER #(
    .VAL_SIZE(5)
) CHANNEL_SHAPER_COURESET(
    .IN(COURESET),
    .OUT(INTERSPILLTRG),
    .CLK(CLK),
    .VAL(SHAPER_VAL)
);
   
   
endmodule
