`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.02.2017 12:28:47
// Design Name: 
// Module Name: MEG_GENT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module FOOT_GENT(
    input 	       T_PED, //PEDESTAL RANDOM TRIGGER 
    input 	       RUNMODE,
    input 	       MARGARITATRG,
    input 	       MARGARITASINGLE,
    input 	       MARGARITATRGREG,
    input 	       TOFTRG,
    input              FRAGTOFREG,	       
    input              FRAGVETO,	       
    input              FRAGTOFROMEREG,	       
    input              FRAGVETOROME,	       
    input 	       FCALOOR,
    input 	       FNEUTRONOR,
    input 	       INTERSPILLTRG,
    input 	       TRIGBACKTRG,
    output [`NTRG-1:0] ISTRG,
    output [`NTRG-1:0] TRGCOUMODE //TRGCOUMODE IS ASSIGNED GENT AND IS RUNMODE IF DRS USED OR 1 IF ONLY DISCRIMINATOR TRG
    );
    /////////////////////////
    // TRIGGER TYPE 0 - FOOT FRAG CALOVETO
    
    assign ISTRG[0] = 1'b0; 
    assign TRGCOUMODE[0] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 1 - FOOT FRAG TOF Pisa
    assign ISTRG[1] = FRAGTOFREG & MARGARITATRGREG &(~FRAGVETO);
    assign TRGCOUMODE[1] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 2 - FOOT FRAG TOF Rome
    assign ISTRG[2] = FRAGTOFROMEREG & MARGARITATRGREG &(~FRAGVETOROME);
    assign TRGCOUMODE[2] = 1'b1;

    /////////////////////////
    // TRIGGER TYPE 3 - UNUSED
    assign ISTRG[3] = 1'b0;
    assign TRGCOUMODE[3] = 1'b0;

    /////////////////////////
    // TRIGGER TYPE 4 - UNUSED
    assign ISTRG[4] = 1'b0;
    assign TRGCOUMODE[4] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 5 - UNUSED
    assign ISTRG[5] = 1'b0;
    assign TRGCOUMODE[5] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 6 - UNUSED
    assign ISTRG[6] = 1'b0;
    assign TRGCOUMODE[6] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 7 - UNUSED
    assign ISTRG[7] = 1'b0;
    assign TRGCOUMODE[7] = 1'b0;

    /////////////////////////
    // TRIGGER TYPE 8 - UNUSED
    assign ISTRG[8] = 1'b0;
    assign TRGCOUMODE[8] = 1'b0;

    /////////////////////////
    // TRIGGER TYPE 9 - UNUSED
    assign ISTRG[9] = 1'b0;
    assign TRGCOUMODE[9] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 10 - MARGARITA + CALO
    assign ISTRG[10] = MARGARITATRGREG & FCALOOR;
    assign TRGCOUMODE[10] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 11 - UNUSED
    assign ISTRG[11] = 1'b0;
    assign TRGCOUMODE[11] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 12 - UNUSED
    assign ISTRG[12] = 1'b0;
    assign TRGCOUMODE[12] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 13 - UNUSED
    assign ISTRG[13] = 1'b0;
    assign TRGCOUMODE[13] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 14 - UNUSED
    assign ISTRG[14] = 1'b0;
    assign TRGCOUMODE[14] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 15 - UNUSED
    assign ISTRG[15] = 1'b0;
    assign TRGCOUMODE[15] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 16 - UNUSED
    assign ISTRG[16] = 1'b0;
    assign TRGCOUMODE[16] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 17 - UNUSED
    assign ISTRG[17] = 1'b0;
    assign TRGCOUMODE[17] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 18 - UNUSED
    assign ISTRG[18] = 1'b0;
    assign TRGCOUMODE[18] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 19 - MARGARITA + NEUTRONS -- CNAO2020
    assign ISTRG[19] = MARGARITATRGREG & FNEUTRONOR;
    assign TRGCOUMODE[19] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 20 - TOF - COINC
    assign ISTRG[20] = 1'b0;
    assign TRGCOUMODE[20] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 21 - TOF COINC ROME 
    assign ISTRG[21] = 1'b0;
    assign TRGCOUMODE[21] = 1'b1;

    /////////////////////////
    // TRIGGER TYPE 22 - TOF BAR
    assign ISTRG[22] = 1'b0;
    assign TRGCOUMODE[22] = 1'b1;

    /////////////////////////
    // TRIGGER TYPE 23 - TOF OR
    assign ISTRG[23] = 1'b0;
    assign TRGCOUMODE[23] = 1'b1;

    /////////////////////////
    // TRIGGER TYPE 24 - UNUSED
    assign ISTRG[24] = 1'b0;
    assign TRGCOUMODE[24] = 1'b0;
          
    /////////////////////////
    // TRIGGER TYPE 25 - UNUSED
    assign ISTRG[25] = 1'b0;
    assign TRGCOUMODE[25] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 26 - UNUSED
    assign ISTRG[26] = 1'b0;
    assign TRGCOUMODE[26] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 27 - UNUSED
    assign ISTRG[27] = 1'b0;
    assign TRGCOUMODE[27] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 28 - UNUSED
    assign ISTRG[28] = 1'b0;
    assign TRGCOUMODE[28] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 29 - UNUSED
    assign ISTRG[29] = 1'b0;
    assign TRGCOUMODE[29] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 30 - CALO OR
    assign ISTRG[30] = FCALOOR;
    assign TRGCOUMODE[30] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 31 - UNUSED
    assign ISTRG[31] = 1'b0;
    assign TRGCOUMODE[31] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 32 - UNUSED
    assign ISTRG[32] = 1'b0;
    assign TRGCOUMODE[32] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 33 - UNUSED
    assign ISTRG[33] = 1'b0;
    assign TRGCOUMODE[33] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 34 - UNUSED
    assign ISTRG[34] = 1'b0;
    assign TRGCOUMODE[34] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 35 - UNUSED
    assign ISTRG[35] = 1'b0;
    assign TRGCOUMODE[35] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 36 - UNUSED
    assign ISTRG[36] = 1'b0;
    assign TRGCOUMODE[36] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 37 - UNUSED
    assign ISTRG[37] = 1'b0;
    assign TRGCOUMODE[37] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 38 - UNUSED
    assign ISTRG[38] = 1'b0;
    assign TRGCOUMODE[38] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 39 - UNUSED
    assign ISTRG[39] = 1'b0;
    assign TRGCOUMODE[39] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 40 - MARGARITA MAJORITY
    assign ISTRG[40] = MARGARITATRG;
    assign TRGCOUMODE[40] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 41 - MARGARITA OR
    assign ISTRG[41] = MARGARITASINGLE;
    assign TRGCOUMODE[41] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 42 - UNUSED
    assign ISTRG[42] = 1'b0;
    assign TRGCOUMODE[42] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 43 - UNUSED
    assign ISTRG[43] = 1'b0;
    assign TRGCOUMODE[43] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 44 - UNUSED
    assign ISTRG[44] = 1'b0;
    assign TRGCOUMODE[44] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 45 - UNUSED
    assign ISTRG[45] = 1'b0;
    assign TRGCOUMODE[45] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 46 - UNUSED
    assign ISTRG[46] = 1'b0;
    assign TRGCOUMODE[46] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 47 - UNUSED
    assign ISTRG[47] = 1'b0;
    assign TRGCOUMODE[47] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 48 - UNUSED
    assign ISTRG[48] = 1'b0;
    assign TRGCOUMODE[48] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 49 - UNUSED
    assign ISTRG[49] = 1'b0;
    assign TRGCOUMODE[49] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 50 - INTERPSILL 
    assign ISTRG[50] = INTERSPILLTRG;
    assign TRGCOUMODE[50] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 51 - UNUSED
    assign ISTRG[51] = TRIGBACKTRG;
    assign TRGCOUMODE[51] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 52 - UNUSED
    assign ISTRG[52] = 1'b0;
    assign TRGCOUMODE[52] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 53 - UNUSED
    assign ISTRG[53] = 1'b0;
    assign TRGCOUMODE[53] = 1'b0;
    
   /////////////////////////
    // TRIGGER TYPE 54 - UNUSED
    assign ISTRG[54] = 1'b0;
    assign TRGCOUMODE[54] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 55 - UNUSED
    assign ISTRG[55] = 1'b0;
    assign TRGCOUMODE[55] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 56 - UNUSED
    assign ISTRG[56] = 1'b0;
    assign TRGCOUMODE[56] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 57 -  UNUSED
    assign ISTRG[57] = 1'b0;
    assign TRGCOUMODE[57] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 58 - UNUSED
    assign ISTRG[58] = 1'b0;
    assign TRGCOUMODE[58] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 59 - UNUSED
    assign ISTRG[59] = 1'b0;
    assign TRGCOUMODE[59] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 60 - UNUSED
    assign ISTRG[60] = 1'b0;
    assign TRGCOUMODE[60] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 61 - UNUSED
    assign ISTRG[61] = 1'b0;
    assign TRGCOUMODE[61] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 62 - NEUTRONS AUX - CNAO 2020
    assign ISTRG[62] = FNEUTRONOR;
    assign TRGCOUMODE[62] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 63 - PEDESTAL
    assign ISTRG[63]=T_PED;
    assign TRGCOUMODE[63] = 1'b1;
    
endmodule
