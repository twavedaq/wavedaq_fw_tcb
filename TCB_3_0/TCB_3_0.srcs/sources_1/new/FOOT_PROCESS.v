`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.02.2017 14:10:34
// Design Name: 
// Module Name: TRI_PROCESS
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module FOOT_PROCESS(
    // GENERAL
    input CLK,
    input ALGCLK,
    input PEDCLK,
    input RUNMODE,
    input SYNC,
    output T_PED,
    // input data
    input [1023:0] DATA,
    // bus for memories
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [31:0] WDATA,
    input RCLK,
    output [31:0] RDATA,
    input WENA,
    // FOOT
    // patameters
    input [31:0] MAJTRIG,
    input [63:0] MASKTOFX,
    input [63:0] MASKTOFY,
    input [8:0]  MASKFCALO,
    input [6:0]  MASKFNEUTRON,
    input [31:0] INTERSPILLDELAY,
    input [4:0] SHAPER_VAL,
    input [4:0] SHAPER_VAL_VETO,
    // triggers
    output MARGARITATRG,
    output MARGARITASINGLE,
    output MARGARITATRGREG,
    output TOFTRG,
    output INTERSPILLTRG,
    output TRIGBACKTRG,
    output FCALOOR,
    output FNEUTRONOR,    
    output FRAGTOFREG,
    output FRAGVETO,
    output FRAGTOFROMEREG,
    output FRAGVETOROME
    );

assign RDATA = 32'hZZZZZZZZ;

/////////////////////////
// GENERAL
//RANDOM TRIGGER
PEDBLOCK PEDBLOCK_INST(
     .PEDCLK(PEDCLK),
     .CLK(CLK),
     .ISTRG(T_PED)
); 

/////////////////////////
// FOOT
FOOT_BLOCK FOOT(
    .DATA(DATA),
    .SHAPER_VAL(SHAPER_VAL),
    .SHAPER_VAL_VETO(SHAPER_VAL_VETO),
    .CLK(CLK),
    .MAJTRIG(MAJTRIG),
    .MASKTOFX(MASKTOFX),
    .MASKTOFY(MASKTOFY),
    .MASKFCALO(MASKFCALO),
    .MASKFNEUTRON(MASKFNEUTRON),
    .MARGTRGOR(MARGARITASINGLE),
    .MARGTRG(MARGARITATRG),
    .MARGTRGREG(MARGARITATRGREG),
    .TOFTRG(TOFTRG),
    .INTERSPILLDELAY(INTERSPILLDELAY),
    .INTERSPILLTRG(INTERSPILLTRG),
    .FCALOOR(FCALOOR),
    .FNEUTRONOR(FNEUTRONOR),
    .TRIGBACKTRG(TRIGBACKTRG),
    .FRAGTOFREG(FRAGTOFREG),
    .FRAGVETO(FRAGVETO),
    .FRAGTOFROMEREG(FRAGTOFROMEREG),
    .FRAGVETOROME(FRAGVETOROME)
);

endmodule
