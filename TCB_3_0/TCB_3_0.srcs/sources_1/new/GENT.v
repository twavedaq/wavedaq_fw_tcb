`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.02.2017 12:28:47
// Design Name: 
// Module Name: MEG_GENT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module GENT(
    input T_PED,         //PEDESTAL RANDOM TRIGGER 
    input SCIFITRG,
    input MATRIXTRG,
    input SINGLECRATETRG,
    output [`NTRG-1:0] ISTRG,
    output [`NTRG-1:0] TRGCOUMODE, //TRGCOUMODE IS ASSIGNED GENT AND IS RUNMODE IF DRS USED OR 1 IF ONLY DISCRIMINATOR TRG
    input RUNMODE
    );
    /////////////////////////
    // TRIGGER TYPE 0 - SINGLE CRATE
    
    assign ISTRG[0] = SINGLECRATETRG;
    assign TRGCOUMODE[0] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 1 - SCIFI
    assign ISTRG[1] = SCIFITRG;
    assign TRGCOUMODE[1] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 2 - MATRIX
    assign ISTRG[2] = MATRIXTRG;
    assign TRGCOUMODE[2] = 1'b1;

    /////////////////////////
    // TRIGGER TYPE [62:3] - UNUSED
    assign ISTRG[62:3] = 0;
    assign TRGCOUMODE[62:3] = 0;

    /////////////////////////
    // TRIGGER TYPE 63 - PEDESTAL
    assign ISTRG[63]=T_PED;
    assign TRGCOUMODE[63] = 1'b1;
    
endmodule
