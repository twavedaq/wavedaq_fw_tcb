`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 10.09.2015 09:44:48
// Design Name: 
// Module Name: MAIN
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: here the blocks for trigger algorithm
// 
//////////////////////////////////////////////////////////////////////////////////

`include "global_parameters.vh"
module MAIN(
    input [1023:0] DATA,
    input CLK,
    input SLOWCLK,
    input PEDCLK,
    input ALGCLK,
    input RUNMODE,
    input SYNC,
    input [31:0] QHTHR,
    input [31:0] QLTHR,
    input [31:0] QCTHR,
    input [31:0] QNGENTHR_LOW,
    input [31:0] QNGENTHR_HIGH,
    input [31:0] QSUMSEL,
    input [31:0] RLXe_PATCH,
    input [31:0] RLXe_PATCH_DLY,
    input [31:0] RLXe_HITTHR,
    input [31:0] RTIMN,
    input [31:0] RTIMW,
    input [31:0] TCHITTHR,
    input [63:0] DETECTORDLY,
    input [`NTRG-1:0] TRGENA,
    input [`NTRG-1:0] TRGFORCE,
    input [(`NTRG*32)-1:0] PRESCA,
    input [(6*`NTRG)-1:0] TRGDLY,
    output GETRI,
    output [31:0] LIVETIME,
    output [31:0] TOTALTIME,
    output [31:0] EVECOU,
    output [`TRITYPDIM-1:0] TRITYPE,
    output [`TRGFAMNUM-1:0] ROENA,
    output [`NTRG-1:0] TRIPATTERN,
    output [(`NTRG*32)-1:0] TRGCOUNTER,
    output [63:0] XECDATA,
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [31:0] WDATA,
    input RCLK,
    output [31:0] RDATA,
    input WENA,
    input ENGETRI,
    output [63:0] GENTBITS,
    output [31:0] PCCOUNT,
    input [4:0] SINGLECRATE_SHAPER_VAL,
    input [4:0] SINGLECRATE_SHAPER_VAL_VETO,
    input [255:0] SINGLECRATE_ISVETO,
    input [255:0] SINGLECRATE_MASK,
    input [127:0] SINGLECRATE_TWOCHANNEL_SEL,
    input SINGLECRATE_LOGIC_SEL,
    input [31:0] PRESCAADC,
    output ENADC,
    input [31:0] MAJTRIG,
    input [63:0] MASKTOFX,
    input [63:0] MASKTOFY,
    input [8:0]  MASKFCALO,
    input [6:0]  MASKFNEUTRON,
    input [31:0] INTERSPILLDELAY,
    output MARGTRG,
    output [(32*(`SCIFIFIBERS+1))-1:0] SCIFIDATA,
    output [(32*((`SCIFIFIBERS/2)*(`SCIFIFIBERS/2)))-1:0]SCIFICOINCDATA,
    input [`MATRIXCHANNELS-1:0] MATRIXMASK
);

wire PROTONCURRENTTRG;

// BLOCK WITH COUNTERS
TIMEEVE TIMEEVE_INST(
    .RUNMODE(RUNMODE),
    .SYNC(SYNC),
    .CLK(CLK),
    .SLOWCLK(SLOWCLK),
    .EVECOU(EVECOU),
    .LIVETIME(LIVETIME),
    .TOTALTIME(TOTALTIME)
);
`ifdef MEG
//BLOCK WITH PROTON CURRENT COUNTER
PCURRCOUNTER PCURRCOUNTER_INST(
    .PCURR(PROTONCURRENTTRG), 
    .CLK(CLK), 
    .SYNC(SYNC),
    .PCCOUNT(PCCOUNT),
    .RUNMODE(RUNMODE)
);
`else 
assign PCCOUNT = 0;
`endif

// TRIGGER GENERATION: ALGORITHM, PRESCALING AND TRIGEGR CODE 
TRIGBLOCK TRIGBLOCK_INST(
    .DATA(DATA),
    .CLK(CLK),
    .PEDCLK(PEDCLK),
    .ALGCLK(ALGCLK),
    .QHTHR(QHTHR),
    .QLTHR(QLTHR),
    .QCTHR(QCTHR),
    .QNGENTHR_LOW(QNGENTHR_LOW),
    .QNGENTHR_HIGH(QNGENTHR_HIGH),
    .QSUMSEL(QSUMSEL),
    .RLXe_PATCH(RLXe_PATCH),
    .RLXe_PATCH_DLY(RLXe_PATCH_DLY),
    .RLXe_HITTHR(RLXe_HITTHR),
    .RTIMN(RTIMN),
    .RTIMW(RTIMW),
    .TCHITTHR(TCHITTHR),
    .DETECTORDLY(DETECTORDLY),
    .TRGENA(TRGENA),
    .TRGFORCE(TRGFORCE),
    .PRESCA(PRESCA),
    .RUNMODE(RUNMODE),
    .SYNC(SYNC),
    .TRIPATTERN(TRIPATTERN),
    .TRITYPE(TRITYPE),
    .ROENA(ROENA),
    .GETRI(GETRI),
    .TRGCOUNTER(TRGCOUNTER),
    .XECDATA(XECDATA),
    .RCLK(RCLK),
    .WENA(WENA),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WDATA(WDATA),
    .RDATA(RDATA),
    .ENGETRI(ENGETRI),
    .GENTBITS(GENTBITS),
    .PROTONCURRENTTRG(PROTONCURRENTTRG),
    .TRGDLY(TRGDLY),
    .SINGLECRATE_SHAPER_VAL(SINGLECRATE_SHAPER_VAL),
    .SINGLECRATE_SHAPER_VAL_VETO(SINGLECRATE_SHAPER_VAL_VETO),
    .SINGLECRATE_ISVETO(SINGLECRATE_ISVETO),
    .SINGLECRATE_MASK(SINGLECRATE_MASK),
    .SINGLECRATE_TWOCHANNEL_SEL(SINGLECRATE_TWOCHANNEL_SEL),
    .SINGLECRATE_LOGIC_SEL(SINGLECRATE_LOGIC_SEL),
    .PRESCAADC(PRESCAADC),
    .ENADC(ENADC),
    .MAJTRIG(MAJTRIG),
    .MASKTOFX(MASKTOFX),
    .MASKTOFY(MASKTOFY),
    .MASKFCALO(MASKFCALO),
    .MASKFNEUTRON(MASKFNEUTRON),
    .INTERSPILLDELAY(INTERSPILLDELAY),
    .MARGTRG(MARGTRG),
    .SCIFIDATA(SCIFIDATA),
    .SCIFICOINCDATA(SCIFICOINCDATA),
    .MATRIXMASK(MATRIXMASK)
);
 
endmodule
