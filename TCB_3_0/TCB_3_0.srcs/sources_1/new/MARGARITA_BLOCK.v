`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.01.2019 11:16:52
// Design Name: 
// Module Name: MARGARITA_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "global_parameters.vh"

module MARGARITA_BLOCK(
    input [1023:0] DATA,
    input [4:0] SHAPER_VAL,
    input CLK,
    input [31:0] MAJTRIG,
    output reg MARGTRGOR,
    output reg MARGTRG,
    output MARGTRGREG
    );
    
// EXTRACT TRIGGER SETTINGS FROM MAJTRIG
wire [3:0] MAJVAL;
assign MAJVAL = MAJTRIG[3:0];
wire [5:0] MARGTRGDLY;
assign MARGTRGDLY = MAJTRIG[9:4];
wire [7:0] MARGATITAMASKS;
assign MARGATITAMASKS = MAJTRIG[23:16];
// EXTRACTS MARGARITA HITS FROM THE DATA STREAM
wire [7:0] MHIT;
assign MHIT[7:0] = DATA[(64*`MARGARITASLOT)+48+:8];
    
// HIT SHAPERS
wire [7:0] MHIT_SH;
genvar iHit;
for(iHit =0; iHit<8; iHit= iHit+1) begin
    PROG_SHAPER #(
        .VAL_SIZE(5)
    ) CHANNEL_SHAPER_MARGARITA(
        .IN(MHIT[iHit] & MARGATITAMASKS[iHit]),
        .OUT(MHIT_SH[iHit]),
        .CLK(CLK),
        .VAL(SHAPER_VAL)
   );
end

// PROTECT FOR MAJVAL = 0, IN CASE FORCED TO 1
reg [3:0] MAJVALPROTECTED;
always @(posedge CLK) begin
    if(MAJVAL == 4'b0000) begin
        MAJVALPROTECTED <= 4'b0001;
    end    
    else begin
        MAJVALPROTECTED <= MAJVAL; 
    end
end
        

reg[3:0] VALUE;
// THE MARGARITA TRIGGER IS FIRED WHEN AT LEAST NCHANNELS ARE FIRED (MAJORTY)
 always @(posedge CLK) begin
    VALUE <= MHIT_SH[7] + MHIT_SH[6] + MHIT_SH[5] + MHIT_SH[4] + MHIT_SH[3] + MHIT_SH[2] + MHIT_SH[1] + MHIT_SH[0];
    if(VALUE>0) 
        MARGTRGOR <= 1'b1;
    else 
        MARGTRGOR <= 1'b0;
    if(VALUE >= MAJVALPROTECTED) 
        MARGTRG <= 1'b1;
    else
        MARGTRG <= 1'b0;
end//always

//PROGRAMMABLE DELAY OF MARGTRG FOR THE FRAGMENTATION TRIGGER
wire MARGTRG_DLY;
reg  MARGTRGSHIFT;
    //Shift register for trigger delay       
    SRLC32E #(
       .INIT(32'h00000000) // Initial Value of Shift Register
       ) SRLC32E_inst (
          .Q(MARGTRG_DLY),     // SRL data output
          .Q31(), // SRL cascade output pin
          .A(MARGTRGDLY[4:0]),     // 5-bit shift depth select input
          .CE(1'b1),   // Clock enable input
          .CLK(CLK), // Clock input
          .D(MARGTRG)      // SRL data input
     );
always @(posedge CLK) begin
    MARGTRGSHIFT<= MARGTRG_DLY;
end
assign MARGTRGREG = (MARGTRGDLY[5]==1'b0)?MARGTRG:MARGTRGSHIFT;

endmodule
