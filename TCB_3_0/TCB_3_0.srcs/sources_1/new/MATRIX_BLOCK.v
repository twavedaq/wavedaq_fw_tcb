`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.09.2020 11:07:38
// Design Name: 
// Module Name: MATRIX_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "global_parameters.vh"

module MATRIX_BLOCK(
    input [1023:0] DATA,
    input [`MATRIXCHANNELS-1:0] MASK,
    input CLK,
    output reg MATRIXTRG
    );
    
    wire [16*(`MATRIXLASTSLOT-`MATRIXFIRSTSLOT+1)-1:0]DISCRSTATE;
    
    genvar iSlot;
    for(iSlot = `MATRIXFIRSTSLOT; iSlot <= `MATRIXLASTSLOT; iSlot = iSlot +1) begin
        assign DISCRSTATE[iSlot*16+:16] = DATA[48+iSlot*64+:16];
    end
    
    reg [`MATRIXCHANNELS-1:0] HIT;
    always @(posedge CLK) begin
        HIT <= DISCRSTATE[`MATRIXCHANNELS-1:0] & MASK;
    end
     
     always @(posedge CLK) begin
        MATRIXTRG <= |HIT;
     end
    
endmodule
