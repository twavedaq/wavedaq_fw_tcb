`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.02.2017 12:28:47
// Design Name: 
// Module Name: MEG_GENT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module MEG_GENT(
    input LXe_QH,        //LXe OVER HIGH THRESHOLD
    input LXe_QL,        //LXe OVER LOW THRESHOLD
    input LXe_QCOSM,     //LXe OVER COSMIC RAY THRESHOLD
    input LXe_QH_VETO,   //LXe OVER HIGH THRESHOLD VETO
    input LXe_QL_VETO,   //LXe OVER LOW THRESHOLD VETO
    input LXe_QNGEN,     //LXe NGEN WINDOW THRESHOLD
    input LXe_ALPHA,     //LXe ALPHA TRIGGER
    input LXe_PATCH,     //LXe INSIDE A PATCH
    input [`NTCPositron-1:0] DM_NARROW,      //DM narrow 
    input [`NTCPositron-1:0] DM_WIDE,        //DM wide 
    input [`NTCPositron-1:0] TIMEN,          // delta T narrow 
    input [`NTCPositron-1:0] TIMEW,          // delta T wide
    input TC_SINGLE,     //TC IN SINGLE (OR)
    input TC_LASER,      //TC LASER
    input TC_LDIODE,     //TC DIODE FOR LASER STABILIY CHECK
    input TC_MULTIPLICITY,     //TC MULTIPLICITY THRESHOLD
    input T_PED,         //PEDESTAL RANDOM TRIGGER 
    input RDCTRG,
    input RDCPLASTICSINGLE,
    input RDCLYSOOR,
    input RDCLYSOTHRFIRE,
    input BGOTRG,
    input BGOCOSM,
    input BGOTHRFIRE,
    input BGOPRESHCOUNTER,
    input TCLASERTRG,
    input TCDIODETRG,
    input XECLEDPMTTRG,
    input XECLEDMPPCTRG,
    input RFACCELTRG,
    input PROTONCURRENTTRG,
    input MONITORDCTRG,
    input NGENTRG,
    input CRCSINGLETRG,
    input CRCPAIRTRG,
    output [`NTRG-1:0] ISTRG,
    output [`NTRG-1:0] TRGCOUMODE, //TRGCOUMODE IS ASSIGNED GENT AND IS RUNMODE IF DRS USED OR 1 IF ONLY DISCRIMINATOR TRG
    input RUNMODE
    );
    /////////////////////////
    // TRIGGER TYPE 0 - MEG TRIGGER
    
    assign ISTRG[0] = LXe_QH & |(DM_NARROW & TIMEN);
    assign TRGCOUMODE[0] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 1 - MEG TRIGGER LOW Q
    assign ISTRG[1] = LXe_QL & |(DM_NARROW & TIMEN);
    assign TRGCOUMODE[1] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 2 - MEG TRIGGER WIDE ANGLE
    assign ISTRG[2] = LXe_QH & |(DM_WIDE & TIMEN);
    assign TRGCOUMODE[2] = RUNMODE;

    /////////////////////////
    // TRIGGER TYPE 3 - MEG TRIGGER WIDE TIME
    assign ISTRG[3] = LXe_QH & |(DM_NARROW & TIMEW);
    assign TRGCOUMODE[3] = RUNMODE;

    /////////////////////////
    // TRIGGER TYPE 4 - RAD NARROW TIME
    assign ISTRG[4] = LXe_QH & |(TIMEN);
    assign TRGCOUMODE[4] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 5 - RAD WIDE TIME
    assign ISTRG[5] = LXe_QH & |(TIMEW);
    assign TRGCOUMODE[5] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 6 - MICHEL
    assign ISTRG[6] = 1'b0;
    assign TRGCOUMODE[6] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 7 - UNUSED
    assign ISTRG[7] = 1'b0;
    assign TRGCOUMODE[7] = 1'b0;

    /////////////////////////
    // TRIGGER TYPE 8 - UNUSED
    assign ISTRG[8] = 1'b0;
    assign TRGCOUMODE[8] = 1'b0;

    /////////////////////////
    // TRIGGER TYPE 9 - UNUSED
    assign ISTRG[9] = 1'b0;
    assign TRGCOUMODE[9] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 10 - LXE HIGH Q
    assign ISTRG[10] = LXe_QH & LXe_PATCH & ~LXe_QH_VETO;
    assign TRGCOUMODE[10] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 11 - LXE LOW Q
    assign ISTRG[11] = LXe_QL & LXe_PATCH & ~LXe_QL_VETO;
    assign TRGCOUMODE[11] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 12 - LXE ALPHA
    assign ISTRG[12] = LXe_ALPHA;
    assign TRGCOUMODE[12] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 13 - LXE LED PMT
    assign ISTRG[13] = XECLEDPMTTRG;
    assign TRGCOUMODE[13] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 14 - LXE LED MPPC
    assign ISTRG[14] = XECLEDMPPCTRG;
    assign TRGCOUMODE[14] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 15 - LXE COSMICS
    assign ISTRG[15] = LXe_QCOSM;
    assign TRGCOUMODE[15] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 16 - UNUSED
    assign ISTRG[16] = 1'b0;
    assign TRGCOUMODE[16] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 17 - UNUSED
    assign ISTRG[17] = 1'b0;
    assign TRGCOUMODE[17] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 18 - UNUSED
    assign ISTRG[18] = 1'b0;
    assign TRGCOUMODE[18] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 19 - UNUSED
    assign ISTRG[19] = 1'b0;
    assign TRGCOUMODE[19] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 20 - TC TRACK
    assign ISTRG[20] = 1'b0;
    assign TRGCOUMODE[20] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 21 - TC SINGLE
    assign ISTRG[21] = TC_SINGLE;
    assign TRGCOUMODE[21] = 1'b1;

    /////////////////////////
    // TRIGGER TYPE 22 - TC COSMIC
    assign ISTRG[22] = 1'b0;
    assign TRGCOUMODE[22] = 1'b0;

    /////////////////////////
    // TRIGGER TYPE 23 - TC LASER
    assign ISTRG[23] = TC_LASER;
    assign TRGCOUMODE[23] = 1'b1;

    /////////////////////////
    // TRIGGER TYPE 24 - TC DIODE
    assign ISTRG[24] = TC_LDIODE;
    assign TRGCOUMODE[24] = 1'b1;
          
    /////////////////////////
    // TRIGGER TYPE 25 - MULTIPLICITY
    assign ISTRG[25] = TC_MULTIPLICITY;
    assign TRGCOUMODE[25] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 26 - UNUSED
    assign ISTRG[26] = 1'b0;
    assign TRGCOUMODE[26] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 27 - UNUSED
    assign ISTRG[27] = 1'b0;
    assign TRGCOUMODE[27] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 28 - UNUSED
    assign ISTRG[28] = 1'b0;
    assign TRGCOUMODE[28] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 29 - UNUSED
    assign ISTRG[29] = 1'b0;
    assign TRGCOUMODE[29] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 30 - RDC LXE
    assign ISTRG[30] = RDCTRG & LXe_QH;
    assign TRGCOUMODE[30] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 31 - RDC ALONE
    assign ISTRG[31] = RDCTRG;
    assign TRGCOUMODE[31] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 32 - RDC SCINT
    assign ISTRG[32] = RDCPLASTICSINGLE;
    assign TRGCOUMODE[32] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 33 - RDC LYSO QSUM
    assign ISTRG[33] = RDCLYSOTHRFIRE;
    assign TRGCOUMODE[33] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 34 - RDC LYSO OR
    assign ISTRG[34] = RDCLYSOOR;
    assign TRGCOUMODE[34] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 35 - UNUSED
    assign ISTRG[35] = 1'b0;
    assign TRGCOUMODE[35] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 36 - UNUSED
    assign ISTRG[36] = 1'b0;
    assign TRGCOUMODE[36] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 37 - UNUSED
    assign ISTRG[37] = 1'b0;
    assign TRGCOUMODE[37] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 38 - UNUSED
    assign ISTRG[38] = 1'b0;
    assign TRGCOUMODE[38] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 39 - UNUSED
    assign ISTRG[39] = 1'b0;
    assign TRGCOUMODE[39] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 40 - DCH TRACK
    assign ISTRG[40] = 1'b0;
    assign TRGCOUMODE[40] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 41 - DCH CRC
    assign ISTRG[41] = 1'b0;
    assign TRGCOUMODE[41] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 42 - DCH COSMIC
    assign ISTRG[42] = 1'b0;
    assign TRGCOUMODE[42] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 43 - CRC PAIR
    assign ISTRG[43] = CRCPAIRTRG;
    assign TRGCOUMODE[43] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 44 - CRC SINGLE
    assign ISTRG[44] = CRCSINGLETRG;
    assign TRGCOUMODE[44] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 45 - DCH SINGLE
    assign ISTRG[45] = 1'b0;
    assign TRGCOUMODE[45] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 46 - DCH MONITORING CHAMBER
    assign ISTRG[46] = MONITORDCTRG;
    assign TRGCOUMODE[46] = 1'b1;
    
    /////////////////////////
    // TRIGGER TYPE 47 - DCH MONITORING CHAMBER COUNTERS
    assign ISTRG[47] = 1'b0;
    assign TRGCOUMODE[47] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 48 - UNUSED
    assign ISTRG[48] = 1'b0;
    assign TRGCOUMODE[48] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 49 - UNUSED
    assign ISTRG[49] = 1'b0;
    assign TRGCOUMODE[49] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 50 - Pi0
    assign ISTRG[50] = LXe_QH & LXe_PATCH & ~LXe_QH_VETO & BGOPRESHCOUNTER;
    assign TRGCOUMODE[50] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 51 - Pi0 NO PRESH
    assign ISTRG[51] = LXe_QH & LXe_PATCH & ~LXe_QH_VETO & BGOTRG;
    assign TRGCOUMODE[51] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 52 - BGO ALONE
    assign ISTRG[52] = BGOTRG;
    assign TRGCOUMODE[52] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 53 - BGO QSUM
    assign ISTRG[53] = BGOTHRFIRE;
    assign TRGCOUMODE[53] = RUNMODE;
    
   /////////////////////////
    // TRIGGER TYPE 54 - BGO COSMIC
    assign ISTRG[54] = BGOCOSM;
    assign TRGCOUMODE[54] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 55 - PRESHOWER ALONE
    assign ISTRG[55] = BGOPRESHCOUNTER;
    assign TRGCOUMODE[55] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 56 - CW BORON
    assign ISTRG[56] = 1'b0;
    assign TRGCOUMODE[56] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 57 - UNUSED
    assign ISTRG[57] = 1'b0;
    assign TRGCOUMODE[57] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 58 - NEUTRON GENERATOR
    assign ISTRG[58] = NGENTRG & LXe_QNGEN;
    assign TRGCOUMODE[58] = RUNMODE;
    
    /////////////////////////
    // TRIGGER TYPE 59 - UNUSED
    assign ISTRG[59] = 1'b0;
    assign TRGCOUMODE[59] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 60 - UNUSED
    assign ISTRG[60] = 1'b0;
    assign TRGCOUMODE[60] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 61 - UNUSED
    assign ISTRG[61] = 1'b0;
    assign TRGCOUMODE[61] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 62 - UNUSED
    assign ISTRG[62] = 1'b0;
    assign TRGCOUMODE[62] = 1'b0;
    
    /////////////////////////
    // TRIGGER TYPE 63 - PEDESTAL
    assign ISTRG[63]=T_PED;
    assign TRGCOUMODE[63] = 1'b1;
    
endmodule
