`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.02.2017 14:10:34
// Design Name: 
// Module Name: TRI_PROCESS
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module MEG_PROCESS(
    // GENERAL
    input CLK,
    input ALGCLK,
    input PEDCLK,
    input RUNMODE,
    input SYNC,
    output T_PED,
    // input data
    input [1023:0] DATA,
    // bus for memories
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [31:0] WDATA,
    input RCLK,
    output [31:0] RDATA,
    input WENA,
    // MEG
    input signed [31:0] QHTHR,
    input signed [31:0] QLTHR,
    input signed [31:0] QCTHR,
    input signed [31:0] QNGENTHR_LOW,
    input signed [31:0] QNGENTHR_HIGH,
    input [31:0] QSUMSEL,
    input [31:0] RLXe_PATCH,
    input [31:0] RLXe_PATCH_DLY,
    input [31:0] RLXe_HITTHR,
    input [31:0] RTIMN,
    input [31:0] RTIMW,
    input [31:0] TCHITTHR,
    input [63:0] DETECTORDLY,
    // LXe quantities
    output [34:0] LXeQSUM,
    output [7:0] LXeMAX,
    output [4:0] LXeTDCNUM,
    output [7:0] LXeTDCSUM,
    output LXe_QH,
    output LXe_QL,
    output LXe_QH_VETO,
    output LXe_QL_VETO,
    output LXe_QCOSM,
    output LXe_QNGEN,
    output LXe_PATCH,
    // TC quantities
    output TC_SINGLE,
    output TC_MULTIPLICITY,
    // Combined quantities
    output [4-1:0] DM_NARROW,
    output [4-1:0] DM_WIDE,
    output [4-1:0] TIMEN,
    output [4-1:0] TIMEW,
    // others
    output T_ALPHA,
    output RDCTRG,
    output RDCPLASTICSINGLE,
    output RDCLYSOOR,
    output RDCLYSOTHRFIRE,
    output BGOPRESHCOUNTER,
    output BGOTRG,
    output BGOCOSM,
    output BGOTHRFIRE,
    output TCLASERTRG,
    output TCDIODETRG,
    output XECLEDPMTTRG,
    output XECLEDMPPCTRG,
    output RFACCELTRG,
    output PROTONCURRENTTRG,
    output MONITORDCTRG,
    output NGENTRG,
    output CRCSINGLETRG,
    output CRCPAIRTRG
    );


/////////////////////////
// GENERAL
//RANDOM TRIGGER
PEDBLOCK PEDBLOCK_INST(
     .PEDCLK(PEDCLK),
     .CLK(CLK),
     .ISTRG(T_PED)
); 


/////////////////////////
// MEG
//LXe PROCESSING    
XECTRG XECTRGBLOCK(
    .DATA(DATA),
    .ALGCLK(ALGCLK),
    .CLK(CLK),
    .SUM(LXeQSUM),
    .MAX(LXeMAX),
    .TDCNUM(LXeTDCNUM),
    .TDCSUM(LXeTDCSUM),
    .QSUMSEL(QSUMSEL)
);
// AT THE END DISCRIMINATION WITH QHTHR
wire LXe_QH_PROMPT;
DISCRIMINATEWFM #(
    .LENGTH(10)
) QHDISCR(
    .INPUT(LXeQSUM[34:3]),
    .THR(QHTHR),
    .CLK(ALGCLK),
    .TRIG(LXe_QH_PROMPT)
);
reg [3:0] LXe_QH_DLY;
always @(posedge ALGCLK) begin
    LXe_QH_DLY <= {LXe_QH_DLY[2:0], LXe_QH_PROMPT};
end
reg [31:0] LXe_QH_PROGDLY;
always @(posedge ALGCLK) begin
   LXe_QH_PROGDLY <= {LXe_QH_PROGDLY[30:0], LXe_QH_DLY[3]};
end
assign LXe_QH = (DETECTORDLY[5])?LXe_QH_PROGDLY[DETECTORDLY[4:0]]:LXe_QH_DLY[3];

wire LXe_QH_VETO_FIRE;
DISCRVETO QHVETO(
    .INPUT(LXeQSUM[34:3]),
    .THR(QCTHR),
    .THR_TOVETO(QHTHR),
    .DISCR_TOVETO(LXe_QH),
    .CLK(ALGCLK),
    .TRIG(LXe_QH_VETO_FIRE)
);
reg [31:0] LXe_QHVETO_PROGDLY;
always @(posedge ALGCLK) begin
   LXe_QHVETO_PROGDLY <= {LXe_QHVETO_PROGDLY[30:0], LXe_QH_VETO_FIRE};
end
assign LXe_QH_VETO = (DETECTORDLY[5])?LXe_QHVETO_PROGDLY[DETECTORDLY[4:0]]:LXe_QH_VETO_FIRE;

// AT THE END DISCRIMINATION WITH QLTHR
wire LXe_QL_PROMPT;
DISCRIMINATEWFM #(
    .LENGTH(10)
) QLDISCR(
    .INPUT(LXeQSUM[34:3]),
    .THR(QLTHR),
    .CLK(ALGCLK),
    .TRIG(LXe_QL_PROMPT)
);

reg [3:0] LXe_QL_DLY;
always @(posedge ALGCLK) begin
    LXe_QL_DLY <= {LXe_QL_DLY[2:0], LXe_QL_PROMPT};
end
reg [31:0] LXe_QL_PROGDLY;
always @(posedge ALGCLK) begin
   LXe_QL_PROGDLY <= {LXe_QL_PROGDLY[30:0], LXe_QL_DLY[3]};
end
assign LXe_QL = (DETECTORDLY[5])?LXe_QL_PROGDLY[DETECTORDLY[4:0]]:LXe_QL_DLY[3];

wire LXe_QL_VETO_FIRE;
DISCRVETO QLVETO(
    .INPUT(LXeQSUM[34:3]),
    .THR(QCTHR),
    .THR_TOVETO(QLTHR),
    .DISCR_TOVETO(LXe_QL),
    .CLK(ALGCLK),
    .TRIG(LXe_QL_VETO_FIRE)
);
reg [31:0] LXe_QLVETO_PROGDLY;
always @(posedge ALGCLK) begin
   LXe_QLVETO_PROGDLY <= {LXe_QLVETO_PROGDLY[30:0], LXe_QL_VETO_FIRE};
end
assign LXe_QL_VETO = (DETECTORDLY[5])?LXe_QLVETO_PROGDLY[DETECTORDLY[4:0]]:LXe_QL_VETO_FIRE;

// AT THE END DISCRIMINATION WITH QCTHR
wire LXe_QCOSM_FIRE;
DISCRIMINATEWFM #(
    .LENGTH(20)
) QCDISCR (
    .INPUT(LXeQSUM[34:3]),
    .THR(QCTHR),
    .CLK(ALGCLK),
    .TRIG(LXe_QCOSM_FIRE)
);
reg [31:0] LXe_QCOSM_PROGDLY;
always @(posedge ALGCLK) begin
   LXe_QCOSM_PROGDLY <= {LXe_QCOSM_PROGDLY[30:0], LXe_QCOSM_FIRE};
end
assign LXe_QCOSM = (DETECTORDLY[5])?LXe_QCOSM_PROGDLY[DETECTORDLY[4:0]]:LXe_QCOSM_FIRE;

// DISCRIMINATION FOR NEUTRON GENERATOR
wire LXe_QNGEN_PROMPT;
DISCRIMINATEWFM #(
    .LENGTH(10)
) QNGENDISCR(
    .INPUT(LXeQSUM[34:3]),
    .THR(QNGENTHR_LOW),
    .CLK(ALGCLK),
    .TRIG(LXe_QNGEN_PROMPT)
);

reg [3:0] LXe_QNGEN_DLY;
always @(posedge ALGCLK) begin
    LXe_QNGEN_DLY <= {LXe_QNGEN_DLY[2:0], LXe_QNGEN_PROMPT};
end

wire LXe_QNGEN_VETO;
DISCRVETO QNGENVETO(
    .INPUT(LXeQSUM[34:3]),
    .THR(QNGENTHR_HIGH),
    .THR_TOVETO(QNGENTHR_LOW),
    .DISCR_TOVETO(LXe_QNGEN_DLY[3]),
    .CLK(ALGCLK),
    .TRIG(LXe_QNGEN_VETO)
);

wire LXe_QNGEN_FIRE;
assign LXe_QNGEN_FIRE = LXe_QNGEN_DLY[3] & ~(LXe_QNGEN_VETO);
reg [31:0] LXe_QNGEN_PROGDLY;
always @(posedge ALGCLK) begin
   LXe_QNGEN_PROGDLY <= {LXe_QNGEN_PROGDLY[30:0], LXe_QNGEN_FIRE};
end
assign LXe_QNGEN = (DETECTORDLY[5])?LXe_QNGEN_PROGDLY[DETECTORDLY[4:0]]:LXe_QNGEN_FIRE;

//PATCH processing
wire [7:0] LXeMAX_REG;
wire LXeMAX_VALID;
wire LXe_PATCH_FIRE;
PATCHTRG PATCHTRGBLOCK(
    .ALGCLK(ALGCLK),
    .CLK(CLK),
    //inputs from WFMTRG
    .TDCNUM(LXeTDCNUM),
    .MAX(LXeMAX),
    //configurations
    .RLXe_PATCH(RLXe_PATCH),
    .RLXe_PATCH_DLY(RLXe_PATCH_DLY),
    .RLXe_HITTHR(RLXe_HITTHR),
    //bus
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WDATA(WDATA),
    .RCLK(RCLK),
    .RDATA(RDATA),
    .WENA(WENA),
    //outputs
    .PATCHTRG(LXe_PATCH_FIRE),
    .XECMAX_VALID(LXeMAX_VALID),
    .XECMAX(LXeMAX_REG)
);
reg [31:0] LXe_PATCH_PROGDLY;
always @(posedge ALGCLK) begin
   LXe_PATCH_PROGDLY <= {LXe_PATCH_PROGDLY[30:0], LXe_PATCH_FIRE};
end
assign LXe_PATCH = (DETECTORDLY[5])?LXe_PATCH_PROGDLY[DETECTORDLY[4:0]]:LXe_PATCH_FIRE;

//TC PROCESSING
wire TC_SINGLE_PROMPT;
wire [6:0] TC_NHIT;
wire [3:0] TC_TRACKHIT;
wire [5*4-1:0] TC_TRACKTILETIME;
wire [9*4-1:0] TC_TRACKTILEID;

TCTRG TCTRGBLOCK(
     .DATA(DATA),
     .CLK(CLK),
     .OR(TC_SINGLE_PROMPT),
     .TRACKHIT(TC_TRACKHIT),
     .TRACKTILETIME(TC_TRACKTILETIME),
     .TRACKTILEID(TC_TRACKTILEID),
     .TCNHIT(TC_NHIT)
);

// TC SINGLE SHAPER
wire TCSINGLE_FIRE;
SHAPER #(.SRLENGTH(`SRSIZE))
TCSINGLESHAPER(
  .DIN(TC_SINGLE_PROMPT),
  .DOUT(TCSINGLE_FIRE),
  .CLK(ALGCLK)  
);

reg [31:0] TCSINGLE_PROGDLY;
always @(posedge ALGCLK) begin
   TCSINGLE_PROGDLY <= {TCSINGLE_PROGDLY[30:0], TCSINGLE_FIRE};
end
assign TC_SINGLE = (DETECTORDLY[13])?TCSINGLE_PROGDLY[DETECTORDLY[12:8]]:TCSINGLE_FIRE;

//TC MULTIPLICITY THRESHOLD
reg TC_MULTIPLICITY_PROMPT;
always @(posedge CLK) begin
    if (TC_NHIT >= TCHITTHR && TCHITTHR != 0) begin
       TC_MULTIPLICITY_PROMPT <= 1'b1;
    end else begin
       TC_MULTIPLICITY_PROMPT <= 1'b0;
    end
end
// TC MULTIPLICITY SHAPER
SHAPER #(.SRLENGTH(`SRSIZE))
TCMULTSHAPER(
  .DIN(TC_MULTIPLICITY_PROMPT),
  .DOUT(TC_MULTIPLICITY),
  .CLK(ALGCLK)  
);

// AUX DECODING
wire BGOPRESHCOUNTER_FIRE;
wire BGOTRG_FIRE;
wire RDCTRG_FIRE;
wire CRCPAIRTRG_FIRE;
wire T_ALPHA_PROMPT; // to alfa trigger shaper

AUXTRG AUXTRGBLOCK(
    .DATA(DATA),
    .CLK(CLK),
    .BGOCOSM(BGOCOSM),
    .BGOTHRFIRE(BGOTHRFIRE),
    .BGOPRESHCOUNTER(BGOPRESHCOUNTER_FIRE),
    .BGOTRG(BGOTRG_FIRE),
    .RDCPLASTICSINGLE(RDCPLASTICSINGLE),
    .RDCLYSOOR(RDCLYSOOR),
    .RDCLYSOTHRFIRE(RDCLYSOTHRFIRE),
    .RDCTRG(RDCTRG_FIRE),
    .TCLASERTRG(TCLASERTRG),
    .TCDIODETRG(TCDIODETRG),
    .XECLEDPMTTRG(XECLEDPMTTRG),
    .XECLEDMPPCTRG(XECLEDMPPCTRG),
    .RFACCELTRG(RFACCELTRG),
    .PROTONCURRENTTRG(PROTONCURRENTTRG),
    .MONITORDCTRG(MONITORDCTRG),
    .ALFATRG(T_ALPHA_PROMPT),
    .NGENTRG(NGENTRG),
    .CRCSINGLETRG(CRCSINGLETRG),
    .CRCPAIRTRG(CRCPAIRTRG_FIRE)
);

// DELAYS TO ALIGN DIFFERENT DETECTORS
reg [31:0] BGOPRESHCOUNTER_PROGDLY;
reg [31:0] BGOTRG_PROGDLY;
reg [31:0] RDCTRG_PROGDLY;
reg [31:0] CRCPAIRTRG_PROGDLY;

always @(posedge ALGCLK) begin
   BGOPRESHCOUNTER_PROGDLY <= {BGOPRESHCOUNTER_PROGDLY[30:0], BGOPRESHCOUNTER_FIRE};
   BGOTRG_PROGDLY <= {BGOTRG_PROGDLY[30:0], BGOTRG_FIRE};
   RDCTRG_PROGDLY <= {RDCTRG_PROGDLY[30:0], RDCTRG_FIRE};
   CRCPAIRTRG_PROGDLY <= {CRCPAIRTRG_PROGDLY[30:0], CRCPAIRTRG_FIRE};
end
assign BGOPRESHCOUNTER = (DETECTORDLY[29])?BGOPRESHCOUNTER_PROGDLY[DETECTORDLY[28:24]]:BGOPRESHCOUNTER_FIRE;
assign BGOTRG = (DETECTORDLY[29])?BGOTRG_PROGDLY[DETECTORDLY[28:24]]:BGOTRG_FIRE;
assign RDCTRG = (DETECTORDLY[37])?RDCTRG_PROGDLY[DETECTORDLY[36:32]]:RDCTRG_FIRE;
assign CRCPAIRTRG = (DETECTORDLY[45])?CRCPAIRTRG_PROGDLY[DETECTORDLY[44:40]]:CRCPAIRTRG_FIRE;

// ALPHA SHAPER
SHAPER #(.SRLENGTH(`SRSIZE))
ALPHASHAPER(
  .DIN(T_ALPHA_PROMPT),
  .DOUT(T_ALPHA),
  .CLK(ALGCLK)  
);

// LUT for direction match narrow
wire [3:0] DM_NARROW_PROMPT;
wire [3:0] DM_WIDE_PROMPT;
reg  [3:0] DM_VALID;

genvar itrack;
for(itrack = 0; itrack<4; itrack = itrack+1) begin
   always @(posedge CLK) begin
      DM_VALID[itrack] <= TC_TRACKHIT[itrack] & LXeMAX_VALID;
   end

   // LUT DM NARROW INSTANTIATION
   DMLUT #(
        //.BASEADDR(`DMNarrowAddr+itrack*32'h1000),
        .DMWIDE(0)
   )
        DirMatchNLUT (
        .LXeID(LXeMAX_REG),
        .TCID(TC_TRACKTILEID[9*itrack+:9]),
        .ISDM(DM_NARROW_PROMPT[itrack]),
        .CLK(CLK),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .WDATA(WDATA),
        .RDATA(RDATA)
       );
   // DM NARROW SHAPER
   SHAPER #(.SRLENGTH(`SRSIZE))
   DMNSHAPER(
     .DIN(DM_NARROW_PROMPT[itrack] & DM_VALID[itrack]),
     .DOUT(DM_NARROW[itrack]),
     .CLK(ALGCLK)  
   );

   // LUT DMWIDE INSTANTIATION
   DMLUT #(
   //.BASEADDR(`DMWideAddr+itrack*32'h1000)
        .DMWIDE(1)
   )
        DirMatchWLUT (
        .LXeID(LXeMAX_REG),
        .TCID(TC_TRACKTILEID[9*itrack+:9]),
        .ISDM(DM_WIDE_PROMPT[itrack]),
        .CLK(CLK),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .WDATA(WDATA),
        .RDATA(RDATA)
       );
   // DM WIDE SHAPER
   SHAPER #(.SRLENGTH(`SRSIZE))
   DMWSHAPER(
     .DIN(DM_WIDE_PROMPT[itrack]&DM_VALID[itrack]),
     .DOUT(DM_WIDE[itrack]),
     .CLK(ALGCLK)  
   );
end

// LXe - TC TIME CONICIDENCES
wire [4-1:0] TIMEN_PROMPT;
wire [4-1:0] TIMEW_PROMPT;

//stretch XEC time for coincidence
wire [8:0] LXeTDCSUMStretched;
wire [4:0] LXeTDCNUMStretched;
wire LXeHasHit;
XECTDCSTRETCH XECSTRETCH(
   .CLK(CLK),
   .HITIN(LXeTDCNUM!=0),
   .NTDCIN(LXeTDCNUM),
   .TIMEIN(LXeTDCSUM),
   .ADDVAL({LXeTDCNUM, 3'b000}),
   .HITOUT(LXeHasHit),
   .NTDCOUT(LXeTDCNUMStretched),
   .TIMEOUT(LXeTDCSUMStretched)
);

genvar itime;
for(itime = 0; itime<4; itime = itime+1) begin
    //stretch each TC time
    wire track_hit;
    wire [5:0] track_time;
    TCTDCSTRETCH TCSTRETCH(
       .CLK(CLK),
       .HITIN(TC_TRACKHIT[itime]),
       .TIMEIN(TC_TRACKTILETIME[5*itime+:5]),
       .ADDVAL(5'b10000),
       .HITOUT(track_hit),
       .TIMEOUT(track_time)
    );

    //compare against thresholds
    TIMECOMPARE TIMECOMPAREN_BLOCK(
       .CLK(CLK),
       .LXETIME(LXeTDCSUMStretched),
       .LXENUM(LXeTDCNUMStretched),
       .LXEHIT(LXeHasHit),
       .TCTIME(track_time),
       .TCHIT(track_hit),
       .THR(RTIMN[5:0]),
       .ISTIME(TIMEN_PROMPT[itime])
    );
    TIMECOMPARE TIMECOMPAREW_BLOCK(
       .CLK(CLK),
       .LXETIME(LXeTDCSUMStretched),
       .LXENUM(LXeTDCNUMStretched),
       .LXEHIT(LXeHasHit),
       .TCTIME(track_time),
       .TCHIT(track_hit),
       .THR(RTIMW[5:0]),
       .ISTIME(TIMEW_PROMPT[itime])
    );

   // TIME NARROW SHAPER
   SHAPER #(.SRLENGTH(`SRSIZE)
   ) TMNSHAPER(
     .DIN(TIMEN_PROMPT[itime]),
     .DOUT(TIMEN[itime]),
     .CLK(ALGCLK)  
   );
  // TIME WIDE SHAPER
  SHAPER #(.SRLENGTH(`SRSIZE))
  TMWSHAPER(
    .DIN(TIMEW_PROMPT[itime]),
    .DOUT(TIMEW[itime]),
    .CLK(ALGCLK)  
  );    
end
  
endmodule
