`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.02.2017 13:57:28
// Design Name: 
// Module Name: ONELUT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ONELUT(
    input [INSIZE-1:0] DATAIN,
    input CLK,
    output reg [OUTSIZE-1:0] DATAOUT,
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [31:0] WDATA,
    input RCLK,
    output [31:0] RDATA,
    input WENA
    );
    
    parameter OUTSIZE = 1;
    parameter INSIZE = 15;
    parameter MEMFILE = "test.mem";
    
    (* rom_style = "block" *) reg [OUTSIZE-1:0] myrom [0:(2**INSIZE)-1];
    
    always @(posedge CLK) begin
        DATAOUT <= myrom[DATAIN];
    end

    assign RDATA = 32'hZZZZZZZZ;
    
    initial begin
        $readmemh(MEMFILE, myrom);
    end
    
endmodule
