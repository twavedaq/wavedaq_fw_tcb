`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 09/30/2016 04:32:45 PM
// Design Name: 
// Module Name: WFMTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// here we perform the sum of 16 samples and compare it with a threshold
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module PATCHTRG(
    input ALGCLK,
    input CLK,
    //inputs from WFMTRG
    input [4:0] TDCNUM,
    input [7:0] MAX,
    //configurations
    input [31:0] RLXe_PATCH,
    input [31:0] RLXe_PATCH_DLY,
    input [31:0] RLXe_HITTHR,
    //bus
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [31:0] WDATA,
    input RCLK,
    output [31:0] RDATA,
    input WENA,
    //outputs
    output PATCHTRG,
    output reg XECMAX_VALID,
    output reg [7:0] XECMAX
    );

// register calculate valid and pipeline max
always @(posedge CLK) begin
   if(TDCNUM >= RLXe_HITTHR[4:0]) begin
      XECMAX_VALID <= 1'b1;
   end else begin
      XECMAX_VALID <= 1'b0;
   end

   XECMAX <= MAX;
end

// lookup table for patch selection
wire LXe_PATCH_PROMPT;
ONELUT  #(
//.BASEADDR(`LXePatchAddr),
.INSIZE(15),
.OUTSIZE(1),
.MEMFILE("patch.mem")
)
    LXePatch_inst (
    .DATAIN({RLXe_PATCH[6:0], MAX}),
    .CLK(CLK),
    .DATAOUT(LXe_PATCH_PROMPT),
    .RCLK(RCLK),
    .WENA(WENA),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WDATA(WDATA),
    .RDATA(RDATA)
);

//DLY to align patch algorithm with SUM WFM
//To be changed with WD2G
reg [32-1:0] LXe_PATCH_SR;
wire LXe_PATCH_DLY;
assign LXe_PATCH_DLY = LXe_PATCH_SR[RLXe_PATCH_DLY[4:0]];
always @(posedge CLK)
begin
   LXe_PATCH_SR <= {LXe_PATCH_SR[32-2:0], LXe_PATCH_PROMPT & XECMAX_VALID};
end

assign PATCHTRG = (RLXe_PATCH_DLY[5])?LXe_PATCH_DLY:LXe_PATCH_PROMPT;
endmodule
