`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.09.2015 18:06:01
// Design Name: 
// Module Name: PEDBLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PEDBLOCK(
    input PEDCLK,
    input CLK,
    output reg ISTRG
    );
// PEDESTAL: I DIVIDE BY 2^10 THE 7 MHZ INPUT FREQUENCY TO HAVE ~ 7 KHZ OF PEDESTAL TRG RATE
// To be further divided by presca block
reg [9:0] PEDCOU;
reg COUNT_END;
//wire ISTRG;
//PEDESTAL COUNTER
always @(posedge PEDCLK) begin
//    if(SYNC) begin
//        PEDCOU <= 0;  //RESET PEDCOU
//    end
//    else
     begin
        PEDCOU <= PEDCOU+1; //INCREASE PEDCOU
    end
    COUNT_END <= (&PEDCOU);
end

//registered output on CLK (clock domain crossing)
reg ISTRG_1;
always @(posedge CLK) begin
    ISTRG_1 <= COUNT_END;
    ISTRG <= ISTRG_1;
end
    
endmodule
