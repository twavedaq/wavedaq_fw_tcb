`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.02.2017 14:10:34
// Design Name: 
// Module Name: TRI_PROCESS
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module PROCESS(
    // GENERAL
    input CLK,
    input ALGCLK,
    input PEDCLK,
    input RUNMODE,
    input SYNC,
    output T_PED,
    // input data
    input [1023:0] DATA,
    // bus for memories
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [31:0] WDATA,
    input RCLK,
    output [31:0] RDATA,
    input WENA,
    // SCIFI
    // scaler data
    output [(32*(`SCIFIFIBERS+1))-1:0] SCIFIDATA,
    output [(32*((`SCIFIFIBERS/2)*(`SCIFIFIBERS/2)))-1:0]SCIFICOINCDATA,
    output SCIFITRG,
    // MATRIX logic
    input [`MATRIXCHANNELS-1:0] MATRIXMASK,
    output MATRIXTRG,
    // SINGLE CRATE
    input [4:0] SINGLECRATE_SHAPER_VAL,
    input [4:0] SINGLECRATE_SHAPER_VAL_VETO,
    input [255:0] SINGLECRATE_ISVETO,
    input [255:0] SINGLECRATE_MASK,
    input [127:0] SINGLECRATE_TWOCHANNEL_SEL,
    input SINGLECRATE_LOGIC_SEL,
    output SINGLECRATETRG
    );

assign RDATA = 32'hZZZZZZZZ;

/////////////////////////
// GENERAL
//RANDOM TRIGGER
PEDBLOCK PEDBLOCK_INST(
     .PEDCLK(PEDCLK),
     .CLK(CLK),
     .ISTRG(T_PED)
); 

/////////////////////////
// SCIFI LOGIC WITH FIBER HIT COUNTER
`ifdef SCIFI
SCIFI_BLOCK SCIFI(
    .DATA(DATA),
    .SHAPER_VAL(SINGLECRATE_SHAPER_VAL),
    .CLK(CLK),
    .RUNMODE(RUNMODE),
    .SYNC(SYNC),
    .SCIFITRG(SCIFITRG),
    .SCIFIDATA(SCIFIDATA),
    .SCIFICOINCDATA(SCIFICOINCDATA)
);

MATRIX_BLOCK MATRIX(
    .DATA(DATA),
    .MASK(MATRIXMASK),
    .CLK(CLK),
    .MATRIXTRG(MATRIXTRG)
);
`else
assign SCIFIDATA = 0;
assign SCIFICOINCDATA = 0;
assign SCIFITRG = 0;
assign MATRIXTRG = 0;
`endif

`ifdef LOLX

//READY TO GO!

`endif

/////////////////////////
// SINGLE CRATE LOGIC
SINGLECRATE_TRG SINGLECRATE(
    .DATA(DATA),
    .SHAPER_VAL(SINGLECRATE_SHAPER_VAL),
    .SHAPER_VAL_VETO(SINGLECRATE_SHAPER_VAL_VETO),
    .ISVETO(SINGLECRATE_ISVETO),
    .MASK(SINGLECRATE_MASK),
    .CLK(CLK),
    .TWOCHANNEL_SEL(SINGLECRATE_TWOCHANNEL_SEL),
    .LOGIC_SEL(SINGLECRATE_LOGIC_SEL),
    .TRG(SINGLECRATETRG)
    );



  
endmodule
