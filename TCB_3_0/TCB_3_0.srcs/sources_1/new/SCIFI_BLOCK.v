`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 03.01.2019 11:16:52
// Design Name: 
// Module Name: SCIFI_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "global_parameters.vh"

module SCIFI_BLOCK(
    input [1023:0] DATA,
    input [4:0] SHAPER_VAL,
    input CLK,
    input RUNMODE,
    input SYNC,
    output SCIFITRG,
    output [(32*(`SCIFIFIBERS+1))-1:0] SCIFIDATA,
    output [(32*((`SCIFIFIBERS/2)*(`SCIFIFIBERS/2)))-1:0]SCIFICOINCDATA
    );
    
// EXTRACTS SCIFI HITS FROM THE DATA STREAM
wire [`SCIFICHANNELS - 1:0] SCIFIHIT;
genvar iSlot;
for(iSlot = `SCIFIFIRSTSLOT; iSlot<(`SCIFILASTSLOT+1); iSlot = iSlot+1) begin
    assign SCIFIHIT[16*(iSlot-`SCIFIFIRSTSLOT)+:16] = DATA[(64*iSlot)+48 +:16];
end

// CREATE THE FIBER HITS
reg [`SCIFIFIBERS-1:0] FIBHIT;
reg FIBOR;
genvar fHit;
for(fHit = 0; fHit<`SCIFIFIBERS; fHit= fHit+1) begin
    always @(posedge CLK) begin
        FIBHIT[fHit] <= SCIFIHIT[fHit*2] & SCIFIHIT[fHit*2+1];
    end//always
end// for

always @(posedge CLK) begin
   FIBOR = |FIBHIT;
end//always
assign SCIFITRG = FIBOR;

// MAKE INTER-FIBER COINCIDENCE
//reg [440:0] FIBCO;
reg [(`SCIFIFIBERS/2)*(`SCIFIFIBERS/2)-1:0] FIBCOINC;
genvar fibA;
genvar fibB;
for(fibA = 0; fibA < (`SCIFIFIBERS/2); fibA = fibA +1) begin
   for(fibB = (`SCIFIFIBERS/2); fibB < `SCIFIFIBERS; fibB = fibB +1) begin
      always @(posedge CLK) begin
         FIBCOINC[fibA+(`SCIFIFIBERS/2)*(fibB-(`SCIFIFIBERS/2))] <= FIBHIT[fibA] & FIBHIT[fibB];
      end
   end
end


// CREATE THE COUNTERS
genvar fibCou;
for(fibCou = 0; fibCou<`SCIFIFIBERS; fibCou= fibCou+1) begin
     TRGCOUNTERS_BLOCK SCIFICOUNTERS_INST(
        .ISTRG(FIBHIT[fibCou]), 
        .SYNC(SYNC),
        .RUNMODE(RUNMODE),
        .TRGCOUNTER(SCIFIDATA[fibCou*32+:32]),
        .CLK(CLK),
        .TRGCOUMODE(1'b1)
    );
end

genvar fibCoincCou;
for(fibCoincCou = 0; fibCoincCou<(`SCIFIFIBERS/2)*(`SCIFIFIBERS/2); fibCoincCou= fibCoincCou+1) begin
     TRGCOUNTERS_BLOCK SCIFICOUNTERS_INST(
        .ISTRG(FIBCOINC[fibCoincCou]), 
        .SYNC(SYNC),
        .RUNMODE(RUNMODE),
        .TRGCOUNTER(SCIFICOINCDATA[fibCoincCou*32+:32]),
        .CLK(CLK),
        .TRGCOUMODE(1'b1)
    );
end


// EXTRACT PROTON CURRENT
wire PROTONCURRENT;
assign PROTONCURRENT = DATA[(64*`SCIFILASTSLOT)+63];

// PROTONCURRENT SHAPER
//wire PROTONCURRENT_SH;
//PROG_SHAPER #(
   //.VAL_SIZE(5)
//) CHANNEL_PCURRSH(
   //.IN(PROTONCURRENT),
   //.OUT(PROTONCURRENT_SH),
   //.CLK(CLK),
   //.VAL(SHAPER_VAL)
//);

// PROTONCURRENT COUNTER
TRGCOUNTERS_BLOCK PCURRCOUNTERS_INST(
   //.ISTRG(PROTONCURRENT_SH), 
   .ISTRG(PROTONCURRENT), 
   .SYNC(SYNC),
   .RUNMODE(RUNMODE),
   .TRGCOUNTER(SCIFIDATA[`SCIFIFIBERS*32+:32]),
   .CLK(CLK),
   .TRGCOUMODE(1'b1)
);



endmodule
