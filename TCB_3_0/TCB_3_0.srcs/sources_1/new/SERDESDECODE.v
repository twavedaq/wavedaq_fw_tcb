`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.11.2016 17:03:29
// Design Name: 
// Module Name: SERDESDECODE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module decodes serdes data from TCB_1
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SERDESDECODE(
    input [64*NSLOT-1:0] DATA,
    //XEC-like connection
    output [30:0] WFMSUM,
    output [5:0] MAX,
    output [7:0] TDCSUM,
    output [4:0] TDCNUM,
    //TC-like connection
    output TCOR,
    output [1:0] TCHITUS,
    output [2*8-1:0] TCTILEIDUS,
    output [2*5-1:0] TCTILETIMEUS,
    output [1:0] TCHITDS,
    output [2*8-1:0] TCTILEIDDS,
    output [2*5-1:0] TCTILETIMEDS,
    output [6:0] TCNHIT,
    //AUX-like connection
    output CRCPAIRTRG,
    output CRCSINGLETRG,
    output BGOPRESHCOUNTER,
    output NGENTRG,
    output ALFATRG,
    //output RDCPLASTICLYSOSINGLE,
    //output BGOVETOFIRE,
    output MONITORDCTRG,
    output PROTONCURRENTTRG,
    output RFACCELTRG,
    output XECLEDMPPCTRG,
    output XECLEDPMTTRG,
    output TCDIODETRG,
    output TCLASERTRG,
    output BGOTHRFIRE,
    output BGOCOSM,
    output BGOTRG,
    output RDCPLASTICSINGLE,
    output RDCLYSOOR,
    output RDCLYSOTHRFIRE,
    output RDCTRG
    );

    parameter SLOT=0;
    parameter NSLOT=1;
    
    //decode XEC
    // CONNECTION SCHEME:
    //  Di :BIT 30-0 FOR SUM OF WAVEFORMs
    //     :BIT 36-31 FOR PATCH ID WITH MOST TDCs
    //     :BIT 55-48 FOR SUM OF TDCs
    //     :BIT 60-56 FOR NUM OF TDCs
    //     :BIT 63 FOR ANY TDC OVER THRESHOLD
    assign WFMSUM = DATA[64*SLOT+:31];
    assign MAX = DATA[64*SLOT+31+:6];
    assign TDCSUM = DATA[64*SLOT+48+:8];
    assign TDCNUM = DATA[64*SLOT+56+:5];

    //decode TC
    //  Di :BIT 0 FOR ANY TC TILE
    //     :BIT 1 FOR TRACK0US HIT
    //     :BIT 9-2 FOR TRACK0US ID
    //     :BIT 14-10 FOR TRACK0US TIME
    //     :BIT 15 FOR TRACK1US HIT
    //     :BIT 23-16 FOR TRACK1US ID
    //     :BIT 28-24 FOR TRACK1US TIME
    //     :BIT 29 FOR TRACK0DS HIT
    //     :BIT 37-30 FOR TRACK0DS ID
    //     :BIT 42-38 FOR TRACK0DS TIME
    //     :BIT 43 FOR TRACK1DS HIT
    //     :BIT 51-44 FOR TRACK1DS ID
    //     :BIT 56-52 FOR TRACK1DS TIME
    //     :BIT 63-57 FOR MULTIPLICITY
    assign TCOR = DATA[64*SLOT];
    assign TCHITUS = {DATA[64*SLOT+15], DATA[64*SLOT+1]};
    assign TCTILEIDUS = {DATA[64*SLOT+16+:8], DATA[64*SLOT+2+:8]};
    assign TCTILETIMEUS = {DATA[64*SLOT+24+:5], DATA[64*SLOT+10+:5]};
    assign TCHITDS = {DATA[64*SLOT+43], DATA[64*SLOT+29]};
    assign TCTILEIDDS = {DATA[64*SLOT+44+:8], DATA[64*SLOT+30+:8]};
    assign TCTILETIMEDS = {DATA[64*SLOT+52+:5], DATA[64*SLOT+38+:5]};
    assign TCNHIT = DATA[64*SLOT+57+:7];

    //decode AUX
    //  Di :BIT 30-0 SUM of PMTs
    //     :BIT 49-31 TRIGGER
    //assign PMTSUM = DATA[64*SLOT+:31];
    assign {CRCPAIRTRG, CRCSINGLETRG,BGOPRESHCOUNTER,NGENTRG,ALFATRG,MONITORDCTRG,PROTONCURRENTTRG,RFACCELTRG,XECLEDMPPCTRG,XECLEDPMTTRG,TCDIODETRG,TCLASERTRG,BGOTHRFIRE,BGOCOSM,BGOTRG,RDCPLASTICSINGLE,RDCLYSOOR,RDCLYSOTHRFIRE,RDCTRG} = DATA[64*SLOT+31+:19];

    
endmodule
