`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.02.2016 16:42:27
// Design Name: 
// Module Name: ALLSERDES
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module SERDES_MASTERWD(
    //SERDES CONNECTIONS
    input SERDES_CLK,
    input CLK,
    input SYNC,
    input [7:0] WDB_RX0_D_P,
    input [7:0] WDB_RX0_D_N,
    input [7:0] WDB_RX1_D_P,
    input [7:0] WDB_RX1_D_N,
    input [7:0] WDB_RX2_D_P,
    input [7:0] WDB_RX2_D_N,
    input [7:0] WDB_RX3_D_P,
    input [7:0] WDB_RX3_D_N,
    input [7:0] WDB_RX4_D_P,
    input [7:0] WDB_RX4_D_N,
    input [7:0] WDB_RX5_D_P,
    input [7:0] WDB_RX5_D_N,
    input [7:0] WDB_RX6_D_P,
    input [7:0] WDB_RX6_D_N,
    input [7:0] WDB_RX7_D_P,
    input [7:0] WDB_RX7_D_N,
    input [7:0] WDB_RX8_D_P,
    input [7:0] WDB_RX8_D_N,
    input [7:0] WDB_RX9_D_P,
    input [7:0] WDB_RX9_D_N,
    input [7:0] WDB_RX10_D_P,
    input [7:0] WDB_RX10_D_N,
    input [7:0] WDB_RX11_D_P,
    input [7:0] WDB_RX11_D_N,
    input [7:0] WDB_RX12_D_P,
    input [7:0] WDB_RX12_D_N,
    input [7:0] WDB_RX13_D_P,
    input [7:0] WDB_RX13_D_N,
    input [7:0] WDB_RX14_D_P,
    input [7:0] WDB_RX14_D_N,
    input [7:0] WDB_RX15_D_P,
    input [7:0] WDB_RX15_D_N,
    output [1023:0] BPDATA,
    //READOUT OF SERDES STATUS and MASKING
    output [`NSERDESBP*8*5-1:0] BPCURRENTDLY,
    output [`NSERDESBP*8*3-1:0] BPCURRENTBISTLIP,
    input [`NSERDESBP-1:0] BPSERDESMASK,
    //AUTOMATIC SERDES CALIBRATION
    input CALIBSTART,
    input RESET_CALIBFSM,
    input PATTERNSERDES,
    input CALIBMASK,
    input BPSERDESMASKENABLE,
    output [`NSERDESBP-1:0] BPCALIBBUSY,
    output [`NSERDESBP-1:0] BPCALIBFAIL,
    output BPALIGNBUSY,
    output BPALIGNFAIL,
    output [`NSERDESBP*4-1:0] BPALIGNOFFSET,
    output [4:0] BPSERDESMIN,
    output [`NSERDESBP-1:0] BPALIGNDLY,
    output [(`NSERDESBP)*32-1:0] BPDLY_TESTED,
    output [(`NSERDESBP)*32-1:0] BPDLY_STATE
    );
    
    wire [8*`NSERDESBP-1:0] INPUT_P;
    wire [8*`NSERDESBP-1:0] INPUT_N;
    assign INPUT_P = {WDB_RX15_D_P, WDB_RX14_D_P, WDB_RX13_D_P, WDB_RX12_D_P, WDB_RX11_D_P, WDB_RX10_D_P, WDB_RX9_D_P, WDB_RX8_D_P, WDB_RX7_D_P, WDB_RX6_D_P, WDB_RX5_D_P, WDB_RX4_D_P, WDB_RX3_D_P, WDB_RX2_D_P, WDB_RX1_D_P, WDB_RX0_D_P};
    assign INPUT_N = {WDB_RX15_D_N, WDB_RX14_D_N, WDB_RX13_D_N, WDB_RX12_D_N, WDB_RX11_D_N, WDB_RX10_D_N, WDB_RX9_D_N, WDB_RX8_D_N, WDB_RX7_D_N, WDB_RX6_D_N, WDB_RX5_D_N, WDB_RX4_D_N, WDB_RX3_D_N, WDB_RX2_D_N, WDB_RX1_D_N, WDB_RX0_D_N};

  
    wire [`NSERDESBP-1:0] BPSERDESMASKLOCAL;
    
    //INPUT FROM WDB
    genvar iSerdes;
    for(iSerdes=0; iSerdes<`NSERDESBP; iSerdes=iSerdes+1) begin
        INPUTSERDES #(
            .nLink(8)
        ) IN(
            .DATA(BPDATA[64*iSerdes+:64]),
            .CLK(SERDES_CLK),
            .CLKDIV(CLK),
            .IN_P(INPUT_P[8*iSerdes+:8]),
            .IN_N(INPUT_N[8*iSerdes+:8]),
            .DATAMASK(BPSERDESMASKLOCAL[iSerdes]|BPSERDESMASK[iSerdes]),
            .CALIBSTART(CALIBSTART),
            .RESET_FSM(RESET_CALIBFSM),
            .BUSY(BPCALIBBUSY[iSerdes]),
            .FAIL(BPCALIBFAIL[iSerdes]),
            .REGENABLE(BPALIGNDLY[iSerdes]),
            .DLY_APPLIED(BPCURRENTDLY[5*8*iSerdes+:5*8]),
            .BISTLIP_APPLIED(BPCURRENTBISTLIP[3*8*iSerdes+:3*8]),
            .DLY_TESTED(BPDLY_TESTED[32*iSerdes+:32]),
            .DLY_STATE(BPDLY_STATE[32*iSerdes+:32]),
            .CALIBMASK(CALIBMASK)
        );
    end
    
    //FSM to align serdes
    SERDESALIGNFSM #(
        .nSerdes(`NSERDESBP)
    ) FSM(
        .SERDESDATA(BPDATA),
        .SERDESBUSY(BPCALIBBUSY),
        .SERDESFAIL(BPCALIBFAIL),
        .CLK(CLK),
        .START(CALIBSTART),
        .RESET_FSM(RESET_CALIBFSM),
        .MASKENABLE(BPSERDESMASKENABLE),
        .SERDESMASK(BPSERDESMASKLOCAL),
        .SERDESDLY(BPALIGNDLY),
        .SERDESOFFSET(BPALIGNOFFSET),
        .SERDESMIN(BPSERDESMIN),
        .BUSY(BPALIGNBUSY),
        .FAIL(BPALIGNFAIL)
        );
    

endmodule
