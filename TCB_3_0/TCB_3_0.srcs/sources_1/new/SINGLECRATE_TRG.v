`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/26/2018 09:09:42 PM
// Design Name: 
// Module Name: SINGLECRATE_TRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module SINGLECRATE_TRG(
    input [1023:0] DATA,
    input [4:0] SHAPER_VAL,
    input [4:0] SHAPER_VAL_VETO,
    input [255:0] ISVETO,
    input [255:0] MASK,
    input CLK,
    input [16*8-1:0] TWOCHANNEL_SEL,
    input LOGIC_SEL,
    output reg TRG
    );
    
    // EXTRACTS HITS FROM THE DATA STREAM
    wire [16*16-1:0] SLOTHIT;
    genvar iSlot;
    for(iSlot =0; iSlot<16; iSlot= iSlot+1) begin
        assign SLOTHIT[(16*iSlot)+:16] = DATA[(64*iSlot)+48+:16];
    end
    
    // HIT SHAPERS
    wire [16*16-1:0] SLOTHIT_SH;
    genvar iHit;
    for(iHit =0; iHit<256; iHit= iHit+1) begin
        PROG_SHAPER #(
            .VAL_SIZE(5)
        ) CHANNEL_SHAPER(
            .IN(SLOTHIT[iHit]),
            .OUT(SLOTHIT_SH[iHit]),
            .CLK(CLK),
            .VAL((ISVETO[iHit])?SHAPER_VAL_VETO:SHAPER_VAL)
        );
    end
    
    // THIS BLOCK MAKE THE PAIR COINCIDENCES
    // IF MASK == 0 ANY HIT IS CANCELLED IN THE LOGIC
    // IF VETO == 1 ANY HIT IS USED AS A VETO, INVERTED IN THE COINCIDENCE
    // IF TWO_CHANNEL_SEL IS 0 THEN THE OR OF THE TWO CHANNELS (AFTER mASK AND VETO) IS PERFORMED
    // IF TWO_CHANNEL_SEL IS 1 THEN THE AND OF THE TWO CHANNELS IS PERFORMDED
    wire [127:0]TWOCHANNEL_OUT;
    genvar iHitPair;
    for(iHitPair =0; iHitPair<128; iHitPair= iHitPair+1) begin
        TWOCHANNEL TWO_CHANNEL_LOGIC (
            .A(SLOTHIT_SH[2*iHitPair]),
            .B(SLOTHIT_SH[2*iHitPair+1]),
            .ISVETO_A(ISVETO[2*iHitPair]),
            .ISVETO_B(ISVETO[2*iHitPair+1]),
            .MASK_A(MASK[2*iHitPair]),
            .MASK_B(MASK[2*iHitPair+1]),
            .CLK(CLK),
            .SEL(TWOCHANNEL_SEL[iHitPair]),
            .OUT(TWOCHANNEL_OUT[iHitPair])
        );
    end
    
    always @(posedge CLK) begin
        if(LOGIC_SEL) begin
            TRG <= &TWOCHANNEL_OUT;
        end else begin
            TRG <= |TWOCHANNEL_OUT;
        end
    end
    
     
endmodule
