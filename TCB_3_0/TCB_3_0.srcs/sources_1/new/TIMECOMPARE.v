`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.05.2019 11:04:29
// Design Name: 
// Module Name: TIMECOMPARE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module makes coincidence and compares two TDC values, one from LXe and one from TC track
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


(* use_dsp48 = "yes" *) module TIMECOMPARE(
    input CLK,
    //input quantities
    input LXEHIT,
    input [8:0] LXETIME,
    input [4:0] LXENUM,
    input TCHIT,
    input [5:0] TCTIME,
    //parameters
    input [5:0] THR,
    //outputs
    output reg ISTIME
    );

    reg signed [10:0] LXETIME_R;
    reg signed [10:0] LXETIME_RR;
    reg signed [6:0] THR_R;
    reg signed [6:0] TCTIME_R;
    reg signed [5:0] LXENUM_R;

    reg VALID;
    reg VALID_R;
    reg VALID_RR;

    //generate output valid
    always @(posedge CLK) begin
       VALID <= LXEHIT & TCHIT;
       VALID_R <= VALID;
       VALID_RR <= VALID_R;
    end

    //register inputs, double latency on 
    always @(posedge CLK) begin
       LXETIME_R <= $signed({1'b0,LXETIME, 1'b0});//add one bit so that LSB is 1/16 of clock
       LXETIME_RR <= LXETIME_R;

       THR_R <= $signed({1'b0, THR});
       TCTIME_R <= $signed({1'b0,TCTIME});
       LXENUM_R <= $signed({1'b0,LXENUM});
    end

    //pre adders
    wire signed [7:0] TC_PLUS_THR;
    wire signed [7:0] TC_MINUS_THR;
    assign TC_PLUS_THR = TCTIME_R + THR_R;
    assign TC_MINUS_THR = TCTIME_R - THR_R;

    //multipliers
    reg signed [6+8-1:0] NXEC_TIMES_TC_PLUS_THR;
    reg signed [6+8-1:0] NXEC_TIMES_TC_MINUS_THR;
    always @(posedge CLK) begin
       NXEC_TIMES_TC_PLUS_THR <= LXENUM_R * TC_PLUS_THR;
       NXEC_TIMES_TC_MINUS_THR <= LXENUM_R * TC_MINUS_THR;
    end

    //difference
    reg signed [1+6+8-1:0] XEC_MINUS_NXEC_TIMES_TC_PLUS_THR;
    reg signed [1+6+8-1:0] XEC_MINUS_NXEC_TIMES_TC_MINUS_THR;
    always @(posedge CLK) begin
        XEC_MINUS_NXEC_TIMES_TC_PLUS_THR <= LXETIME_RR - NXEC_TIMES_TC_PLUS_THR;
        XEC_MINUS_NXEC_TIMES_TC_MINUS_THR <= LXETIME_RR - NXEC_TIMES_TC_MINUS_THR;
    end

    //validate output
    always @(posedge CLK) begin
        if(VALID_RR == 1'b1 && XEC_MINUS_NXEC_TIMES_TC_PLUS_THR<0  && XEC_MINUS_NXEC_TIMES_TC_MINUS_THR>0) begin
            ISTIME <= 1'b1;
        end else begin
            ISTIME <= 1'b0;
        end
    end

endmodule
