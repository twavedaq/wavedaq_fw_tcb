`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.01.2019 11:16:52
// Design Name: 
// Module Name: TOF_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "global_parameters.vh"

module TOF_BLOCK(
    input [1023:0] DATA,
    input [4:0] SHAPER_VAL,
    input [4:0] SHAPER_VAL_VETO,
    input CLK,
    input [31:0] MAJTRIG,
    input [63:0] MASKTOFX,
    input [63:0] MASKTOFY,
    output reg TOFTRG,
    output reg FRAGTOFREG,
    output reg FRAGVETO,
    output reg FRAGTOFROMEREG,
    output reg FRAGVETOROME
    );

wire TOFHITLOGIC;
assign TOFHITLOGIC = MAJTRIG[31];
wire TOFTRGLOGIC;
assign TOFTRGLOGIC = MAJTRIG[30];
wire TOFHITLOGICROME;
assign TOFHITLOGICROME = MAJTRIG[29];


//TOF TRIGGER ALONE SETUP
//ONE VIEW HASE 22 BARS, I.E 44 CHANNELS CONNECTED TO 3 WDBS
// EXTRACTS TOF X-VIEW HITS FROM THE DATA STREAM
wire [39:0] TOFXHIT;
// BAR 0 -> 7
assign TOFXHIT[15:0] = DATA[(64*`TOFXSLOT0)+48+:16];
// BAR 8 
assign TOFXHIT[17:16] = DATA[(64*`TOFXSLOT1)+48+:2];
// BAR 9 
assign TOFXHIT[19:18] = DATA[(64*`TOFCENTRALSLOT)+48+:2];
// BAR 10 -> 16 
assign TOFXHIT[33:20] = DATA[(64*`TOFXSLOT1)+50+:14];
// BAR 17 -> 19 
assign TOFXHIT[39:34] = DATA[(64*`TOFXSLOT2)+48+:6];
// EXTRACTS TOF Y-VIEW HITS FROM THE DATA STREAM
wire [39:0] TOFYHIT;
//BAR 0 -> 4 
assign TOFYHIT[9:0] = DATA[(64*`TOFYSLOT0)+54+:10];
//BAR 5 -> 8 
assign TOFYHIT[17:10] = DATA[(64*`TOFYSLOT1)+48+:8];
// BAR 9 
assign TOFYHIT[19:18] = DATA[(64*`TOFCENTRALSLOT)+50+:2];
//BAR 10 -> 13 
assign TOFYHIT[27:20] = DATA[(64*`TOFYSLOT1)+56+:8];
//BAR 14 -> 19 
assign TOFYHIT[39:28] = DATA[(64*`TOFYSLOT2)+48+:12];

// TOF X HIT SHAPERS
wire [39:0] TOFXHIT_SH;
genvar TOFXiHit;
for(TOFXiHit =0; TOFXiHit<40; TOFXiHit= TOFXiHit+1) begin
    PROG_SHAPER #(
        .VAL_SIZE(5)
    ) CHANNEL_SHAPER_TOFX(
        .IN(TOFXHIT[TOFXiHit] & MASKTOFX[TOFXiHit]),
        .OUT(TOFXHIT_SH[TOFXiHit]),
        .CLK(CLK),
        .VAL(SHAPER_VAL)
    );
end
// TOF Y HIT SHAPERS
wire [39:0] TOFYHIT_SH;
genvar TOFYiHit;
for(TOFYiHit =0; TOFYiHit<40; TOFYiHit= TOFYiHit+1) begin
    PROG_SHAPER #(
        .VAL_SIZE(5)
    ) CHANNEL_SHAPER_TOFY(
        .IN(TOFYHIT[TOFYiHit] & MASKTOFY[TOFYiHit]),
        .OUT(TOFYHIT_SH[TOFYiHit]),
        .CLK(CLK),
        .VAL(SHAPER_VAL)
    );
end

// IF TOFHITLOGIC IS 0 THEN THE HIT IS THE OR OF THE BAR ENDS
reg [19:0] BARXHIT;
reg [19:0] BARYHIT;

genvar barHit;
for(barHit = 0; barHit<20; barHit= barHit+1) begin
    always @(posedge CLK) begin
        if (TOFHITLOGIC) begin // THEN USE AND
            BARXHIT[barHit] = TOFXHIT_SH[barHit*2] & TOFXHIT_SH[barHit*2+1];
            BARYHIT[barHit] = TOFYHIT_SH[barHit*2] & TOFYHIT_SH[barHit*2+1];
        end 
        else begin  // THEN USE OR
            BARXHIT[barHit] = TOFXHIT_SH[barHit*2] | TOFXHIT_SH[barHit*2+1];
            BARYHIT[barHit] = TOFYHIT_SH[barHit*2] | TOFYHIT_SH[barHit*2+1];        
        end //if
    end//always        
end// for

//NOW COMPUTE TOF ALONE TRIGGER
always @(posedge CLK) begin
    if (TOFTRGLOGIC) begin // THEN USE AND
        TOFTRG = (|BARXHIT) & (|BARYHIT);
    end 
    else begin  // THEN USE OR
        TOFTRG = (|BARXHIT) | (|BARYHIT);
    end //if
end//always        

// FOOT FRAGMENTATION TRIGGER, PISA VERSION:
// FOR THE MARGARITA IS THE MAJORITY TRIGGER
// FOR THE TOF THE VETO IS GIVEN BY THE AND OF THE HITS ON THE CENTRAL BARS (BAR 9)
// THE HIT IS THE OR OF THE OTHER BARS, THE HIT IS THE AND OF THE ENDS
//
// CREATE THE HITS AS THE AND
reg [19:0] BARXHITFRAG;
reg [19:0] BARYHITFRAG;
genvar fragHit;
for(fragHit = 0; fragHit<20; fragHit= fragHit+1) begin
    always @(posedge CLK) begin
            BARXHITFRAG[fragHit] = TOFXHIT_SH[fragHit*2] & TOFXHIT_SH[fragHit*2+1];
            BARYHITFRAG[fragHit] = TOFYHIT_SH[fragHit*2] & TOFYHIT_SH[fragHit*2+1];
    end//always        
end// for
reg FRAGTOF;
// NOW COMPUTE TOF FRAG TRIGGER, THE CENTRAL BARS ARE STILL IN, WILL BE REMOVED BY VETO
// THE TRIGGER IS THE OR OF THE TWO VIEWS
always @(posedge CLK) begin
        FRAGTOF = (|BARXHIT) | (|BARYHIT);
end//always 
//
// HERE CREATE THE VETO FROM THE CENTRAL BARS
// TOF X VETO SHAPERS, 2 CLK TICKS LONGER THAN OTHERS
wire  [1:0] VETOTOFX_SH;
PROG_SHAPER #(
    .VAL_SIZE(5)
)VETO_SHAPER_TOFX0(
     .IN(TOFXHIT[9*2]),
     .OUT(VETOTOFX_SH[0]),
     .CLK(CLK),
     .VAL(SHAPER_VAL_VETO)
);
PROG_SHAPER #(
    .VAL_SIZE(5)
)VETO_SHAPER_TOFX1(
     .IN(TOFXHIT[9*2+1]),
     .OUT(VETOTOFX_SH[1]),
     .CLK(CLK),
     .VAL(SHAPER_VAL_VETO)
);
// TOF Y VETO SHAPERS, 2 CLK TICKS LONGER THAN OTHERS
wire  [1:0] VETOTOFY_SH;
PROG_SHAPER #(
    .VAL_SIZE(5)
)VETO_SHAPER_TOFY0(
     .IN(TOFYHIT[9*2]),
     .OUT(VETOTOFY_SH[0]),
     .CLK(CLK),
     .VAL(SHAPER_VAL_VETO)
);
PROG_SHAPER #(
    .VAL_SIZE(5)
)VETO_SHAPER_TOFY1(
     .IN(TOFYHIT[9*2+1]),
     .OUT(VETOTOFY_SH[1]),
     .CLK(CLK),
     .VAL(SHAPER_VAL_VETO)
);
// CREATE THE VETO
always @(posedge CLK) begin
    FRAGVETO <= &VETOTOFX_SH & &VETOTOFY_SH; 
end
// IN PARALLEL REGISTER FRAGTOF AND MARGTRG TO PUT THEM 1 CLK LATER THAN THE VETO
always @(posedge CLK) begin
    FRAGTOFREG <= FRAGTOF; 
end

//
//
//////
//
//

// FOOT FRAGMENTATION TRIGGER, ROME VERSION:
// FOR THE MARGARITA IS THE MAJORITY TRIGGER
// FOR THE TOF THE VETO IS GIVEN BY THE AND OF THR EVEN HITS ON THE CENTRAL BARS (BAR 9)
// THE HIT IS THE AND OF THE BAR VIEWS, THE HIT IS THE PROGRAMMABLE OR OR AND OF THE ENDS
//     PLUS THE ODD SIGNALS OF THE CENTRAL BARS
//
// CREATE THE HITS AS THE AND
reg [19:0] BARXHITFROME;
reg [19:0] BARYHITFROME;
genvar rHit;
for(rHit = 0; rHit<20; rHit= rHit+1) begin
    always @(posedge CLK) begin
        if (TOFHITLOGICROME) begin // THEN USE AND
            BARXHITFROME[rHit] = TOFXHIT_SH[rHit*2] & TOFXHIT_SH[rHit*2+1];
            BARYHITFROME[rHit] = TOFYHIT_SH[rHit*2] & TOFYHIT_SH[rHit*2+1];
        end 
        else begin  // THEN USE OR
            BARXHITFROME[rHit] = TOFXHIT_SH[rHit*2] | TOFXHIT_SH[rHit*2+1];
            BARYHITFROME[rHit] = TOFYHIT_SH[rHit*2] | TOFYHIT_SH[rHit*2+1];        
         end //if
    end//always  
end// for
reg FRAGTOFROME;
// NOW COMPUTE TOF FRAG TRIGGER, THE CENTRAL BARS ARE STILL IN, WILL BE REMOVED BY VETO
// THE TRIGGER IS THE OR OF THE TWO VIEWS
always @(posedge CLK) begin
        FRAGTOFROME = (|BARXHITFROME | TOFXHIT_SH[9*2+1] ) & (|BARYHITFROME | TOFYHIT_SH[9*2+1]);
end//always 
//
// HERE CREATE THE VETO FROM THE CENTRAL BARS
// TOF X VETO SHAPERS, 2 CLK TICKS LONGER THAN OTHERS
wire   VETOTOFXROME_SH;
PROG_SHAPER #(
    .VAL_SIZE(5)
)VETO_SHAPER_TOFX0ROME(
     .IN(TOFXHIT[9*2]),
     .OUT(VETOTOFXROME_SH),
     .CLK(CLK),
     .VAL(SHAPER_VAL_VETO)
);
// TOF Y VETO SHAPERS, 2 CLK TICKS LONGER THAN OTHERS
wire  VETOTOFYROME_SH;
PROG_SHAPER #(
    .VAL_SIZE(5)
)VETO_SHAPER_TOFY0ROME(
     .IN(TOFYHIT[9*2]),
     .OUT(VETOTOFYROME_SH),
     .CLK(CLK),
     .VAL(SHAPER_VAL_VETO)
);
// CREATE THE VETO
always @(posedge CLK) begin
    FRAGVETOROME <= VETOTOFXROME_SH & VETOTOFYROME_SH; 
end
// IN PARALLEL REGISTER FRAGTOF AND MARGTRG TO PUT THEM 1 CLK LATER THAN THE VETO
always @(posedge CLK) begin
    FRAGTOFROMEREG <= FRAGTOFROME; 
end

endmodule
