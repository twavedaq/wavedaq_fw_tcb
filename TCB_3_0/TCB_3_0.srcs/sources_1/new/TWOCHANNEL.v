`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/26/2018 09:09:42 PM
// Design Name: 
// Module Name: TWOCHANNEL
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TWOCHANNEL(
    input A,
    input ISVETO_A,
    input MASK_A,
    input B,
    input ISVETO_B,
    input MASK_B,
    input CLK,
    input SEL,
    output reg OUT
    );
    
    //ISVETO=0 MASK=1: USE THAT CHANNEL
    //ISVETO=0 MASK=0: SILENCE NOISY CHANNEL IN OR -> OUT = 0
    //ISVETO=1 MASK=0: VETO CHANNEL
    //ISVETO=1 MASK=1: UNUSED CHANNEL IN AND -> OUT = 1
    
    
    wire A_IN, B_IN;
    assign A_IN = (MASK_A)? 0: A;
    assign B_IN = (MASK_B)? 0: B;
    
    wire A_VETO_IN, B_VETO_IN;
    assign A_VETO_IN = (ISVETO_A)? ~A_IN: A_IN;
    assign B_VETO_IN = (ISVETO_B)? ~B_IN: B_IN;
    
    always @(posedge CLK) begin
    
        case (SEL)
            1'b0:
                OUT <= A_VETO_IN | B_VETO_IN;
            1'b1:
                OUT <= A_VETO_IN & B_VETO_IN;
        endcase
    
    end
endmodule
