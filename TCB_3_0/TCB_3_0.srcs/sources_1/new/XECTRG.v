`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 09/30/2016 04:32:45 PM
// Design Name: 
// Module Name: XECTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// here we perform the sum of 16 samples and compare it with a threshold
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module XECTRG(
    input [1023:0] DATA,
    input ALGCLK,
    input CLK,
    output signed [34:0] SUM,
    output  [7:0] MAX,
    output [4:0] TDCNUM,
    output [7:0] TDCSUM,
    input [31:0] QSUMSEL
    );


//DECODE DATA
genvar islot;
wire [31*16-1:0] WFMFROMTCB;
wire [6*16-1:0] MAXFROMTCB;
wire [8*16-1:0] TDCSUMFROMTCB;
wire [5*16-1:0] TDCNUMFROMTCB;

for(islot = 0; islot < 16; islot = islot+1) begin
   SERDESDECODE #(
      .SLOT(islot),
      .NSLOT(16)
   ) DECODE (
      .DATA(DATA),
      .WFMSUM(WFMFROMTCB[31*islot+:31]),
      .MAX(MAXFROMTCB[6*islot+:6]),
      .TDCSUM(TDCSUMFROMTCB[8*islot+:8]),
      .TDCNUM(TDCNUMFROMTCB[5*islot+:5]),
      .TCOR(),
      .TCHITUS(),
      .TCTILEIDUS(),
      .TCTILETIMEUS(),
      .TCHITDS(),
      .TCTILEIDDS(),
      .TCTILETIMEDS(),
      .TCNHIT(),
      .CRCPAIRTRG(),
      .CRCSINGLETRG(),
      .BGOPRESHCOUNTER(),
      .NGENTRG(),
      //.RDCPLASTICLYSOSINGLE(),
      //.BGOVETOFIRE(),
      .ALFATRG(),
      .MONITORDCTRG(),
      .PROTONCURRENTTRG(),
      .RFACCELTRG(),
      .XECLEDMPPCTRG(),
      .XECLEDPMTTRG(),
      .TCDIODETRG(),
      .TCLASERTRG(),
      .BGOTHRFIRE(),
      .BGOCOSM(),
      .BGOTRG(),
      .RDCPLASTICSINGLE(),
      .RDCLYSOOR(),
      .RDCLYSOTHRFIRE(),
      .RDCTRG()
   );
end

//ASSIGN MPPC TO SUM UP
wire [31*4-1:0] MPPCWFM;
wire [6*4-1:0] MPPCMAX;
wire [8*4-1:0] MPPCTDCSUM;
wire [5*4-1:0] MPPCTDCNUM;

assign MPPCWFM[31*0+:31] =  WFMFROMTCB[31*`MPPC0SLOT+:31];
assign MPPCWFM[31*1+:31] =  WFMFROMTCB[31*`MPPC1SLOT+:31];
assign MPPCWFM[31*2+:31] =  WFMFROMTCB[31*`MPPC2SLOT+:31];
assign MPPCWFM[31*3+:31] =  WFMFROMTCB[31*`MPPC3SLOT+:31];
assign MPPCMAX[6*0+:6] =  MAXFROMTCB[6*`MPPC0SLOT+:6];
assign MPPCMAX[6*1+:6] =  MAXFROMTCB[6*`MPPC1SLOT+:6];
assign MPPCMAX[6*2+:6] =  MAXFROMTCB[6*`MPPC2SLOT+:6];
assign MPPCMAX[6*3+:6] =  MAXFROMTCB[6*`MPPC3SLOT+:6];
assign MPPCTDCNUM[5*0+:5] = TDCNUMFROMTCB[5*`MPPC0SLOT+:5];
assign MPPCTDCNUM[5*1+:5] = TDCNUMFROMTCB[5*`MPPC1SLOT+:5];
assign MPPCTDCNUM[5*2+:5] = TDCNUMFROMTCB[5*`MPPC2SLOT+:5];
assign MPPCTDCNUM[5*3+:5] = TDCNUMFROMTCB[5*`MPPC3SLOT+:5];
assign MPPCTDCSUM[8*0+:8] = TDCSUMFROMTCB[8*`MPPC0SLOT+:8];
assign MPPCTDCSUM[8*1+:8] = TDCSUMFROMTCB[8*`MPPC1SLOT+:8];
assign MPPCTDCSUM[8*2+:8] = TDCSUMFROMTCB[8*`MPPC2SLOT+:8];
assign MPPCTDCSUM[8*3+:8] = TDCSUMFROMTCB[8*`MPPC3SLOT+:8];

//SELECT PMT WAVEFORM
wire [31-1:0] PMTWFM;
assign PMTWFM =  WFMFROMTCB[31*`PMTSLOT+:31];

/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
///             THIS IS FOR THE SUM                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
//SUM MPPCs MULTIPLESUM BLOCK
wire [33-1:0] MPPCSUM;
   QUADRUPLESUM #(
        .SIZE(31),
        .REGIN(0),
        .REGOUT(1)
    ) QUADRUPLESUM_INST (
        .CLK(ALGCLK),
        .IN(MPPCWFM),
        .OUT(MPPCSUM)
    );
    
//// NOW SUM PMTs, extending PMTs
reg [31-1:0] PMTWFMREG; //pipeline register for PMTSUM
reg [34-1:0] MPPCPMTSUM;
always @(posedge ALGCLK) begin
    PMTWFMREG <= PMTWFM;
    MPPCPMTSUM <= $signed(MPPCSUM) + $signed({PMTWFMREG[30],PMTWFMREG[30],PMTWFMREG});
end

//RUNNING AVERAGE
reg [34-1:0] PRESUM_SR;
reg [34-1:0] PRESUM_SR2;
reg [34:0] RAVE;
always @(posedge ALGCLK) begin
    //DELAY Twice to account for ALGCLK
    PRESUM_SR <= MPPCPMTSUM;
    PRESUM_SR2 <= PRESUM_SR;

    RAVE <= $signed(MPPCPMTSUM)+$signed(PRESUM_SR2);
end

//multiplexer for 2-point average
assign SUM = (QSUMSEL[0])?RAVE:{MPPCPMTSUM[33], MPPCPMTSUM};

/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
///             THIS IS FOR THE MAX                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////////////////////////////////////////////////////////////////////////////////////    

MAXFIND #(
    .CMP_LAYER(2),
    .MAXWFMSIZE(5),
    .MAXSIZE(6),
    .SIGNED_COMPARE(0),
    .TIMESIZE(8)
) MAXFIND_INST (
    .CLK(ALGCLK),
    .MAXWFMIN(MPPCTDCNUM),
    .MAXIN(MPPCMAX),
    .TIMEIN(MPPCTDCSUM),
    .MAXWFM(TDCNUM),
    .MAX(MAX),
    .TIME(TDCSUM)
);

endmodule
