`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.08.2017 10:18:04
// Design Name: 
// Module Name: ADDERTREE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ADDERTREE(
    input [SIZE*16-1:0] IN,
    output [SIZE+4-1:0] OUT,
    input CLK
    );
    
    parameter SIZE=22;
    
    wire [(SIZE+2)*4-1:0] PARTIALSUM;
    
    //FIRST LAYER SUM (16 TO 4)
    genvar iAdder;
    for(iAdder=0; iAdder<4; iAdder=iAdder+1) begin 
        QUADRUPLESUM #(
            .SIZE(SIZE),
            .REGIN(1),
            .REGOUT(0)
        ) QUADRUPLESUM_INST (
            .CLK(CLK),
            .IN(IN[SIZE*4*iAdder+:SIZE*4]),
            .OUT(PARTIALSUM[(SIZE+2)*iAdder+:(SIZE+2)])
        );
    end
    
   //SECOND LAYER SUM (4 TO 1) 
   QUADRUPLESUM #(
        .SIZE(SIZE+2),
        .REGIN(1),
        .REGOUT(1)
    ) QUADRUPLESUM_INST (
        .CLK(CLK),
        .IN(PARTIALSUM),
        .OUT(OUT)
    );
    
endmodule
