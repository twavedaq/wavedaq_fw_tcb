`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.06.2017 11:01:31
// Design Name: 
// Module Name: BUFFERBUSY
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BUFFERBUSY(
    input CLK,
    input [31:0] SPI_CTRL,
    input [31:0] PACKET_CTRL,
    input NEXTBUFFER,
    output reg [MEMSELSIZE-1:0] SPI_SELECT,
    output reg [MEMSELSIZE-1:0] PACKET_SELECT,
    output [31:0] SPI_SELECT_WIRE,
    output [31:0] PACKET_SELECT_WIRE,
    output reg [MEMNUM-1:0] MEMORY_STATE
    );
    
    parameter MEMSELSIZE = 2;
    
    parameter MEMNUM = 2**MEMSELSIZE;
    
    wire INC_SPI;
    wire INC_PACKET;
    wire RESET;
    assign INC_SPI = SPI_CTRL[0] || NEXTBUFFER;
    assign INC_PACKET = PACKET_CTRL[0];
    assign RESET = SPI_CTRL[1];
    
    reg INC_SPI_OLD;
    reg INC_PACKET_OLD;
    reg RESET_OLD;
    
    always @(posedge CLK) begin
        INC_SPI_OLD <= INC_SPI;
        INC_PACKET_OLD <= INC_PACKET;
        RESET_OLD <= RESET;
    
        if(RESET & ~RESET_OLD) begin
            SPI_SELECT <= 0;
            PACKET_SELECT <= 0;
        end else begin
            if(INC_SPI & ~INC_SPI_OLD)begin
                SPI_SELECT <= SPI_SELECT + 1;
            end
            if(INC_PACKET & ~INC_PACKET_OLD)begin
                PACKET_SELECT <= PACKET_SELECT + 1;
            end
        end
    end
    
    genvar iMEM;
    for(iMEM=0; iMEM<MEMNUM; iMEM=iMEM+1) begin
        always @(posedge CLK) begin
            if(RESET & ~RESET_OLD) begin
                MEMORY_STATE[iMEM] <= 0;
            end else begin
                if(INC_PACKET & ~INC_PACKET_OLD) begin
                    if(PACKET_SELECT==iMEM) MEMORY_STATE[iMEM] <= 1'b1;
                end else if (INC_SPI & ~INC_SPI_OLD) begin
                    if(SPI_SELECT==iMEM) MEMORY_STATE[iMEM] <= 1'b0;
                end
            end
        end
    end
        
    
    assign SPI_SELECT_WIRE[MEMSELSIZE-1:0] = SPI_SELECT;
    assign SPI_SELECT_WIRE[7:MEMSELSIZE] = 1'b0;
    assign SPI_SELECT_WIRE[8+MEMSELSIZE-1:8] = PACKET_SELECT;
    assign SPI_SELECT_WIRE[15:8+MEMSELSIZE] = 1'b0;
    assign SPI_SELECT_WIRE[16+MEMNUM-1:16] = MEMORY_STATE;
    assign SPI_SELECT_WIRE[31:16+MEMNUM] = 1'b0;
    
    assign PACKET_SELECT_WIRE[MEMSELSIZE-1:0] = PACKET_SELECT;
    assign PACKET_SELECT_WIRE[7:MEMSELSIZE] = 1'b0;
    assign PACKET_SELECT_WIRE[8+MEMSELSIZE-1:8] = SPI_SELECT;
    assign PACKET_SELECT_WIRE[15:8+MEMSELSIZE] = 1'b0;
    assign PACKET_SELECT_WIRE[16+MEMNUM-1:16] = MEMORY_STATE;
    assign PACKET_SELECT_WIRE[31:16+MEMNUM] = 1'b0;
    
endmodule
