`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.11.2015 15:16:10
// Design Name: 
// Module Name: BUSSPARE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BUSSPARE(
    input [7:0] IN,
    output [7:0] OUT
    );
//ARRAY OF OBUF
OBUF OBUF_0[7:0](
.O(OUT),
.I(IN)
);
endmodule
