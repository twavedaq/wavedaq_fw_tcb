`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.01.2018 17:53:30
// Design Name: 
// Module Name: COMPARELINK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module COMPARELINK(
    input [8*nLink-1:0] LINKDATA,
    input CLK,
    output MATCH,
    output MATCH_EXACT
    );
    
    parameter [3:0] PATTERN = 4'hA;
    parameter nLink = 8;
    
    reg [8*nLink-1:0] FOUND_REG;
    genvar iLink;
    for(iLink=0; iLink<nLink; iLink=iLink+1) begin
        //compare with pattern (also shifted)
        wire [7:0] FOUND;
        assign FOUND[0] = (PATTERN == LINKDATA[7:4]);
        assign FOUND[1] = (PATTERN == LINKDATA[6:3]);
        assign FOUND[2] = (PATTERN == LINKDATA[5:2]);
        assign FOUND[3] = (PATTERN == LINKDATA[4:1]);
        assign FOUND[4] = (PATTERN == LINKDATA[3:0]);
        assign FOUND[5] = (PATTERN == {LINKDATA[2:0], LINKDATA[7]});
        assign FOUND[6] = (PATTERN == {LINKDATA[1:0], LINKDATA[7:6]});
        assign FOUND[7] = (PATTERN == {LINKDATA[0], LINKDATA[7:5]});
    
        //register comparisons
        always @(posedge CLK) begin
            FOUND_REG[8*iLink+:8] <= FOUND;
        end
    end
    
    //make and of all links to extract match
    genvar iOffset;
    wire [7:0] FOUND_SERDES;
    for(iOffset=0; iOffset<8; iOffset=iOffset+1) begin
        wire [nLink-1:0] RESULTS;
        for(iLink=0; iLink<nLink; iLink=iLink+1) begin
            assign RESULTS[iLink] = FOUND_REG[8*iLink+iOffset];
        end
        assign FOUND_SERDES[iOffset] = &RESULTS;
    end
    
    
    assign MATCH = |FOUND_SERDES;
    assign MATCH_EXACT = FOUND_SERDES[0];
endmodule
