// CRC-32 calculation
// Author: Marco Francesconi

module CRCCALC(CLK, RESET, CALCENA, DATA, CRC);
   input CLK;
   input RESET;
   input CALCENA;

   input [7:0] DATA;
   output [31:0] CRC;

   reg [31:0] calcreg;

   localparam [31:0] initval = 32'hFFFFFFFF;
   //localparam [31:0] initval = 32'h0;
   localparam [31:0] polynomial = 32'h04C11DB7;
   localparam [31:0] CRCxor = 32'hFFFFFFFF;
   //localparam [31:0] CRCxor = 32'h0;
   

   //single bit implemnetation
   
   /*integer i;
   always @(posedge CLK) begin
      if (RESET == 1'b1) begin
         //init
         calcreg = initval;
      end else if (CALCENA == 1'b1) begin
         calcreg = calcreg ^ {DATA, 24'h000000};
         
         for (i = 0; i<8; i = i+1) begin
            //$display ("%x", calcreg);
            if(calcreg[31]) begin
               calcreg = {calcreg[30:0], 1'b0};
               calcreg = calcreg ^ polynomial;
            end else begin
               calcreg = {calcreg[30:0], 1'b0};
            end
         end
      end
   end*/

   wire [31:0] calcnext;

   /*assign calcnext[0] = calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[1] = calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[2] = calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[3] = calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[4] = calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[5] = calcreg[29] ^ DATA[5] ^ calcreg[28] ^ data[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[6] = calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[7] = calcreg[31] ^ DATA[7] ^ calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[8] = calcreg[0] ^ calcreg[31] ^ DATA[7] ^ calcreg[30] ^ DATA[6] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[9] = calcreg[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[10] = calcreg[2] ^ calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[11] = calcreg[3] ^ calcreg[31] ^ DATA[7] ^ calcreg[30] ^ DATA[6] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[12] = calcreg[4] ^ calcreg[31] ^ DATA[7] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[13] = calcreg[5] ^ calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[14] = calcreg[6] ^ calcreg[31] ^ DATA[7] ^ calcreg[30] ^ DATA[6] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2];
   assign calcnext[15] = calcreg[7] ^ calcreg[31] ^ DATA[7] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3];
   assign calcnext[16] = calcreg[8] ^ calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[17] = calcreg[9] ^ calcreg[31] ^ DATA[7] ^ calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[18] = calcreg[10] ^ calcreg[31] ^ DATA[7] ^ calcreg[30] ^ DATA[6] ^ calcreg[26] ^ DATA[2];
   assign calcnext[19] = calcreg[11] ^ calcreg[31] ^ DATA[7] ^ calcreg[27] ^ DATA[3];
   assign calcnext[20] = calcreg[12] ^ calcreg[28] ^ DATA[4];
   assign calcnext[21] = calcreg[13] ^ calcreg[29] ^ DATA[5];
   assign calcnext[22] = calcreg[14] ^ calcreg[30] ^ DATA[6] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[23] = calcreg[15] ^ calcreg[31] ^ DATA[7] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[24] = calcreg[16] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[25] = calcreg[17] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2];
   assign calcnext[26] = calcreg[18] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[27] = calcreg[19] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[28] = calcreg[20] ^ calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[26] ^ DATA[2];
   assign calcnext[29] = calcreg[21] ^ calcreg[31] ^ DATA[7] ^ calcreg[30] ^ DATA[6] ^ calcreg[27] ^ DATA[3];
   assign calcnext[30] = calcreg[22] ^ calcreg[31] ^ DATA[7] ^ calcreg[28] ^ DATA[4];
   assign calcnext[31] = calcreg[23] ^ calcreg[29] ^ DATA[5];*/

   //remove double xors
   assign calcnext[0] = calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[1] = calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[2] = calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[3] = calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[4] = calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[5] = calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[6] = calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[7] = calcreg[31] ^ DATA[7] ^ calcreg[29] ^ DATA[5] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2] ^ calcreg[24] ^ DATA[0];
   assign calcnext[8] = calcreg[0] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[25] ^ DATA[1] ^ calcreg[24] ^ DATA[0];
   assign calcnext[9] = calcreg[1] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1];
   assign calcnext[10] = calcreg[2] ^ calcreg[29] ^ DATA[5] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2] ^ calcreg[24] ^ DATA[0];
   assign calcnext[11] = calcreg[3] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[25] ^ DATA[1] ^ calcreg[24] ^ DATA[0];
   assign calcnext[12] = calcreg[4] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[13] = calcreg[5] ^ calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[14] = calcreg[6] ^ calcreg[31] ^ DATA[7] ^ calcreg[30] ^ DATA[6] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2];
   assign calcnext[15] = calcreg[7] ^ calcreg[31] ^ DATA[7] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3];
   assign calcnext[16] = calcreg[8] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[24] ^ DATA[0];
   assign calcnext[17] = calcreg[9] ^ calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[25] ^ DATA[1];
   assign calcnext[18] = calcreg[10] ^ calcreg[31] ^ DATA[7] ^ calcreg[30] ^ DATA[6] ^ calcreg[26] ^ DATA[2];
   assign calcnext[19] = calcreg[11] ^ calcreg[31] ^ DATA[7] ^ calcreg[27] ^ DATA[3];
   assign calcnext[20] = calcreg[12] ^ calcreg[28] ^ DATA[4];
   assign calcnext[21] = calcreg[13] ^ calcreg[29] ^ DATA[5];
   assign calcnext[22] = calcreg[14] ^ calcreg[24] ^ DATA[0];
   assign calcnext[23] = calcreg[15] ^ calcreg[25] ^ DATA[1] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[24] = calcreg[16] ^ calcreg[26] ^ DATA[2] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[25] = calcreg[17] ^ calcreg[27] ^ DATA[3] ^ calcreg[26] ^ DATA[2];
   assign calcnext[26] = calcreg[18] ^ calcreg[28] ^ DATA[4] ^ calcreg[27] ^ DATA[3] ^ calcreg[24] ^ DATA[0] ^ calcreg[30] ^ DATA[6];
   assign calcnext[27] = calcreg[19] ^ calcreg[29] ^ DATA[5] ^ calcreg[28] ^ DATA[4] ^ calcreg[25] ^ DATA[1] ^ calcreg[31] ^ DATA[7];
   assign calcnext[28] = calcreg[20] ^ calcreg[30] ^ DATA[6] ^ calcreg[29] ^ DATA[5] ^ calcreg[26] ^ DATA[2];
   assign calcnext[29] = calcreg[21] ^ calcreg[31] ^ DATA[7] ^ calcreg[30] ^ DATA[6] ^ calcreg[27] ^ DATA[3];
   assign calcnext[30] = calcreg[22] ^ calcreg[31] ^ DATA[7] ^ calcreg[28] ^ DATA[4];
   assign calcnext[31] = calcreg[23] ^ calcreg[29] ^ DATA[5];

   always @(posedge CLK) begin
      if (RESET == 1'b1) begin
         //$display ("RESET");
         calcreg <= initval;
      end else if(CALCENA == 1'b1) begin
         calcreg <= calcnext;
         //$display ("%x %x -> %x", DATA, calcnext, calcreg);
      end
   end

   assign CRC = calcreg ^ CRCxor;
endmodule
