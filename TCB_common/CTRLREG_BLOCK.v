`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.06.2017 10:49:03
// Design Name: 
// Module Name: CTRLREG_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CTRLREG_BLOCK(
        input CLK,
        input [31:0]RADDR,
        input [31:0]WADDR,
        input RCLK,
        input WENA,
        input [31:0] WDATA,
        output tri [31:0] RDATA,
        output [31:0] PULSE_OUT,
        input [31:0] DATA_IN
    );
    
    parameter [31:0]REG_ADDR = 32'b0;
    
    PULSEREG 
     #(
        .REG_ADDR(REG_ADDR)
     ) RBASE_REG (
        .CLK(CLK),
        .RCLK(RCLK),
        .WADDR(WADDR),
        .WENA(WENA),
        .WDATA(WDATA),
        .OUT(PULSE_OUT)
    );

    SINGLEREG_R_BLOCK 
    #(
       .REG_ADDR(REG_ADDR)
    ) RSUM_REG (
       .CLK(CLK),
       .RCLK(RCLK),
       .RADDR(RADDR),
       .DATAOUT(RDATA),
       .VALUE(DATA_IN)
   );
    
endmodule
