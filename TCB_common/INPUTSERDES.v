`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.01.2018 11:55:22
// Design Name: 
// Module Name: INPUTSERDES
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module INPUTSERDES(
    //SERDES CONNECTIONS
    output [8*nLink-1:0] DATA,
    input CLK,
    input CLKDIV,
    input [nLink-1:0] IN_P,
    input [nLink-1:0] IN_N,
    //BASIC CONFIGURATION AND READOUT
    input DATAMASK,
    input REGENABLE,
    output [5*nLink-1:0] DLY_APPLIED,
    output [3*nLink-1:0] BISTLIP_APPLIED,
    //AUTOCONFIGURATION INTERFACE
    input CALIBSTART,
    input RESET_FSM,
    input CALIBMASK,
    output BUSY,
    output FAIL,
    output [31:0] DLY_TESTED,
    output [31:0] DLY_STATE
   );

    parameter nLink = 8;
    parameter negPolarity = 0;
    
    //CALIB FSM HERE
    wire [4:0] LOCAL_DLY;
    wire RESET;
    wire BITSLIP;
    wire LOADDLY;
    wire USE_DLY;
    wire [8*nLink-1:0] LINKDATA;
    LINKCALIBFSM #(.nLink(nLink)) CALIB(
        .LINKDATA(LINKDATA),
        .CLK(CLKDIV),
        .START(CALIBSTART),
        .BITSLIP(BITSLIP),
        .DLY(LOCAL_DLY),
        .LOADDLY(LOADDLY),
        .USE_DLY(USE_DLY),
        .RESET(RESET),
        .RESET_FSM(RESET_FSM),
        .BUSY(BUSY),
        .FAIL(FAIL),
        .DLY_TESTED(DLY_TESTED),
        .DLY_STATE(DLY_STATE),
        .CALIBMASK(CALIBMASK)
    );
    
    // link instantiation
    genvar iLink;
    for(iLink = 0; iLink < nLink; iLink = iLink+1) begin
     
        wire [7:0] LOCALDATA;
        INPUTLINK LINK(
            .DATA(LOCALDATA),
            .RESET(RESET),       
            .BITSLIP(BITSLIP),    
            .DLY(LOCAL_DLY),
            .CURRENT_DLY(DLY_APPLIED[5*iLink+:5]),
            .CURRENT_BISLIP(BISTLIP_APPLIED[3*iLink+:3]),
            .LOADDLY(LOADDLY),
            .CLK(CLK),
            .CLKDIV(CLKDIV),
            .IN_P(IN_P[iLink]),
            .IN_N(IN_N[iLink])
        );
        
        //parameter option to invert data polarity (used for FCI Cables)
        assign LINKDATA[8*iLink+:8] = (negPolarity)? ~LOCALDATA: LOCALDATA;
        
    end
       
    reg [8*nLink-1:0] LINKDATA_REG;
    
    always @(posedge CLKDIV) begin
        LINKDATA_REG <= LINKDATA;
    end
    
    //mask data to algorithms
    assign DATA = (DATAMASK)? 0 : (REGENABLE) ? LINKDATA_REG : LINKDATA;
    //assign DATA[8*iLink+:8] = LINKDATA;
    
endmodule
