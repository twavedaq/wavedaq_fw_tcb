`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.06.2017 14:17:21
// Design Name: 
// Module Name: MULTIPLEBUFFER
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MULTIPLEBUFFER(
        input CLK,
        input [31:0] RADDR_SPI,
        input [31:0] WADDR_SPI,
        output tri [31:0] RDATA_SPI,
        input [31:0] WDATA_SPI,
        input RCLK_SPI,
        input WENA_SPI,
        input [31:0] RADDR_PACKET,
        input [31:0] WADDR_PACKET,
        output tri [31:0] RDATA_PACKET,
        input [31:0] WDATA_PACKET,
        input RCLK_PACKET,
        input WENA_PACKET,
        input NEXTBUFFER,
        input [31:0] BUFFERADDR,
        output [31:0] BUFFERDATA,
        output BUSY
    );
    
    parameter [31:0] BASEADDR = 32'h40000;
    
    parameter MEMSELSIZE = 2;
    parameter MEMSIZE = 10;
    
    parameter MEMNUM = 2**MEMSELSIZE;
    

    tri [32*MEMNUM-1:0] RDATA_VECTOR;
    wire [32*MEMNUM-1:0] BUFFERDATA_VECTOR;

    wire [31:0]SPI_CTRL;
    wire [MEMSELSIZE-1:0]SPI_SELECT;
    wire [31:0]SPI_SELECT_WIRE;
    
    CTRLREG_BLOCK
    #(
       .REG_ADDR(BASEADDR+(2**MEMSIZE))
    ) SPICTRL_REG (
        .CLK(CLK),
        .RCLK(RCLK_SPI),
        .RADDR(RADDR_SPI),
        .WADDR(WADDR_SPI),
        .WENA(WENA_SPI),
        .WDATA(WDATA_SPI),
        .RDATA(RDATA_SPI),
        .PULSE_OUT(SPI_CTRL),
        .DATA_IN(SPI_SELECT_WIRE)
    );
    
    wire [31:0]PACKET_CTRL;
    wire [MEMSELSIZE-1:0]PACKET_SELECT;
    wire [31:0]PACKET_SELECT_WIRE;
    
    CTRLREG_BLOCK
    #(
       .REG_ADDR(BASEADDR+(2**MEMSIZE))
    ) PACKETCTRL_REG (
        .CLK(CLK),
        .RCLK(RCLK_PACKET),
        .RADDR(RADDR_PACKET),
        .WADDR(WADDR_PACKET),
        .WENA(WENA_PACKET),
        .WDATA(WDATA_PACKET),
        .RDATA(RDATA_PACKET),
        .PULSE_OUT(PACKET_CTRL),
        .DATA_IN(PACKET_SELECT_WIRE)
    );

    wire [MEMNUM-1:0] MEMORY_STATE;
    assign BUSY = &MEMORY_STATE;

    BUFFERBUSY
    #(
        .MEMSELSIZE(MEMSELSIZE)
    ) BUSY_CTRL (
        .CLK(CLK),
        .SPI_CTRL(SPI_CTRL),
        .PACKET_CTRL(PACKET_CTRL),
        .NEXTBUFFER(NEXTBUFFER),
        .SPI_SELECT(SPI_SELECT),
        .PACKET_SELECT(PACKET_SELECT),
        .SPI_SELECT_WIRE(SPI_SELECT_WIRE),
        .PACKET_SELECT_WIRE(PACKET_SELECT_WIRE),
        .MEMORY_STATE(MEMORY_STATE)
        );
    
    genvar iMem;
    for(iMem=0; iMem < MEMNUM; iMem = iMem +1) begin
        reg RCLK_MUX;
        reg WENA_MUX;
        reg [31:0] RADDR_MUX;
        reg [31:0] WADDR_MUX;
        reg [31:0] WDATA_MUX;
        reg [1:0] DESTINATION;
        
        always @(*) begin
            if(MEMORY_STATE[iMem]==1'b0)begin
                //memory clear: prioritize writing
                if(PACKET_SELECT == iMem)begin
                    RCLK_MUX <= RCLK_PACKET;
                    WENA_MUX <= WENA_PACKET;
                    RADDR_MUX <= RADDR_PACKET;
                    WADDR_MUX <= WADDR_PACKET;
                    WDATA_MUX <= WDATA_PACKET;
                    DESTINATION <= 2'h1;
                end else if (SPI_SELECT == iMem) begin
                    RCLK_MUX <= RCLK_SPI;
                    WENA_MUX <= WENA_SPI;
                    RADDR_MUX <= RADDR_SPI;
                    WADDR_MUX <= WADDR_SPI;
                    WDATA_MUX <= WDATA_SPI;
                    DESTINATION <= 2'h2;
                end else begin
                    RCLK_MUX <= 1'b0;
                    WENA_MUX <= 1'b0;
                    RADDR_MUX <= 32'b0;
                    WADDR_MUX <= 32'b0;
                    WDATA_MUX <= 32'b0;
                    DESTINATION <= 2'h0;
                end
            end else begin
                //memory fill: prioritize reading
                if(SPI_SELECT == iMem)begin
                    RCLK_MUX <= RCLK_SPI;
                    WENA_MUX <= WENA_SPI;
                    RADDR_MUX <= RADDR_SPI;
                    WADDR_MUX <= WADDR_SPI;
                    WDATA_MUX <= WDATA_SPI;
                    DESTINATION <= 2'h2;
                end else if (PACKET_SELECT == iMem) begin
                    RCLK_MUX <= RCLK_PACKET;
                    WENA_MUX <= WENA_PACKET;
                    RADDR_MUX <= RADDR_PACKET;
                    WADDR_MUX <= WADDR_PACKET;
                    WDATA_MUX <= WDATA_PACKET;
                    DESTINATION <= 2'h1;
                end else begin
                    RCLK_MUX <= 1'b0;
                    WENA_MUX <= 1'b0;
                    RADDR_MUX <= 32'b0;
                    WADDR_MUX <= 32'b0;
                    WDATA_MUX <= 32'b0;
                    DESTINATION <= 2'h0;
                end
            end
        end


        NEWRAM 
        #(
        .BASEADDR(BASEADDR),
        .SIZE(MEMSIZE)
        ) ram (
        .CLK(CLK),
        .CLKDATA(1'b0),
        .ENA(1'b0),
        .RECORD(1'b0),
        .RCLK(RCLK_MUX),
        .WENA(WENA_MUX),
        .RADDR(RADDR_MUX),
        .WADDR(WADDR_MUX),
        .INADDR(BUFFERADDR[MEMSIZE-1:0]),
        .WDATA(WDATA_MUX),
        .RDATA(RDATA_VECTOR[32*iMem+:32]),
        .DIN(32'b0),
        .DOUT(BUFFERDATA_VECTOR[32*iMem+:32])
    );
    
    assign RDATA_SPI = (DESTINATION==2'h2)? RDATA_VECTOR[32*iMem+:32]:32'bZZZZZZZZ;
    assign RDATA_PACKET = (DESTINATION==2'h1)? RDATA_VECTOR[32*iMem+:32]:32'bZZZZZZZZ;
    
    end
    
    assign BUFFERDATA = BUFFERDATA_VECTOR[32*SPI_SELECT+:32];
    
    
endmodule
