`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.10.2018 16:39:38
// Design Name: 
// Module Name: PACKREGS
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PACKREGS(
    input CLK,
    input RCLK,
    input WENA,
    input [31:0] WADDR,
    input [31:0] RADDR,
    input [31:0] WDATA,
    output [31:0] RDATA,
    output [31:0] WADDR_FROM_REG,
    output [31:0] RADDR_FROM_REG
    );
    
    parameter [31:0] BASEADDR = 'h1000;
    
    //input registers
    wire [31:0] A;
    SINGLEREG_RW_BLOCK 
    #(
    .REG_ADDR(BASEADDR)
    ) REG_A (
    .CLK(CLK),
    .RCLK(RCLK),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WENA(WENA),
    .DATAIN(WDATA),
    .DATAOUT(RDATA),
    .VALUE(A),
    .RESET(1'b0)
    );
    
    wire [31:0] B;
    SINGLEREG_RW_BLOCK 
    #(
    .REG_ADDR(BASEADDR+1)
    ) REG_B (
    .CLK(CLK),
    .RCLK(RCLK),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WENA(WENA),
    .DATAIN(WDATA),
    .DATAOUT(RDATA),
    .VALUE(B),
    .RESET(1'b0)
    );
    
    wire [31:0] C;
    SINGLEREG_RW_BLOCK 
    #(
    .REG_ADDR(BASEADDR+2)
    ) REG_C (
    .CLK(CLK),
    .RCLK(RCLK),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WENA(WENA),
    .DATAIN(WDATA),
    .DATAOUT(RDATA),
    .VALUE(C),
    .RESET(1'b0)
    );
    
    SINGLEREG_RW_BLOCK 
    #(
    .REG_ADDR(BASEADDR+3)
    ) REG_DR (
    .CLK(CLK),
    .RCLK(RCLK),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WENA(WENA),
    .DATAIN(WDATA),
    .DATAOUT(RDATA),
    .VALUE(RADDR_FROM_REG),
    .RESET(1'b0)
    );
    
        SINGLEREG_RW_BLOCK 
    #(
    .REG_ADDR(BASEADDR+4)
    ) REG_DW (
    .CLK(CLK),
    .RCLK(RCLK),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WENA(WENA),
    .DATAIN(WDATA),
    .DATAOUT(RDATA),
    .VALUE(WADDR_FROM_REG),
    .RESET(1'b0)
    );
    
    //flag reg
    reg CARRY;
    reg ALEC;
    reg AGTC;
    reg AEQC;
    reg ANEQC;
    SINGLEREG_R_BLOCK 
    #(
    .REG_ADDR(BASEADDR+8)
    ) RFLAG (
    .CLK(CLK),
    .RCLK(RCLK),
    .RADDR(RADDR),
    .DATAOUT(RDATA),
    .VALUE({27'b0, CARRY, ALEC, AGTC, ANEQC, AEQC})
    );
    
    //sum reg
    reg [31:0] SUM;
    SINGLEREG_R_BLOCK 
    #(
    .REG_ADDR(BASEADDR+9)
    ) RSUM (
    .CLK(CLK),
    .RCLK(RCLK),
    .RADDR(RADDR),
    .DATAOUT(RDATA),
    .VALUE(SUM)
    );
    
    //bit regs
    reg [31:0] AANDB;
    SINGLEREG_R_BLOCK 
    #(
    .REG_ADDR(BASEADDR+10)
    ) RAND (
    .CLK(CLK),
    .RCLK(RCLK),
    .RADDR(RADDR),
    .DATAOUT(RDATA),
    .VALUE(AANDB)
    );
    reg [31:0] AORB;
    SINGLEREG_R_BLOCK 
    #(
    .REG_ADDR(BASEADDR+11)
    ) ROR (
    .CLK(CLK),
    .RCLK(RCLK),
    .RADDR(RADDR),
    .DATAOUT(RDATA),
    .VALUE(AORB)
    );
    reg [31:0] AXORB;
    SINGLEREG_R_BLOCK 
    #(
    .REG_ADDR(BASEADDR+12)
    ) RXOR (
    .CLK(CLK),
    .RCLK(RCLK),
    .RADDR(RADDR),
    .DATAOUT(RDATA),
    .VALUE(AXORB)
    );
    
    always @(posedge CLK) begin
        {CARRY, SUM} <= A+B;
        ALEC <= (A<C);
        AEQC <= (A==C);
        ANEQC <= (A!=C);
        AGTC <= (A>C);
        AANDB <= A & B;
        AORB <= A | B;
        AXORB <= A ^ B;
    end
    
endmodule
