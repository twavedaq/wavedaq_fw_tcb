`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.01.2018 11:55:22
// Design Name: 
// Module Name: PATTERNGENERATOR
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PATTERNGENERATOR(
    input SYNC,
    input CLK,
    output reg [7:0] PATTERN
    );
    
    parameter [7:0] PATTERN_INIT = 8'hA0;
    
    wire [7:0] PATTERN_NEXT;
    
    NEXTPATTERN NEXT(
        .CURRENT(PATTERN),
        .NEXT(PATTERN_NEXT)
    );
    
    always @(posedge CLK) begin
        if(SYNC)
            PATTERN <= PATTERN_INIT;
        else
            PATTERN <= PATTERN_NEXT;
    end
endmodule
