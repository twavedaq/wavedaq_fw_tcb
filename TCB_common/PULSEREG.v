`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.10.2016 23:15:55
// Design Name: 
// Module Name: PULSEREG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PULSEREG(
    input RCLK,
    input CLK,
    input [31:0]WADDR,
    input [31:0] WDATA,
    output reg [31:0] OUT,
    input WENA
    );
    
    parameter [31:0]REG_ADDR = 32'b0;
    
    reg OLD_RCLK;
    
    always @(posedge CLK) begin
        OLD_RCLK <= ~RCLK;
        OUT <= 32'b0;
        
        if (WENA && (REG_ADDR==WADDR)) begin
            if(RCLK & OLD_RCLK) begin
               //posedge RCLK
               OUT <= WDATA;
            end else begin
               //else
                OUT <= 32'b0; 
            end
        end
    
    end
    
endmodule
