`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.07.2020 15:29:22
// Design Name: 
// Module Name: READOUTFSM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module READOUTFSM(
    input CLK,
    // DCB interface
    output reg [7:0] DCBDATA,
    input DCBREADY,
    // Buffer interface
    input BUFFERBUSY,
    output reg NEXTBUFFER,
    output reg [31:0] BUFFERADDR,
    input [31:0] BUFFERDATA,
    // flags & control
    input ENABLE,
    input READYIGNORE,
    input [15:0] PAYLOAD_MAX, 
    //header data
    input [3:0] BOARD_REVISION,
    input [15:0] SERIAL_NUMBER,
    input [7:0] CRATE_ID,
    input [7:0] SLOT_ID
    );

//=============Internal Constants======================
parameter SIZE = 4;
parameter WAIT_STATE         = 4'b0000,
            FETCH_BUFFER_HEAD  = 4'b0001, 
            FETCH_BANK_HEAD    = 4'b0010,
            FETCH_BANK_LENGTH  = 4'b0011,
            CHECK_READY        = 4'b0100,
            SEND_START         = 4'b0101,
            SEND_PKTSIZE       = 4'b0110,
            SEND_HEADER        = 4'b0111,
            SEND_DATA          = 4'b1000,
            SEND_CRC           = 4'b1001;

parameter OUT_SIZE = 3;
parameter OUT_IDLE      = 3'b000,
            OUT_SOF     = 3'b001,
            OUT_PKTSIZE = 3'b010,
            OUT_HEADER  = 3'b011,
            OUT_DATA    = 3'b100,
            OUT_CRC     = 3'b101;
    


//=============Internal Variables======================
reg   [SIZE-1:0]          STATE        ;// Seq part of the FSM
reg   [OUT_SIZE-1:0]      OUT_SOURCE   ;// Source selection
reg                       WAIT_BUFFER  ;// Flag to delay buffer read

// internal packet header componenets
reg   [15:0]              PACKET_NUMBER ;
reg   [15:0]              PAYLOAD_LENGTH;
reg   [15:0]              PAYLOAD_OFFSET;
reg   [31:0]              CURRENT_BANK;


//const definitions
parameter [7:0] IDLE_PATTERN = 8'h5A;
parameter [15:0] START_VALUE = 16'hF00D;
wire [15:0] START_PATTERN;
assign START_PATTERN = START_VALUE;
reg START_BYTE;

//packet size word
reg PKTSIZE_BYTE;
reg [15:0] PKTSIZE_VALUE;

//header const
parameter [7:0] PROTOCOL_VERSION = 8'h08;
parameter [3:0] BOARD_TYPE = 4'h4; //TCB
parameter [7:0] DATA_TYPE = 8'h8; //TCB data

//header informations from buffer
reg       [31:0] NBANKS;
reg       [31:0] BANK_LENGTH;
reg       [47:0] TRIGGER_INFO;
reg       [31:0] EVENT_NUMBER;
reg       [31:0] BANK_NAME;
reg       [31:0] TIMESTAMP;

//temporarely unused
wire      [15:0] TEMPERATURE;
assign TEMPERATURE = 16'hFFFF;

// Flags
reg   SOE;
reg   EOE;
reg   SOT;
reg   EOT;

//wire remaining bits
wire [15:0] REMAINING_WORDS;
assign REMAINING_WORDS = BANK_LENGTH[15:0] - {2'b0, PAYLOAD_OFFSET[15:2]};

// make sure FSM stay alive
reg FORCE_SINGLE_PACKET;
always @(posedge CLK) begin
    if (PAYLOAD_MAX == 16'b0) begin
        FORCE_SINGLE_PACKET <= 1'b1;
    end else begin
        FORCE_SINGLE_PACKET <= 1'b0;
    end
end


//header definition
parameter HEADER_BYTESIZE = 40;

wire [8*HEADER_BYTESIZE-1:0] HEADER;
reg [5:0] HEADER_BYTE;

assign HEADER = {PROTOCOL_VERSION,
                BOARD_TYPE,
                BOARD_REVISION,
                SERIAL_NUMBER,
                CRATE_ID,
                SLOT_ID,
                PACKET_NUMBER,
                DATA_TYPE,
                {4'b0, SOT, EOT, SOE, EOE},
                TRIGGER_INFO,
                EVENT_NUMBER,
                PAYLOAD_LENGTH,
                PAYLOAD_OFFSET,
                BANK_NAME,
                TIMESTAMP,
                TEMPERATURE,
                48'b0};

/*assign HEADER[8*1-1:8*0] = PROTOCOL_VERSION; // 8 bit
assign HEADER[8*2-1:8*1] = {BOARD_TYPE, BOARD_REVISION}; // 2x 4 bit
assign HEADER[8*4-1:8*2] = SERIAL_NUMBER; // 16 bit
assign HEADER[8*5-1:8*4] = CRATE_ID; // 8 bit
assign HEADER[8*6-1:8*5] = SLOT_ID; // 8 bit
assign HEADER[8*8-1:8*6] = PACKET_NUMBER; // 16 bit
assign HEADER[8*9-1:8*8] = DATA_TYPE; // 8 bit
assign HEADER[8*10-1:8*9] = {4'b0, SOT, EOT, SOE, EOE}; // 8 bit
assign HEADER[8*16-1:8*10] = TRIGGER_INFO; //48 bits
assign HEADER[8*20-1:8*16] = EVENT_NUMBER; //32 bits
assign HEADER[8*22-1:8*20] = PAYLOAD_LENGTH; // 16 bits
assign HEADER[8*24-1:8*22] = PAYLOAD_OFFSET; // 16 bits
assign HEADER[8*28-1:8*24] = BANK_NAME; // 32 bits
assign HEADER[8*32-1:8*28] = TIMESTAMP; // 32 bits
assign HEADER[8*34-1:8*32] = TEMPERATURE; // 16 bits
assign HEADER[8*40-1:8*34] = 48'b0; //RESERVED 48 bits*/


//CRC definition
reg [1:0] CRC_BYTE;
reg CRCRESET;
reg CRCCALCENA;
wire [31:0] CRCOUT;
wire [31:0] CRCDATA;

//add crc calculation here
CRCCALC CRCCALC_BLOCK(
    .CLK(CLK),
    .RESET(CRCRESET),
    .CALCENA(CRCCALCENA),
    .DATA(DCBDATA),
    .CRC(CRCOUT)
);

genvar iCRCByte;
genvar iCRCBit;
for(iCRCByte=0; iCRCByte<4; iCRCByte = iCRCByte +1) begin
    for(iCRCBit=0; iCRCBit<8; iCRCBit = iCRCBit +1) begin
        // former bit ordering
        //assign CRCDATA[8*iCRCByte+iCRCBit] = CRCOUT[8*iCRCByte+(7-iCRCBit)];
        // do not change bit ordering
        assign CRCDATA[8*iCRCByte+iCRCBit] = CRCOUT[8*iCRCByte+iCRCBit];
    end
end

// register incoming data from BUFFER
reg [31:0] BUFFERVAL;
reg [1:0] DATA_BYTE;
reg UPDATE_BUFFERVAL;

always @(posedge CLK) begin
    if(UPDATE_BUFFERVAL) begin
        BUFFERVAL <= BUFFERDATA;
    end
end

// mux
always @(*) begin
    case(OUT_SOURCE)
        OUT_IDLE: begin
            DCBDATA <= IDLE_PATTERN;
        end
        OUT_SOF: begin
            DCBDATA <= START_PATTERN[8*START_BYTE +:8];
        end
        OUT_PKTSIZE: begin
            DCBDATA <= PKTSIZE_VALUE[8*PKTSIZE_BYTE +:8];
        end
        OUT_HEADER: begin
            DCBDATA <= HEADER[8*HEADER_BYTE +:8];
        end
        OUT_DATA: begin
            DCBDATA <= BUFFERVAL[8*DATA_BYTE +:8];
        end
        OUT_CRC: begin
            DCBDATA <= CRCDATA[8*CRC_BYTE +:8];
        end
        default: begin
            DCBDATA <= IDLE_PATTERN;
        end
    endcase
end

always @ (posedge CLK)
begin : FSM
    //reset
    if (ENABLE == 1'b0) begin
        STATE <= WAIT_STATE;
        OUT_SOURCE <= OUT_IDLE;
    end else
        case(STATE)
            WAIT_STATE : begin
                OUT_SOURCE <= OUT_IDLE;
                UPDATE_BUFFERVAL <= 0;
                WAIT_BUFFER <= 1'b0; 
                NEXTBUFFER <= 1'b0;
                //WAIT FOR BUFFER TO BE AVAILABLE
                if( BUFFERBUSY & ENABLE) begin
                    BUFFERADDR <= 0;
                    STATE <= FETCH_BUFFER_HEAD;
                    WAIT_BUFFER <= 1'b1; 
                    PACKET_NUMBER <= 0;
                end
            end
            FETCH_BUFFER_HEAD : begin
                SOE <=1'b1;
                EOE <= 1'b0;
                
                if (WAIT_BUFFER) begin
                    WAIT_BUFFER <= 1'b0;
                end else begin 
                    case(BUFFERADDR)
                        31'h0: begin
                            NBANKS <= BUFFERDATA;
                            BUFFERADDR <= BUFFERADDR + 1;
                            WAIT_BUFFER <= 1'b1;
                        end
                        31'h1: begin
                            EVENT_NUMBER <= BUFFERDATA;
                            BUFFERADDR <= BUFFERADDR + 1;
                            WAIT_BUFFER <= 1'b1;
                        end
                        31'h2: begin
                            TIMESTAMP <= BUFFERDATA;
                            BUFFERADDR <= BUFFERADDR + 1;
                            WAIT_BUFFER <= 1'b1;
                        end
                        31'h3: begin
                            TRIGGER_INFO[31:0] <= BUFFERDATA;
                            BUFFERADDR <= BUFFERADDR + 1;
                            WAIT_BUFFER <= 1'b1;
                        end
                        31'h4: begin
                            TRIGGER_INFO[47:32] <= BUFFERDATA[15:0];
                            BUFFERADDR <= BUFFERADDR + 1;
                            WAIT_BUFFER <= 1'b1;
                        end
                        31'h5: begin
                            STATE <= FETCH_BANK_HEAD;
                            CURRENT_BANK <= 31'h0;
                        end
                    endcase
                end
            end
            
            FETCH_BANK_HEAD: begin
                if(CURRENT_BANK == NBANKS) begin
                    // The End (of event)
                    STATE <= WAIT_STATE;
                    NEXTBUFFER <= 1'b1;
                end else begin
                    BANK_NAME <= BUFFERDATA;
                    BUFFERADDR <= BUFFERADDR + 1;
                    STATE <= FETCH_BANK_LENGTH;
                    WAIT_BUFFER <= 1'b1;
                end
            end
            FETCH_BANK_LENGTH: begin
                if (WAIT_BUFFER) begin
                    WAIT_BUFFER <= 1'b0;
                end else begin 
                    BANK_LENGTH <= BUFFERDATA;
                    PAYLOAD_OFFSET <= 16'b0;
                    BUFFERADDR <= BUFFERADDR + 1;
                    STATE <= CHECK_READY;
                    SOT <= 1'b1;
                end
            end
            CHECK_READY : begin
                UPDATE_BUFFERVAL <= 1'b1;
                if(REMAINING_WORDS <= PAYLOAD_MAX || FORCE_SINGLE_PACKET) begin
                    // can send all remaining words in one packet
                    EOT <= 1'b1;
                    EOE <= (CURRENT_BANK == (NBANKS-1));
                    PAYLOAD_LENGTH <= {REMAINING_WORDS[13:0], 2'b00};
                end else begin
                    // need another packet after this one
                    EOT <= 1'b0;
                    PAYLOAD_LENGTH <= {PAYLOAD_MAX[13:0], 2'b00};
                end
                //WAIT FOR DCB 
                if(DCBREADY | READYIGNORE) begin
                    STATE <= SEND_START;
                    START_BYTE <= 1'b1;
                    OUT_SOURCE <= OUT_SOF;
                end
            end
        
            SEND_START: begin
                START_BYTE <= 1'b0;

                //calculate packet size
                PKTSIZE_VALUE <= PAYLOAD_LENGTH + HEADER_BYTESIZE;
                
                if(START_BYTE == 1'b0) begin
                    STATE <= SEND_PKTSIZE;
                    PKTSIZE_BYTE <= 1'b1;
                    OUT_SOURCE <= OUT_PKTSIZE;
                end
            end

            SEND_PKTSIZE: begin
                PKTSIZE_BYTE <= 1'b0;
                CRCRESET <= 1'b1;
                CRCCALCENA <= 1'b0;
                
                if(PKTSIZE_BYTE == 1'b0) begin
                    STATE <= SEND_HEADER;
                    HEADER_BYTE <= HEADER_BYTESIZE-1;
                    OUT_SOURCE <= OUT_HEADER;
                    CRCRESET <= 1'b0;
                    CRCCALCENA <= 1'b1;
                end

            end
        
            SEND_HEADER: begin
                HEADER_BYTE <= HEADER_BYTE - 1;
                CRCRESET <= 1'b0;
                CRCCALCENA <= 1'b1;
                if(HEADER_BYTE == 0) begin
                    if(PAYLOAD_LENGTH == 0) begin
                        STATE <= SEND_CRC;
                        CRC_BYTE <= 3;
                        OUT_SOURCE <= OUT_CRC;
                        CRCCALCENA <= 1'b0;
                    end else begin
                        STATE <= SEND_DATA;
                        DATA_BYTE <= 3;
                        OUT_SOURCE <= OUT_DATA;
                        PAYLOAD_LENGTH <= PAYLOAD_LENGTH - 1;
                    end
                end
            end
        
            SEND_DATA: begin
                DATA_BYTE <= DATA_BYTE - 1;
                UPDATE_BUFFERVAL <= 1'b0;
                PAYLOAD_LENGTH <= PAYLOAD_LENGTH - 1;
                PAYLOAD_OFFSET <= PAYLOAD_OFFSET + 1;
                
                if(DATA_BYTE == 2) begin
                    BUFFERADDR <= BUFFERADDR + 1;
                end
                if(DATA_BYTE == 1) begin
                    UPDATE_BUFFERVAL <= 1'b1;
                end
                
                if(DATA_BYTE == 0) begin
                    DATA_BYTE <= 3;
                end
                
                if(PAYLOAD_LENGTH == 0) begin
                    STATE <= SEND_CRC;
                    CRC_BYTE <= 3;
                    OUT_SOURCE <= OUT_CRC;
                    CRCCALCENA <= 1'b0;
                end
            end
        
            SEND_CRC: begin
                CRC_BYTE <= CRC_BYTE - 1;
                if(CRC_BYTE == 0) begin
                    OUT_SOURCE <= OUT_IDLE;
                    PACKET_NUMBER <= PACKET_NUMBER + 1;
                    SOT <= 1'b0;
                    SOE <= 1'b0;
                    if (EOT == 1'b1) begin
                        // was last packet, go to next bank
                        CURRENT_BANK <= CURRENT_BANK + 1;
                        STATE <= FETCH_BANK_HEAD;
                    end else begin
                        // another packet to be sent in this type
                        STATE <= CHECK_READY;
                    end
                end
            end
        
            default : begin
                STATE <= WAIT_STATE;
            end
        endcase
    end

endmodule
