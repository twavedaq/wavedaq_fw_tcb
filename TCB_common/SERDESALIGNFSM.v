`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.02.2018 16:48:22
// Design Name: 
// Module Name: SERDESALIGNFSM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SERDESALIGNFSM(
      input [nSerdes*nLink*8-1:0] SERDESDATA,
      input [nSerdes-1:0] SERDESBUSY,
      input [nSerdes-1:0] SERDESFAIL,
      input CLK,
      input START,
      input RESET_FSM,
      input MASKENABLE,
      output reg [nSerdes-1:0] SERDESMASK,
      output reg [nSerdes-1:0] SERDESDLY,
      output reg [nSerdes*4-1:0] SERDESOFFSET,
      output reg [4:0] SERDESMIN,
      output BUSY,
      output reg FAIL
      );


//=============Internal Constants======================
parameter SIZE = 4;
parameter WAIT_STATE         = 4'b0001,
          WAIT_BUSY_STATE    = 4'b0010,
          WAIT_START_STATE   = 4'b0011,
          START_STATE        = 4'b0100,
          FIND_FIRST_STATE   = 4'b0101,
          FIND_MIN_STATE     = 4'b0110,
          CALC_DLY_STATE     = 4'b0111;

parameter nLink = 8;
parameter nSerdes = 16;
//=============Internal Variables======================
reg   [SIZE-1:0]          STATE        ;// Seq part of the FSM
reg   [4:0]               SERDESCOUNTER;

assign BUSY = (STATE !=WAIT_STATE);

//mux to select data from serdes
wire [3:0] DATA;
assign DATA = SERDESDATA[SERDESCOUNTER*nLink*8+:4];
//mux to select reference serdes
wire [3:0] DATAMIN;
assign DATAMIN = SERDESDATA[SERDESMIN*nLink*8+:4];
//difference of serdes values
wire [3:0] DIFF;
assign DIFF = DATA - DATAMIN;

always @ (posedge CLK)
   begin : FSM
      //reset
      if (RESET_FSM == 1'b1) begin
         STATE <= WAIT_STATE;
      end else
         case(STATE)
            WAIT_STATE : begin
               if(START) begin
                  SERDESMASK <= 0;
                  FAIL <= 1'b0;
                  STATE <= WAIT_BUSY_STATE;
               end
            end
            WAIT_BUSY_STATE : begin
            //wait for SERDESBUSY to rise
               if(|SERDESBUSY) begin
                  STATE <= WAIT_START_STATE;
               end
            end
         WAIT_START_STATE : begin
            //wait all SERDESBUSY to fall
            if(~(|SERDESBUSY)) begin
               STATE <= START_STATE;
            end
         end
         START_STATE : begin
            STATE <= FIND_FIRST_STATE;
            SERDESDLY <= 0;
            SERDESOFFSET <= 0;
            SERDESCOUNTER <= 0;
            SERDESMIN <= 0;
         end
         FIND_FIRST_STATE : begin
            if(SERDESCOUNTER < nSerdes) begin
               SERDESCOUNTER <= SERDESCOUNTER+1;
               if(!SERDESFAIL[SERDESCOUNTER]) begin
                  //found valid serdes
                  SERDESMIN <= SERDESCOUNTER;
                  STATE <= FIND_MIN_STATE;
               end else begin
                  //current serdes not valid
                  STATE <= FIND_FIRST_STATE;
               end
            end else begin
               //no serdes valid, end without fail
               STATE <= WAIT_STATE;
            end
         end
         FIND_MIN_STATE : begin
            if(SERDESCOUNTER < nSerdes) begin
               STATE <= FIND_MIN_STATE;
               SERDESCOUNTER <= SERDESCOUNTER+1;
               if(!SERDESFAIL[SERDESCOUNTER]) begin
                  //skip serdes not calibrated
                  if(DIFF[3]) begin
                     //bigger delay
                     SERDESMIN <= SERDESCOUNTER;
                  end
               end
            end else begin
               STATE <= CALC_DLY_STATE;
               SERDESCOUNTER <= 0;
            end 
         end
         CALC_DLY_STATE : begin
            if(SERDESCOUNTER < nSerdes) begin
               SERDESCOUNTER <= SERDESCOUNTER+1;
               SERDESOFFSET[4*SERDESCOUNTER+:4] <= DIFF;
               if(!SERDESFAIL[SERDESCOUNTER]) begin
                  //skip serdes not calibrated
                  if(DIFF == 1) begin
                     //possible to align with 1 delay
                     SERDESDLY[SERDESCOUNTER] <= 1'b1;
                     STATE <= CALC_DLY_STATE;
                  end else if(DIFF == 0) begin
                     //already aligned
                     SERDESDLY[SERDESCOUNTER] <= 1'b0;
                     STATE <= CALC_DLY_STATE;
                  end else begin
                     //NO POSSIBILITY TO ALIGN: fail
                     FAIL<=1'b1;
                     SERDESDLY[SERDESCOUNTER] <= 1'b0;
                     STATE <= CALC_DLY_STATE;//keep on to compute all inter-serdes Delays
                  end
               end
            end else begin
               //DONE
               STATE <= WAIT_STATE;
               if(MASKENABLE & ~FAIL) SERDESMASK <= SERDESFAIL;
            end
         end
         default : begin
            STATE <= WAIT_STATE;
         end
      endcase
   end




   endmodule
