`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.07.2020 10:22:00
// Design Name: 
// Module Name: SERDES_DCB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module SERDES_DCB(
    input SERDES_CLK,
    input CLK,
    input RESET,
    //SERDES CONNECTIONS
    output DCB_DATA_P,
    output DCB_DATA_N,
    input [7:0] DCBDATA,
    //READY CONNECTIONS
    input DCB_READY_P,
    input DCB_READY_N,
    output reg DCBREADY
    );
    
    //OUTPUT TO DCB 
    OUTPUTLINK OUT(
        .OUT_P(DCB_DATA_P),
        .OUT_N(DCB_DATA_N),
        .CLK(SERDES_CLK),
        .CLKDIV(CLK),
        .DATA(DCBDATA),
        .RESET(RESET)
    );

    wire READY;
    IBUFDS #(
       .DIFF_TERM("TRUE"), // Differential Termination
       .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest perforrmance="FALSE"
       .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_READY (
       .O(READY), // Buffer output
       .I(DCB_READY_P), // Diff_p buffer input (connect directly to top-level port)
       .IB(DCB_READY_N) // Diff_n buffer input (connect directly to top-level port)
    );

    //register READY signal
    always @(posedge CLK) begin
        DCBREADY <= READY; 
    end

endmodule
