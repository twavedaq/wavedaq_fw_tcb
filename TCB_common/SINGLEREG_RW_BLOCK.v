`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.11.2015 15:55:14
// Design Name: 
// Module Name: SINGLEREG_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SINGLEREG_RW_BLOCK(
    input CLK,
    input [31:0]RADDR,
    input [31:0]WADDR,
    input RCLK,
    input WENA,
    input RESET,
    input [31:0] DATAIN,
    output tri [31:0] DATAOUT,
    output reg [31:0] VALUE
    );
    
    parameter [31:0] REG_ADDR = 32'b0;
    parameter [31:0] DEFAULT = 32'b0;

    
//SIMPLE REGISTER
reg RCLK_OLD;
reg OUT;
always @(posedge CLK) begin
    RCLK_OLD <= RCLK;
    if(RCLK & ~RCLK_OLD) begin
        if(WADDR==REG_ADDR) begin
            //write
            if(WENA)
                VALUE <= DATAIN;
        end
        if(RADDR==REG_ADDR)
            OUT <= 1'b1;
        else OUT <= 1'b0;
    end else if(RESET) begin
        VALUE <= DEFAULT;
    end
end

assign DATAOUT = (OUT)?VALUE:32'hzzzzzzzz;


endmodule
