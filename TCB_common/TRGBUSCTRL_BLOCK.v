`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.11.2015 10:40:02
// Design Name: 
// Module Name: TRGBUSCTRL_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TRGBUSCTRL_BLOCK(
    input CLK,
    input SYNCIN,
    input TRGIN,
    input SWSYNC,
    input SWSTOP,
    input GETRI,
    output reg BOASTOP, // THIS IS THE STOP FROM THE TRIGGER BUS TO THE FPGA
    output reg BOASYNC, // THIS IS THE SYNC FROM THE TRIGGER BUS TO THE FPGA
    output TRGOUT,  // THIS IS THE STOP TO THE WHOLE SYSTEM THROUGH THE TRIGGER BUS
    output reg SYNCOUT, // THIS IS THE SYNC TO THE WHOLE SYSTEM THROUGH THE TRIGGER BUS

    input SYSBUSY,
    output reg BUSYOUT,
    input MASKTRG,
    input MASKSYNC,
    input MASKBUSY,
    input TRGBUS_ENA
    );

always @(posedge CLK) begin
    if (TRGBUS_ENA ==1'b1) begin
        BOASTOP <= (TRGIN & MASKTRG) | SWSTOP;
        BOASYNC <= SYNCIN & MASKSYNC;
    end else begin
        BOASTOP <= (GETRI & MASKTRG) | SWSTOP;
        BOASYNC <= SWSYNC & MASKSYNC;
    end
    SYNCOUT <= SWSYNC;
    BUSYOUT <= SYSBUSY & MASKBUSY;
end

assign TRGOUT = GETRI;

endmodule
