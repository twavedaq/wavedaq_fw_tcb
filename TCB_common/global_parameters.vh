// If we have not included file before,
// this symbol _my_incl_vh_ is not defined.
`ifndef _global_parameters_
`define _global_parameters_
// Start of include contents
`define MAGIC_NUMBER 8'hAC
`define VENDOR_ID 8'h02
`define BOARD_TYPE 8'h04

`define NTRG 64
`define TRGFAMNUM 6
`define TRITYPDIM 6

//Serdes number
`define NSERDESBP 16
`define NSERDESFP 4

//Experiment logic switch
`define MEG
//`define FOOT
//`define SCIFI
//`define LOLX

//MEG TRGCrate Config
`define DC0SLOT 3
`define DC1SLOT 4
`define MPPC0SLOT 5
`define MPPC1SLOT 6
`define MPPC2SLOT 7
`define TCSLOT 8
`define MPPC3SLOT 9
`define AUXSLOT 10
`define PMTSLOT 10
`define DC2SLOT 11
`define DC3SLOT 12

//SCI CONFIG
`define SCIFIFIRSTSLOT 0
`define SCIFILASTSLOT  5
`define SCIFIFIBERS 42
`define SCIFICHANNELS 96

//MATRIX CONFIG
`define MATRIXFIRSTSLOT 0
`define MATRIXLASTSLOT  5
`define MATRIXCHANNELS 81

//FOOT CONFIG
`define MARGARITASLOT 4
`define TOFXSLOT0 5
`define TOFXSLOT1 6
`define TOFXSLOT2 7
`define TOFYSLOT0 7
`define TOFYSLOT1 8
`define TOFYSLOT2 9
`define TOFCENTRALSLOT 10
`define FCALOSLOT 13

//TRIPATTERN SHIFT REGISTER
`define SRSIZE  8
`define DLYSIZE  4

//TCB_1 pattern
`define PATTERNNUM  3

//LUT TABLES
`define LXePatchAddr 32'h20000
`define LXePatchAddrMux 32'h20ZZZ
`define DMNarrowAddr 32'h30000
`define DMNarrowAddrMux 32'h3ZZZZ
`define DMWideAddr 32'h40000
`define DMWideAddrMux 32'h4ZZZZ

//TC Stuff
`define NTCPositron 4

//Address space
`define RHW_ADDR               32'h00000000
`define RRUN_ADDR              32'h00000001
`define RDLY_ADDR              32'h00000002
`define RNTRG_ADDR             32'h00000003
`define RALGSEL_ADDR           32'h00000004
`define PLLRES_ADDR            32'h00000005 
`define RBOARDID_ADDR          32'h00000006 
`define PRESCAADC_ADDR         32'h00000007
`define SYNCWFM_ADDR           32'h00000008
`define RPAYLOADMAX_ADDR       32'h00000009
`define RUSR_ADDR              32'h0000000F
`define RENA_ADDR              32'h00000020
`define RENA_SIZE              4
`define RTRGPATTERN_ADDR       32'h00000030
`define RTRGPATTERN_SIZE       4
`define RTRGFORCE_ADDR         32'h00000040
`define RTRGFORCE_SIZE         4
`define RCMD_ADDR              32'h000000FF
`define RPRESCA_ADDR           32'h00000100
`define RPRESCA_SIZE           8
`define RTOTALTIME_ADDR        32'h00000200
`define RLIVETIME_ADDR         32'h00000201
`define REVENTCOUNTER_ADDR     32'h00000202
`define RTRGTYPE_ADDR          32'h00000203
`define RSYSCOUNTER_ADDR       32'h00000204
`define RSYSTYPE_ADDR          32'h00000205
`define RPCURR_ADDR            32'h00000206
`define RFSTIME_ADDR           32'h00000207
`define RSERDESTXCONF_ADDR     32'h00000321
`define RSERDESCALIBFSM_ADDR   32'h00000326
`define RSERDESMASK_ADDR       32'h00000327
`define RSERDESSTATUS_ADDR     32'h00000330
`define RSERDESCALIBBUSY_ADDR  32'h00000351
`define RSERDESCALIBFAIL_ADDR  32'h00000352
`define RDCBREADY_ADDR         32'h00000353
`define RALIGNDLY_ADDR         32'h00000354
`define RALIGNOFFSET0_ADDR     32'h00000355
`define RALIGNOFFSET1_ADDR     32'h00000356
`define RSERDESMINLATENCY_ADDR 32'h00000357
`define RSERDESCALIBSTATE_ADDR 32'h00000360
`define RSERDESCALIBTESTED_ADDR 32'h00000380
`define RTRGCOUNTER_ADDR       32'h00000400
`define RTRGCOUNTER_SIZE       6
`define RTRGDLY_ADDR           32'h00000500
`define RTRGDLY_SIZE           6
`define RLXEQSUMH_ADDR         32'h00000600
`define RTILEMASK_ADDR         32'h00000601
`define RTILEMASK_SIZE         2
`define RLXEQSUML_ADDR         32'h00000606
`define RLXEQSUMCOSM_ADDR      32'h00000607
`define RLXEPATCH_ADDR         32'h00000608
`define RLXETIMEOFF_ADDR       32'h00000609
`define RTIMEN_ADDR            32'h0000060A
`define RTIMEW_ADDR            32'h0000060B
`define RTCTIMEH_ADDR          32'h0000060C
`define RTCTIMEL_ADDR          32'h0000060D
`define RRDCLYSOTHR_ADDR       32'h0000060E
`define RBGOTHR_ADDR           32'h0000060F
`define RRDCMASK_ADDR          32'h00000610
`define RBGOMASK_ADDR          32'h00000611
`define RALFATHR_ADDR          32'h00000612
`define RALFAPEAK_ADDR         32'h00000613
`define RQSUMSELECT_ADDR       32'h00000614
`define RBGOVETOTHR_ADDR       32'h00000615
`define RTCHITTHR_ADDR         32'h00000616
`define RNGENDLY_ADDR          32'h00000617
`define RNGENWIDTH_ADDR        32'h00000618
`define RLXENGENH_ADDR         32'h00000619
`define RLXENGENL_ADDR         32'h0000061A
`define RBGOHITDLY_ADDR        32'h0000061B
`define RCRCHITMASK_ADDR       32'h0000061C
`define RCRCPAIRENABLE_ADDR    32'h0000061D
`define RMAJORITYVALUE_ADDR    32'h0000061E
`define RMASKTOFX_ADDR         32'h0000061F
`define RMASKTOFX_SIZE         2
`define RMASKTOFY_ADDR         32'h00000621
`define RMASKTOFY_SIZE         2
`define RINTERSPILLDELAY       32'h00000623
`define RMATRIXMASK0_ADDR      32'h00000624
`define RMATRIXMASK1_ADDR      32'h00000625
`define RMATRIXMASK2_ADDR      32'h00000626
`define RMASKFCALO_ADDR        32'h00000627
`define RMASKFNEUTRON_ADDR     32'h00000628
`define RLXEHITTHR_ADDR        32'h00000629
`define RLXEPATCHDLY_ADDR      32'h0000062A
`define RRDCLYSOVETOTHR_ADDR   32'h0000062B
`define RRDCHITDLY_ADDR        32'h0000062C
`define RRDCLYSOHITMASK_ADDR   32'h0000062D
`define RRDCLYSOHITMASK_SIZE   3
`define RDETECTORDLY_ADDR      32'h00000630
`define RDETECTORDLY_SIZE      2
`define RFIBCOUNTER_ADDR       32'h00000700
`define RSINGLECRATECFG_ADDR   32'h00000800
`define RSINGLECRATEVETO_ADDR  32'h00000801
`define RSINGLECRATEVETO_SIZE  3
`define RSINGLECRATEMASK_ADDR  32'h00000809
`define RSINGLECRATEMASK_SIZE  3
`define RSINGLECRATELOGIC_ADDR 32'h00000811
`define RSINGLECRATELOGIC_SIZE 4
`define RALGCLKMEMADDR_ADDR    32'h0000FFFE
`define RMEMADDR_ADDR          32'h0000FFFF
`define RINMEM0_ADDR           32'h00010000
`define RINMEM0_SIZE           8
`define RINMEM1_ADDR           32'h00010100
`define RINMEM1_SIZE           7
`define RINMEM2_ADDR           32'h00010200
`define RINMEM2_SIZE           7
`define RINMEM3_ADDR           32'h00010300
`define RINMEM3_SIZE           7
`define RINMEM4_ADDR           32'h00010400
`define RINMEM4_SIZE           7
`define RINMEM5_ADDR           32'h00010500
`define RINMEM5_SIZE           7
`define RINMEM6_ADDR           32'h00010600
`define RINMEM6_SIZE           7
`define RINMEM7_ADDR           32'h00010700
`define RINMEM7_SIZE           7
`define RINMEM8_ADDR           32'h00010800
`define RINMEM8_SIZE           7
`define RINMEM9_ADDR           32'h00010900
`define RINMEM9_SIZE           7
`define RINMEM10_ADDR          32'h00010A00
`define RINMEM10_SIZE          7
`define RINMEM11_ADDR          32'h00010B00
`define RINMEM11_SIZE          7
`define RINMEM12_ADDR          32'h00010C00
`define RINMEM12_SIZE          7
`define RINMEM13_ADDR          32'h00010D00
`define RINMEM13_SIZE          7
`define RINMEM14_ADDR          32'h00010E00
`define RINMEM14_SIZE          7
`define RINMEM15_ADDR          32'h00010F00
`define RINMEM15_SIZE          7
`define ROUTMEM_ADDR           32'h00011000
`define ROUTMEM_SIZE           7
`define RXECMEM_ADDR           32'h00011100
`define RXECMEM_SIZE           7
`define RGENTMEM_ADDR          32'h00012000
`define RGENTMEM_SIZE          5
`define RBGOMEM_ADDR           32'h00013000
`define RBGOMEM_SIZE           7
`define RRDCMEM_ADDR           32'h00014000
`define RRDCMEM_SIZE           7
`define RTCMEM_ADDR            32'h00015000
`define RTCMEM_SIZE            7
`define RALFAMEM_ADDR          32'h00016000
`define RALFAMEM_SIZE          7
`define RFIBCOINCCOUNTER_ADDR  32'h00800000
`define RFIBCOINCCOUNTER_SIZE  9
`define RPACK0_ADDR            32'h01000000
`define RPACK0_SIZE            10
`define RPACK1_ADDR            32'h01000400
`define RPACK1_SIZE            10
`define RPACK2_ADDR            32'h01000800
`define RPACK2_SIZE            10
`define RARBITER_ADDR          32'h01001000
`define RBUFFER_ADDR           32'h02000000
`define RBUFFER_SIZE           14
`define RPACKREG_ADDR          32'h03000000
`define RPACKREG_SIZE          3


`endif //_my_incl_vh_ 
