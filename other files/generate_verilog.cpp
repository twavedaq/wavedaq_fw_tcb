#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <array>

typedef struct {
   enum {
      cInput,
      cRegister
   } source;
   int bit;
} bit_type;

int main(){
   const uint32_t polynomial = 0x4C11DB7;

   const char stringInput[] = "data"; //data input
   const char stringRegister[] = "calcreg"; //CRC register
   const char stringNext[] = "calcnext"; //next CRC register

   //init register content
   std::array<std::vector<bit_type>, 32+8> xors;
   for(int iBit=0; iBit<32; iBit++){
      bit_type bit;
      bit.bit = iBit;
      bit.source = bit_type::cRegister;

      xors[iBit+8].clear();
      xors[iBit+8].push_back(bit);
   }
   for(int iBit=0; iBit<8; iBit++){
      xors[iBit].clear();
   }
   
   //xor 8 input bits
   for(int iBit=0; iBit<8; iBit++){
      bit_type bit;
      bit.bit = iBit;
      bit.source = bit_type::cInput;
      
      xors[32+iBit].push_back(bit);
   }

   //apply polynomial 8 times
   for(int iBit=7; iBit>=0; iBit--){
   //   int polystart = 32+iBit-1;

      for(int jBit=0; jBit <32; jBit++){
         if(polynomial & (0x80000000>>jBit)){
            for(auto b : xors[32+iBit]){
                  xors[31-jBit+iBit].push_back(b);
            }
         }
      }
   }

   //print
   for(int iBit=0; iBit<32+8; iBit++){
      printf("bit %d, len %d:\n\t", iBit, xors[iBit].size());
      for(auto b: xors[iBit]){
         printf("%c%d, ", b.source+'A', b.bit);
      }
      printf("\n");
   }

   //search duplicates
   for(int iBit=0; iBit<32+8; iBit++){
      for(int i=0; i < xors[iBit].size(); i++){
         bit_type a = xors[iBit][i];
         int j;
         for (j=i+1; j < xors[iBit].size(); j++){
            bit_type b = xors[iBit][j];
            if(a.bit == b.bit && a.source == b.source){
               //printf("bit %d xor duplicate: %c%d %c%d\n", iBit, a.source+'A', a.bit, b.source+'A', b.bit);
               break;
            }
         }
         if(j!= xors[iBit].size()){
            //printf("need ro remove %d and %d\n", i, j);
            xors[iBit].erase(xors[iBit].begin() + j);
            xors[iBit].erase(xors[iBit].begin() + i);
            i--;
         }
      }
   }


   //print verilog
   for(int iBit=0; iBit<32+8; iBit++){
      printf("assign %s[%d] = ", stringNext, iBit);
      
      bool first= true;
      for(auto b: xors[iBit]){
         if(!first)
            printf(" ^ ");
         else 
            first = false;

         if(b.source == bit_type::cInput)
            printf("%s[%d]", stringInput, b.bit);
         else if(b.source == bit_type::cRegister)
            printf("%s[%d]", stringRegister, b.bit);
      }
      printf(";\n");
   }
   
}
