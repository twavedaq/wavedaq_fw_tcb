// DESCRIPTION: Verilator: Verilog example module
//
// This file ONLY is placed under the Creative Commons Public Domain, for
// any use, without warranty, 2017 by Wilson Snyder.
// SPDX-License-Identifier: CC0-1.0
//======================================================================

// Include common routines
#include <verilated.h>

// Include model header, generated from Verilating "top.v"
#include "Vcrccalc.h"

int main(int argc, char** argv, char** env) {
   // See a similar example walkthrough in the verilator manpage.

   // This is intended to be a minimal example.  Before copying this to start a
   // real project, it is better to start with a more complete example,
   // e.g. examples/c_tracing.

   // Prevent unused variable warnings
   if (false && argc && argv && env) {}

   // Construct the Verilated model, from Vcrccalc.h generated from Verilating "crccalc.v"
   Vcrccalc* top = new Vcrccalc;

   //init
   top->clk = 0;
   top->reset = 1;
   top->calcena = 0;
   top->data = 0;
   top->eval();
   top->clk = 1;
   top->eval();
   printf("initial %08x\n", top->out);

   int vals[] = {0x01, 0xFF};

   // Simulate until $finish or end of steps
   for(int iVal = 0; iVal < sizeof(vals)/sizeof(int) && !Verilated::gotFinish(); iVal++) {
      top->reset = 0;
      top->calcena = 1;

      top->clk = 0;
      // Evaluate model
      top->data = vals[iVal];
      top->eval();

      top->clk = 1;

      // Evaluate model
      top->eval();

      printf("%02x %08x\n", top->data, top->out);
   }
   printf("final %08x\n", top->out);

   int out = top->out;
   top->clk = 0;
   top->data = (out >> 24) & 0xFF;
   // Evaluate model
   top->eval();

   top->clk = 1;
   // Evaluate model
   top->eval();

   printf("%02x %08x\n", top->data, top->out);

   top->clk = 0;
   top->data = (out >> 16) & 0xFF;
   // Evaluate model
   top->eval();

   top->clk = 1;
   // Evaluate model
   top->eval();

   printf("%02x %08x\n", top->data, top->out);

   top->clk = 0;
   top->data = (out >> 8) & 0xFF;
   // Evaluate model
   top->eval();

   top->clk = 1;
   // Evaluate model
   top->eval();

   printf("%02x %08x\n", top->data, top->out);

   top->clk = 0;
   top->data = (out >> 0) & 0xFF;
   // Evaluate model
   top->eval();

   top->clk = 1;
   // Evaluate model
   top->eval();

   printf("%02x %08x\n", top->data, top->out);


   // Final model cleanup
   top->final();

   // Destroy model
   delete top;

   // Fin
   exit(0);
}
